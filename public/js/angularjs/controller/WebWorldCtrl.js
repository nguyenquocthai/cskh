/*
* Main controller
* use rootScope
*/

WebWorldApp.controller('WebWorldCtrl', ['$scope','$rootScope', '$locale',
    function ($scope, $rootScope, $locale) {
        $rootScope.app = {
            title: 'Admin - PhanKhang',
            class: '',
        };
        $rootScope.api_token = '?api_token=PBsQwZyv';

        $locale.NUMBER_FORMATS.DECIMAL_SEP = ',';
        $locale.NUMBER_FORMATS.GROUP_SEP = '.';
    }
]);

