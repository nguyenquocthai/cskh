//-------------------------------------------------------------------------------
$.validator.addMethod(
    'phoneVI',
    function(value, element) {
      var re = new RegExp(/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/);
      return this.optional(element) || re.test(value);
    },
    'Số điện thoại không hợp lệ'
);