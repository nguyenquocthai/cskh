<?php
    $class_name = '';
    $text = '';

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Kích hoạt';
            break;

        case 1:
            $class_name = 'red';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Tạm dừng';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span title="<?= $title ?>" style='color:<?= $class_name ?>' class=" <?= $class_name ?>">
    <?= $title; ?>
</span>