<?php
    function displayList($list, $array_cat, $chart = 'i_') {
?>
    <ol class="dd-list">
    <?php foreach($list as $item): 
        $active = "";
        if (in_array($item["id"], $array_cat)) {
           $active = "active";
        }
    ?>
    <li class="dd-item par<?php echo $chart ;?>">
        <div data-id="<?php echo $item["id"];?>" class="<?php echo $chart ;?> dd-handle <?php echo $active;?>"><?php echo $item["title"]; ?> <i class="far fa-check-square checked"></i> <i class="far fa-square no-check"></i></div>
    <?php if (array_key_exists("children", $item)): ?>
    <?php displayList($item["children"], $array_cat, $chart.'i_'); ?>
    <?php endif; ?>
    </li>
    <?php endforeach; ?>
    </ol>
<?php
}
?>

<?php displayList($list, $array_cat); ?>