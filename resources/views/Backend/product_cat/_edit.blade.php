<?php $data_json = json_encode($data);?>
<div class="row">
    <div class="col-sm-8">

        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Thông tin</span>
                </div>

                <div class="actions box_lang_muti">
                    <div class="btn-group">
                        <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> <span class="name_mod">Vietnam</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a data-lang="vn" class="pick_lang_muti" href="javascript:;">Vietnam</a>
                            </li>
                            <li>
                                <a data-lang="en" class="pick_lang_muti" href="javascript:;">English</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="form-group">
                    <label class="lang_label">
                        <span>Tiêu đề</span>
                        <img class="lang_pick active" data-lang="vn" src="/public/img/vn.png" alt="">
                        <img class="lang_pick" data-lang="en" src="/public/img/en.png" alt="">
                    </label>
                
                    <input name="title" data-lang="vn" type="text" placeholder="Tiêu đề" class="form-control value_lang active">
                
                    <input name="title_en" data-lang="en" type="text" placeholder="Tiêu đề (English)" class="form-control value_lang">
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Mô tả ngắn</span>
                        <img class="lang_pick active" data-lang="vn" src="/public/img/vn.png" alt="">
                        <img class="lang_pick" data-lang="en" src="/public/img/en.png" alt="">
                    </label>
                
                    <textarea name="summary" data-lang="vn" placeholder="Nhập mô tả ngắn" class="form-control value_lang active" rows="5"></textarea>
                
                    <textarea name="summary_en" data-lang="en" placeholder="Nhập mô tả ngắn (English)" class="form-control value_lang" rows="5"></textarea>
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Tài liệu</span>
                        <img class="lang_pick active" data-lang="vn" src="/public/img/vn.png" alt="">
                        <img class="lang_pick" data-lang="en" src="/public/img/en.png" alt="">
                    </label>
                    <div class="tailieu_box value_lang active" data-lang="vn">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="ten_tailieu" placeholder="Tên tài liệu" class="form-control" type="text">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control tailieu" type="file">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tailieu_box value_lang" data-lang="en">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="ten_tailieu_en" placeholder="Tên tài liệu (English)" class="form-control" type="text">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control tailieu_en" type="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Nội dung</span>
                        <span data-lang="en" class="lang_alt">(English)</span>
                        <img class="lang_pick active" data-lang="vn" src="/public/img/vn.png" alt="">
                        <img class="lang_pick" data-lang="en" src="/public/img/en.png" alt="">
                    </label>
                    
                    <div class="value_lang active" data-lang="vn">
                        <textarea name="content" id="content" class="form-control"></textarea>
                    </div>
                
                    <div class="value_lang" data-lang="en">
                        <textarea name="content_en" id="content_en" class="form-control"></textarea>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="col-sm-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Hình đại diện</span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-group">
                    <div class="file_upload_box">
                        <input type="file" class="file_image">
                        <img class="image_review" src="/public/img/no-image.png" alt="...">
                    </div>
                </div>

                <div class="form-group">
                    <label class="next-label">Meta title</label>
                    <input type="text" class="form-control" name="keyword">
                </div>

                <div class="form-group">
                    <label class="next-label">Meta description</label>
                    <textarea name="description" class="form-control"></textarea>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input
</script>