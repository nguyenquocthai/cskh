<ol class="dd-list">
@if (count($product_cats) != 0)
    @foreach ($product_cats as $product_cat)
    <li class="dd-item" data-id="{{$product_cat->id}}">
        <div class="ctr_action">
            <a data-id="{{$product_cat->id}}" class="btn btn-xs blue btn-edit-row" title="Sửa">
                <i class="fa fa-edit"></i>
            </a>
            
            <a data-id="{{$product_cat->id}}" class="btn btn-xs red delete_row" title="Xóa">
                <i class="fa fa-trash-o"></i>
            </a>

            <a data-id="{{$product_cat->id}}" class="btn btn-xs red btn-status" title="Kích hoạt">
                <i class="far fa-check-square"></i>
            </a>
        </div>
        <div class="dd-handle">{{$product_cat->title}}</div>
    </li>
    @endforeach
@else

@endif
</ol>