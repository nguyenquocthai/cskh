WebWorldApp.controller('product_cats.index', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {

        $rootScope.app.title = 'Quản trị: loại sản phẩm';
        $scope.title = 'Danh sách loại sản phẩm';
        $scope.form_add = $('#form_add').html();
        $scope.product_cat = {};
        $scope.form = $('#userForm').html();
        
        // DATATABLE
        commonService.requestFunction('index_product_cat', {}, function(e) {

            $('#nestable').html(e.nestable);
            $('.hide_product_cat').html(e.product_cat0);
            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1,
                maxDepth: 2
            })
            .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable').data('output', $('#nestable-output')));
        });

        $('.ctr_modal_addrow').click(function(event) {
            $('#form_add').html($scope.form_add);
            $('#form_edit').html('');
            $('#modal_addrow').modal('show');
            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        });

        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        $('#nestable-menu').on('click', function(e)
        {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var title = $('.search_title').val() || '';
            console.log(title);
            $scope.datatable.columns(3).search(title)
                            .draw();
        };

        //-------------------------------------------------------------------------------
        $('.btn_addrow').click(function(event) {
            var data = {};

            $("#form_add").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();

            var file = $("#form_add .file_image").get(0).files[0];
            var tailieu = $("#form_add .tailieu").get(0).files[0];
            var tailieu_en = $("#form_add .tailieu_en").get(0).files[0];

            var fd = new FormData();
            fd.append("value", JSON.stringify(data));
            if (typeof(file) !== 'undefined') {
                fd.append("avatar", file);
            }
            if (typeof(tailieu) !== 'undefined') {
                fd.append("tailieu", tailieu);
            }
            if (typeof(tailieu_en) !== 'undefined') {
                fd.append("tailieu_en", tailieu_en);
            }

            showPageLoading();
            $.ajax({
                type: 'POST',
                url: '/api/adminproduct_cats',
                data: fd,
                dataType: 'json',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                error: function(){
                    hidePageLoading();
                    toastr.error(result.error);
                },
                success: function(e) {
                    switch (e.code) {
                        case 200:
                            $('#modal_addrow').modal('hide');
                            $('#form_add').html($scope.form_add);
                            reload();
                            hidePageLoading();
                            break;
                        case 300:
                            hidePageLoading();
                            toastr.error(e.error);
                            break;
                        default:
                            hidePageLoading();
                            break;
                    }
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.btn-edit-row', function(event) {
            var data = {};
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "show";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_product_cat/' + id, data, function(e) {
                $('#form_add').html('');
                $('#form_edit').html(e.data);
                $('#modal_editrow').modal('show');
                tinymce.remove();
                load_tinymce('#content', null);
                load_tinymce('#content_en', null);
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.btn-status', function(event) {
            var data = {};
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "edit_status";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_product_cat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        reload();
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn_editrow').click(function(event) {
            var id_edit = $('#form_edit').attr('data-id');

            var data = {};

            $("#form_edit").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();

            var file = $("#form_edit .file_image").get(0).files[0];
            var tailieu = $("#form_edit .tailieu").get(0).files[0];
            var tailieu_en = $("#form_edit .tailieu_en").get(0).files[0];

            var fd = new FormData();
            fd.append("value", JSON.stringify(data));
            if (typeof(file) !== 'undefined') {
                fd.append("avatar", file);
            }
            if (typeof(tailieu) !== 'undefined') {
                fd.append("tailieu", tailieu);
            }
            if (typeof(tailieu_en) !== 'undefined') {
                fd.append("tailieu_en", tailieu_en);
            }

            fd.append("status_code", 'edit');
            fd.append("_method", 'PUT');

            showPageLoading();
            $.ajax({
                type: 'POST',
                url: '/api/adminproduct_cats/'+id_edit,
                data: fd,
                dataType: 'json',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                error: function(){
                    hidePageLoading();
                    toastr.error(result.error);
                },
                success: function(e) {
                    switch (e.code) {
                        case 200:
                            $('#form_edit').html('');
                            $('#modal_editrow').modal('hide');
                            reload();
                            hidePageLoading();
                            break;
                        case 300:
                            hidePageLoading();
                            toastr.error(e.error);
                            break;
                        default:
                            hidePageLoading();
                            break;
                    }
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.delete_row', function(event) {
            var row_id = $(this).attr('data-id');
            var row = $(this).closest('tr');
            confirmPopup('Xóa product_cat', 'Bạn muốn xóa product_cat không ? ', function() {
                commonService.requestFunction('delete_product_cat/' + row_id + $rootScope.api_token, {}, function(e) {
                    switch (e.code) {
                        case 200:
                            reload();
                            break;
                        default:
                            break;
                    }
                });
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn_status', function(event) {

            var here = $(this);
            var id = $(this).closest('tr').attr('data-id');
            var curenpage = $scope.datatable.page.info().page;

            var data = {};
            data['status_code'] = "change_status";
            data['_method'] = 'PUT';

            commonService.requestFunction('update_product_cat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id){
                                $scope.value[i][6] = e.status;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn-save-row').click(function(event) {
            var data = {};
            var val = $('#nestable-output').val();
            var id = 0;

            data['val'] = val;
            data['status_code'] = 'edit_list';

            commonService.requestFunction('update_product_cat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        
                        break;
                    default:
                        break;
                }
            });
        });

        function reload() {
            $('#nestable').nestable('destroy');
            commonService.requestFunction('index_product_cat', {}, function(e) {
                $('#nestable').html(e.nestable);
                $('.hide_product_cat').html(e.product_cat0);
                $('#nestable').nestable({
                    group: 1,
                    maxDepth: 2
                })
                .on('change', updateOutput);
            });
        }
    }
]);
