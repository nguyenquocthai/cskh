<?php 
    function displayList($list) {
?>
    <ol class="dd-list">
    <?php foreach($list as $item): ?>
        
    <li class="dd-item" data-id="<?php echo $item["id"]; ?>">
        <div class="ctr_action">
            <a data-id="<?php echo $item["id"]; ?>" class="btn btn-xs blue btn-edit-row" title="Sửa">
                <i class="fa fa-edit"></i>
            </a>
            
            <a data-id="<?php echo $item["id"]; ?>" class="btn btn-xs red btn-status" title="Tạm dừng">
                <i class="far fa-window-close"></i>
            </a>
        </div>
        <div class="dd-handle"><?php echo $item["title"]; ?></div>
    <?php if (array_key_exists("children", $item)): ?>
    <?php displayList($item["children"]); ?>
    <?php endif; ?>
    </li>
    <?php endforeach; ?>
    </ol>
<?php
}
?>

<?php displayList($list); ?>