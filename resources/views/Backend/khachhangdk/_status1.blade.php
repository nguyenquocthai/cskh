<?php
    $class_name = '';
    $text = '';

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Đã chốt';
            break;

        case 1:
            $class_name = 'yelow';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Đang code';
            break;
        case 2:
            $class_name = 'red';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Đang trao đổi';
            break;
        case 3:
            $class_name = 'yelow';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Đang test';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span style="width: 100px" title="<?= $title ?>" class="btn_status <?= $class_name ?>">
    <?= $title; ?>
</span>