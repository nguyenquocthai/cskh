<?php
    $now = time(); // or your date as well
    $your_date = strtotime($start);
    $datediff = $now - $your_date;

    $day = round($datediff / (60 * 60 * 24));

    $class_name = '';
    $text = '';
    $status = 0;

    if ($day >= 335) {
        $status = 1;
    }

    if ($day >= 365) {
        $status = 2;
    }

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = $day.' Ngày';
            $title = 'Unactivated';
            break;

        case 1:
            $class_name = 'yelow';
            $text = $day.' Ngày';
            $title = 'Activated';
            break;

        case 2:
            $class_name = 'red';
            $text = $day.' Ngày';
            $title = 'Activated';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span style="width: 70px" class="btn_status <?= $class_name ?>">
    <?= $text; ?>
</span>