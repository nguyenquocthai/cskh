<div class="form-group">
    <label>Name</label>
    <input value="{{$data->name}}" type="text" class="form-control" name="name" readonly>
</div>

<div class="form-group">
    <label class="next-label">Status</label>
    <select class="form-control" ng-required="true" name="status">
        <option @if($data->status == 0) selected @endif value="0">Activated</option>
        <option @if($data->status == 1) selected @endif value="1">Unactivated</option>
    </select>
</div>

<div class="form-group">
    <label class="next-label">Customer reviews</label>
    <textarea class="form-control" name="content" readonly>{{$data->content}}</textarea>
</div>

<div class="form-group">
    <label class="next-label">Note</label>
    <textarea class="form-control" name="note">{{$data->note}}</textarea>
</div>