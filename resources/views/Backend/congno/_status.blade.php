<?php
    $priceiff = $chiphi - $giaidoan1;

    $class_name = '';
    $text = '';
    $status = 0;

    if ($priceiff != $giaidoan2) {
        $status = 1;
    }

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = 'Đã thanh toán';
            $title = 'Unactivated';
            break;

        case 1:
            $class_name = 'red';
            $text = 'Còn '. $priceiff.'$';
            $title = 'Activated';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span style="width: 150px" class="btn_status <?= $class_name ?>">
    <?= $text; ?>
</span>