<?php 
    
    $data->start = BladeGeneral::date($data->start);
    $data_json = json_encode($data);

?>
<div class="form-group">
    <label class="lang_label">
        <span>Họ tên</span>
    </label>

    <input name="title" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Website</span>
    </label>

    <input name="link" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Email</span>
    </label>

    <input name="email" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Đăng ký</span>
    </label>
    <select name="type" class="form-control">
        <option value="">-- Chọn --</option>
        <option value="1">Đã đăng ký</option>
        <option value="0">Chưa đăng ký</option>
    </select>
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Chi phí</span>
    </label>

    <input name="chiphi" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Số điện thoại</span>
    </label>

    <input name="phone" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Ngày bắt đầu</span>
    </label>

    <input name="start" id="start" type="text" class="form-control">
</div>

<div class="form-group">
    <label class="lang_label">
        <span>Ghi chú</span>
    </label>

    <textarea name="summary" class="form-control" rows="5"></textarea>
</div>
<div class="form-group hidden">
    <label class="next-label">Vị trí</label>
    <div class="load_position">
        <div class="position_box" data-id="">
            @if (count($positions) != 0)
                @foreach ($positions as $key => $position)
                <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                @endforeach
            @else
            @endif
        </div>
    </div>
</div>

<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input
</script>