WebWorldApp.controller('users.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Chỉnh sửa user';

        // Title block
        $scope.detail_block_title = 'Chi tiết user';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_user/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/users');
                    return false;
                }
                $scope.user = e.data;

                select2s('#id_usergroup',{
                    commonService: commonService,
                    name:'user_groups',
                    //have_default: true,
                    selected: e.data.id_usergroup,
                });

                select2s('#id_customerlavel',{
                    commonService: commonService,
                    name:'customers_levels',
                    //have_default: true,
                    selected: e.data.id_customerlavel,
                });
                $( "#probationaryday" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                $( "#officialday" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                $( "#birthday" ).datepicker({
                        dateFormat: 'dd/mm/yyyy',

                });
                $( "#restday" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });

                $('.image_review').attr('src', e.data.avatar+'?'+e.data.updated_at);
                
            });

            
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.user.slug = slugify(str);
        };

        //-----------------------
        jQuery(document).ready(function($) {
            $('#phone').keyup(function(event) {

          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;

          // format number
          $(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, "")
            ;
          });
        });
        });

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['name']);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_user/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/users');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
