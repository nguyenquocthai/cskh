<a data-id="{{$id}}" href="/admin/user/edit/{{$id}}" class="btn btn-xs blue btn-edit-row" title="Edit">
	<i class="fa fa-edit"></i>
</a>

<a data-id="{{$id}}" class="btn btn-xs red delete_row" title="Delete"><i class="fa fa-trash-o"></i></a>

