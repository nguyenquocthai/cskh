WebWorldApp.controller('customers_levels.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: phân cấp KH';

        // Title block
        $scope.detail_block_title = 'Chi tiết phân cấp KH';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.customers_level = {};
            $scope.customers_level.status = '0';

            tinymce.remove();
            load_tinymce('#description', null);
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customers_level.slug = slugify(str);
        };
        //-------------------------
        jQuery(document).ready(function($) {
            $('#moneylandmark').keyup(function(event) {

          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;

          // format number
          $(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
          });
        });
        });
        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }
            
            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['name']);
            data['description'] = tinymce.get('description').getContent();
            data['moneylandmark'] = data['moneylandmark'].replace(/,/g,"");
            //console.log(data['moneylandmark']);return false;
            request['noimg'] = 1;
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_customers_level', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customers_levels');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
