WebWorldApp.controller('customers_levels.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Phân cấp KH';

        // Title block
        $scope.detail_block_title = 'Phân cấp KH';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_customers_level/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/customers_levels');
                    return false;
                }
                $scope.customers_level = e.data;

                tinymce.get('description').setContent(html_entity_decode($scope.customers_level.description) || '');

                

                //format number
                function addCommas(nStr)
                {
                    nStr += '';
                    x = nStr.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    return x1 + x2;
                }

                $scope.customers_level.moneylandmark = addCommas($scope.customers_level.moneylandmark);
                
            });
            // Tiny mce
            tinymce.remove();
            load_tinymce('#description', null);

            
        }


        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customers_level.slug = slugify(str);
        };
        //-----------------------
        jQuery(document).ready(function($) {
            $('#moneylandmark').keyup(function(event) {

              // skip for arrow keys
              if(event.which >= 37 && event.which <= 40) return;

              // format number
              $(this).val(function(index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
              });
            });
            });
        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };
        
        
        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['description'] = tinymce.get('description').getContent();
            data['slug'] = slugify(data['name']);
            
            data['moneylandmark'] = data['moneylandmark'].replace(/,/g,"");
            //console.log(data['moneylandmark']);return false;
            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_customers_level/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customers_levels');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
