<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="next-label">Image</label>
            <div class="file_upload_box">
                <input type="file" class="file_image">
                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}" alt="...">
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label>Name</label>
            <input value="{{$data->name}}" type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input value="{{$data->email}}" type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input value="{{$data->phone}}" type="text" class="form-control" name="phone">
        </div>
        <div class="form-group">
            <label class="next-label">Status</label>
            <select class="form-control" ng-required="true" name="status">
                <option @if($data->status == 0) selected @endif value="0">Activated</option>
                <option @if($data->status == 1) selected @endif value="1">Locked</option>
            </select>
        </div>

    </div>
    
</div>