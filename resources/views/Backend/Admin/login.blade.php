<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Login & Register form</title>
  
  <style>
      * {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

html {
  background: #95a5a6;
  background-image: url(http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/dark_embroidery.png);
  font-family: 'Helvetica Neue', Arial, Sans-Serif;
}
html .login-wrap {
  margin: 0 auto;
  background: #ecf0f1;
  width: 350px;
  border-radius: 5px;
  box-shadow: 3px 3px 10px #333;
  padding: 15px;
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
}
html .login-wrap h2 {
  text-align: center;
  font-weight: 200;
  font-size: 2em;
  margin-top: 10px;
  color: #34495e;
}
html .login-wrap .form {
  padding-top: 20px;
}
html .login-wrap .form input,
html .login-wrap .form button {
  height: 40px;
  border-radius: 5px;
  outline: 0;
  width: 100%;
  -moz-outline-style: none;
}
.form-group{
    width: 80%;
    margin-left: 10%;
    margin-bottom: 25px;
}
html .login-wrap .form input[type="text"],
html .login-wrap .form input[type="password"] {
  border: 1px solid #bbb;
  padding: 0 0 0 10px;
  font-size: 14px;
}
html .login-wrap .form input[type="text"]:focus,
html .login-wrap .form input[type="password"]:focus {
  border: 1px solid #3498db;
}
html .login-wrap .form p {
  text-align: center;
  font-size: 10px;
  color: #3498db;
}
html .login-wrap .form a p {
  padding-bottom: 10px;
}
html .login-wrap .form button {
  background: #e74c3c;
  border: none;
  color: white;
  font-size: 18px;
  font-weight: 200;
  cursor: pointer;
  transition: box-shadow .4s ease;
}
html .login-wrap .form button:hover {
  box-shadow: 1px 1px 5px #555;
}
html .login-wrap .form button:active {
  box-shadow: 1px 1px 7px #222;
}
html .login-wrap:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  background: -webkit-linear-gradient(left, #27ae60 0%, #27ae60 20%, #8e44ad 20%, #8e44ad 40%, #3498db 40%, #3498db 60%, #e74c3c 60%, #e74c3c 80%, #f1c40f 80%, #f1c40f 100%);
  background: -moz-linear-gradient(left, #27ae60 0%, #27ae60 20%, #8e44ad 20%, #8e44ad 40%, #3498db 40%, #3498db 60%, #e74c3c 60%, #e74c3c 80%, #f1c40f 80%, #f1c40f 100%);
  height: 5px;
  border-radius: 5px 5px 0 0;
}
        .error{
            font-size: 12px;
            color: red;
            display: block;
        }
    </style>
  
</head>

<body>

    <div class="login-wrap">
        <h2>Admin</h2>
      
        <form class="form" id="form-login">
            <div class="form-group">
                <input type="text" placeholder="Tài khoản" name="username" />
            </div>
            
            <div class="form-group">
                <input type="password" placeholder="Mật khẩu" name="password" />
            </div>

            <div class="form-group">
                <button> Log in </button>
            </div>
            
            <p> Data management. </p>
        </form>
    </div>

    <script src="/public/js/plugins/jquery/jquery-3.2.1.min.js"></script>
    <script src="/public/vendor/jquery-validation-1.16.0/jquery.validate.min.js"></script>

    <script type="text/javascript">
        var form_login = $('#form-login').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass);
            },
            rules: {
                username: 'required',
                password: 'required',
            },
            messages: {
                username: 'Please enter username',
                password: 'Please enter a password',
            },
            submitHandler: function (form) {
                var data = {};
                $("#form-login").serializeArray().map(function(x){data[x.name] = x.value;});

                $.ajax({
                    type: "GET",
                    url: '/api/login?api_token=PBsQwZyv',
                    data: data,
                    dataType: 'json',
                    success: function(result) {
                        switch (result.code) {
                            case 200:
                                $.ajax({
                                    type: "GET",
                                    url: '/api/save_session?api_token=PBsQwZyv',
                                    data: data,
                                    dataType: 'json',
                                    success: function(result) {
                                        switch (result.code) {
                                            case 200:
                                                window.location.reload();
                                                break;
                                        }
                                    }
                                });
                                break;
                            default:
                                alert(result.error);
                                break;
                        }
                    }
                });
                return false;
            }
        });
    </script>

</body>

</html>
