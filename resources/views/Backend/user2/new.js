WebWorldApp.controller('user2s.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới user2';

        // Title block
        $scope.detail_block_title = 'Chi tiết user2';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.user2 = {};
            $scope.user2.status = '1';

            select2s('#id_usergroup',{
                    commonService: commonService,
                    name:'user_groups',
                    have_default: true,
                    //selected: $('#id_customertype').attr('data-select')
                });
            select2s('#id_customerlavel',{
                    commonService: commonService,
                    name:'customers_levels',
                    have_default: true,
                    //selected: $('#id_customertype').attr('data-select')
                });
            $( "#probationaryday" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
            $( "#officialday" ).datepicker({
                dateFormat: 'dd/mm/yyyy',

            });
            $( "#birthday" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

            });
            $( "#restday" ).datepicker({
                dateFormat: 'dd/mm/yyyy',

            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.user2.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['name']);
            data['status'] = 1;

            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_user2', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/user2s');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
