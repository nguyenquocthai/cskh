<?php
    $class_name = '';
    $text = '';

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Làm chính thức';
            break;

        case 1:
            
            $class_name = 'orange';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Thử việc';
            break;

        case 2:
            $class_name = 'red';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Nghỉ việc';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span title="<?= $title ?>" style='color:<?= $class_name ?>' class=" <?= $class_name ?>">
    <?= $title; ?>
</span>