
<div class="col-sm-5">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Khách hàng</span>
                </div>
            </div>

            <div class="portlet-body form">

                <div class="datatable-scroll table-responsive clearfix">
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td>Họ tên</td>
								<td>{{$khachhang->name}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$khachhang->email}}</td>
							</tr>
							<tr>
								<td>Số điện thoại</td>
								<td>{{$khachhang->phone}}</td>
							</tr>
						</tbody>
					</table>
				</div>

            </div>

        </div>
    </div>
    <div class="col-sm-7">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Chi tiết</span>
                </div>
            </div>

            <div class="portlet-body form">

                <div class="datatable-scroll table-responsive clearfix">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Tên sản phẩm</th>
								<th>Hình đại diện</th>
								<th>Số lượng</th>
							</tr>
						</thead>
						<tbody>
							@if (count($carts) != 0)
							    @foreach ($carts as $cart)
							    <tr>
									<td>{{@$cart->item_project->title}}</td>
									<td>
										<div class="table_img">
											<img alt="..." src="{{BladeGeneral::GetImg(['avatar' => @$cart->item_project->avatar,'data' => 'item_project', 'time' => @$cart->item_project->updated_at])}}">
										</div>
									</td>
									<td>{{$cart->soluong}}</td>
								</tr>
							    @endforeach
							@else
							
							@endif
						</tbody>
					</table>
				</div>

            </div>

        </div>
    </div>