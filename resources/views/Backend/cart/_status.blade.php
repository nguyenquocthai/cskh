<?php
    $class_name = '';
    $text = '';

    switch ($status) {
        case 0:
            $class_name = 'red';
            $text = 'Chưa xem';
            $title = 'Unactivated';
            break;

        case 1:
            $class_name = 'green';
            $text = 'Đã xem';
            $title = 'Activated';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span style="width: 70px" title="<?= $title ?>" class="btn_status <?= $class_name ?>">
    <?= $text; ?>
</span>