WebWorldApp.controller('chungnhans.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Chỉnh sửa: chứng nhận';

        // Title block
        $scope.detail_block_title = 'Chi tiết chứng nhận';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_chungnhan/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/chungnhans');
                    return false;
                }
                $scope.chungnhan = e.data;

                $('.image_review').attr('src', e.data.avatar+'?'+e.data.updated_at);
                tinymce.get('content').setContent(html_entity_decode($scope.chungnhan.content) || '');
                tinymce.get('content_en').setContent(html_entity_decode($scope.chungnhan.content_en) || '');

            });

            // Tiny mce
            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.chungnhan.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_chungnhan/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/chungnhans');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
