<?php
    $now = time(); // or your date as well
    $your_date = strtotime($startcode);
    $datediff = $now - $your_date;

    $day = round($datediff / (60 * 60 * 24));

    $class_name = '';
    $text = '';
    $status = 0;

    if ($day < 0) {
        $status = 1;
    }

    switch ($status) {
        case 0:
            $class_name = 'green';
            $text = $day.' Ngày';
            $title = 'Unactivated';
            break;

        case 1:
            $class_name = 'red';
            $text = 'Hết hạn '. abs($day).' Ngày';
            $title = 'Activated';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span style="width: 150px" class="btn_status <?= $class_name ?>">
    <?= $text; ?>
</span>