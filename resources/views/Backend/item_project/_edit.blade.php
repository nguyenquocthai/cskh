<div class="form-group">
    <label>Tên sản phẩm</label>
    <input value="{{$data->title}}" type="text" class="form-control" name="title">
</div>

<div class="form-group">
    <label class="next-label">Trạng thái</label>
    <select id="active_post" class="form-control" ng-required="true" name="status">
        <option value="0">Kích hoạt </option>
        <option value="1">Tạm dừng</option>
    </select>
    <script>
        $('#active_post').val('{{$data->status}}').trigger('change');
    </script>
</div>

<div class="form-group">
    <label class="next-label">Location</label>
    <div class="load_position">
        <div class="position_box" data-id="">
            @if (count($positions) != 0)
                @foreach ($positions as $key => $position)
                <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                @endforeach
            @else
            @endif
        </div>
    </div>
</div>