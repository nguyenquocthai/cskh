WebWorldApp.controller('item_projects.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: sản phẩm';

        // Title block
        $scope.detail_block_title = 'Chi tiết sản phẩm';
        $scope.image_block_title = 'Hình ảnh';
        $scope.category_block_title = 'Phân loại';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // Catlist
            commonService.requestFunction('index_product_cat', {status_code:'getcat',id:0}, function(e) {
                $('.load_cat').html(e.nestable);
            });

            $scope.item_project = {};
            $scope.item_project.status = '0';

            select2s('#thuonghieu',{
                commonService: commonService,
                name:'thuonghieus',
                have_default: true,
            });

            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        //-------------------------------------------------------------------------------
        $('.load_cat').on('click', '.i_', function(event) {
            $('.dd-handle').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.pari_').find('.pari_i_:first-child .i_i_').addClass('active');
        });

        $('.load_cat').on('click', '.i_i_', function(event) {
            $('.i_i_').removeClass('active');
            $('.i_').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.pari_').find('.i_').addClass('active');
        });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.item_project.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];

            var cat_id = [];
            $('.dd-handle.active').each(function(index, el) {
                cat_id.push($(this).attr('data-id'));
            });

            files['avatar'] = $("#form_box .file_image").get(0).files[0];
            files['tailieu'] = $("#form_box .tailieu").get(0).files[0];
            files['tailieu_en'] = $("#form_box .tailieu_en").get(0).files[0];

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();

            data['id_product_cat'] = JSON.stringify(cat_id);

            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl2(files, request, 'create_item_project', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/item_projects');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        

        //-------------------------------------------------------------------------------
        $('.addprice').click(function(event) {
            var id_culy = $('#id_distance').val();
            var mark_culy = $("#id_distance option:selected").text();
            var price = $('#price').val();
            var list_idcl = [];

            $('.item_price').each(function(index, el) {
                list_idcl.push($(this).attr('data-culy'));
            });

            if(id_culy == 0){
                toastr.error('Chưa chọn cự lý.');
                return false;
            } else if(jQuery.inArray(id_culy, list_idcl) !== -1){
                toastr.error('Đã tồn tại.');
                return false;
            } else if(price == ""){
                toastr.error('Chưa nhập giá.');
                return false;
            }

            var data = '<tr data-culy="'+id_culy+'" class="item_price">'+
                            '<td class="culy">'+mark_culy+'</td>'+
                            '<td class="chiphi">'+price+'</td>'+
                            '<td><span class="xoaprice"> <i class="glyphicon glyphicon-remove"></i></span></td>'+
                        '</tr>';

            $('.load_price').append(data);
        });

        $('.load_price').on('click', '.xoaprice', function(event) {
            $(this).closest('tr').remove();
        });
    }
]);
