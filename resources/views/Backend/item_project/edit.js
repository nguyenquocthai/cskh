WebWorldApp.controller('item_projects.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Quản trị sản phẩm';

        // Title block
        $scope.detail_block_title = 'Chi tiết sản phẩm';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_item_project/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/item_projects');
                    return false;
                }
                $scope.item_project = e.data;

                select2s('#thuonghieu',{
                    commonService: commonService,
                    name:'thuonghieus',
                    have_default: true,
                    selected: e.data.id_thuonghieu,
                    //title: 'code',
                    //limit: 100,
                    //where: ['type,=,1'],
                });

                $('.image_review').attr('src', e.data.avatar+'?'+e.data.updated_at);
                tinymce.get('content').setContent(html_entity_decode($scope.item_project.content) || '');
                tinymce.get('content_en').setContent(html_entity_decode($scope.item_project.content_en) || '');

            });

            // Tiny mce
            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        // Catlist
        commonService.requestFunction('index_product_cat', {status_code:'getcat',id:$routeParams.id}, function(e) {

            $('.load_cat').html(e.nestable);

        });

        $('.load_cat').on('click', '.i_', function(event) {
            $('.dd-handle').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.pari_').find('.pari_i_:first-child .i_i_').addClass('active');
        });

        $('.load_cat').on('click', '.i_i_', function(event) {
            $('.i_i_').removeClass('active');
            $('.i_').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.pari_').find('.i_').addClass('active');
        });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.item_project.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var keyword = $('#keyword').val() || '';
            $scope.datatable.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.search_datatable2 = function() {
            var keyword = $('#keyword2').val() || '';
            $scope.datatable2.search(keyword).draw();
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var cat_id = [];
            $('.dd-handle.active').each(function(index, el) {
                cat_id.push($(this).attr('data-id'));
            });

            var request = {};
            var data = {};
            var files = [];

            var id_edit = $routeParams.id;

            files['avatar'] = $("#form_box .file_image").get(0).files[0];
            files['tailieu'] = $("#form_box .tailieu").get(0).files[0];
            files['tailieu_en'] = $("#form_box .tailieu_en").get(0).files[0];


            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();
            data['id_product_cat'] = JSON.stringify(cat_id);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl2(files, request, 'update_item_project/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/item_projects');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
