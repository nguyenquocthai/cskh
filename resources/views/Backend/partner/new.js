WebWorldApp.controller('partners.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới đối tác';

        // Title block
        $scope.detail_block_title = 'Chi tiết đối tác';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.partner = {};
            //$scope.partner.status = '0';

            select2s('#id_customertype',{
                    commonService: commonService,
                    name:'customer_types',
                    have_default: true,
                });
            $( "#datepicker" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
            $( "#datepicker1" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.partner.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            request['noimg'] = 1;
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_partner', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/partners');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
