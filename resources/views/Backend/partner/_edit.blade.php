<?php $data_json = json_encode($data);?>
<div class="row">

    <div class="col-sm-12">

        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Thông tin</span>
                </div>

                

            </div>

            <div class="portlet-body form">
                <div class="form-group">
                    <label class="lang_label">
                        <span>Tên đối tác</span>
                        
                    </label>

                    <input name="name" data-lang="vn" type="text" placeholder="" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="next-label">Loại</label>
                    <select class="form-control" name="id_customertype" id="id_customertype"></select>
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Số điện thoại</span>
                        
                    </label>

                    <input name="phone" data-lang="vn" type="text" placeholder="" class="form-control value_lang active">
                    
                </div>
                <div class="form-group">
                    <label class="lang_label">
                        <span>Email</span>
                        
                    </label>

                    <input name="email" data-lang="vn" type="text" placeholder="" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Địa chỉ</span>
                        
                    </label>

                    <input name="address" data-lang="vn" type="text" placeholder="" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Ngày sinh</span>
                        
                    </label>

                    <input name="birthday" data-lang="vn" type="text" data-language='en' id="datepicker"  placeholder="" class="form-control value_lang active datepicker-here" readonly>
                    
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Ngày bắt đầu</span>
                        
                    </label>

                    <input name="startdate" data-lang="vn" type="text" id="datepicker1" data-language='en' class="form-control value_lang active datepicker-here" readonly>
                    
                </div>

                <div class="form-group">
                    <label class="next-label">Trạng thái</label>
                    <select class="form-control" name="status">
                        
                        <option value="0">Kích hoạt</option>
                        <option value="1">Tạm dừng</option>
                    </select>
                </div>

                

            </div>

        </div>

    </div>


</div>



<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
        $(this).attr('data-select', data_json[name]);
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input
</script>