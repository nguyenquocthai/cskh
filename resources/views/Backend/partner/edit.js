WebWorldApp.controller('partners.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Đối tác';

        // Title block
        $scope.detail_block_title = 'Đối tác';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_partner/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/partners');
                    return false;
                }
                $scope.partner = e.data;

                select2s('#id_customertype',{
                    commonService: commonService,
                    name:'customer_types',
                    //have_default: true,
                    selected: e.data.id_customertype,
                });

                $( "#datepicker" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                $( "#datepicker1" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                
            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.partner.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_partner/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/partners');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
