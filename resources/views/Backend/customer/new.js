WebWorldApp.controller('customers.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới customer';

        // Title block
        $scope.detail_block_title = 'Chi tiết customer';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.customer = {};
            $scope.customer.status = '0';

            tinymce.remove();
            load_tinymce('#description', null);

            select2s('#id_partner',{
                commonService: commonService,
                name:'partners',
                have_default: true,
                // selected: e.data.id_partner,
            });

            select2s('#id_user',{
                commonService: commonService,
                name:'users',
                have_default: true,
                // selected: e.data.id_user,
            });
            select2s('#id_usergroup',{
                commonService: commonService,
                name:'user_groups',
                have_default: true,
                // selected: e.data.id_usergroup,
            });

            select2s('#id_service',{
                commonService: commonService,
                name:'services',
                have_default: true,
                // selected: e.data.id_service,
            });
            select2s('#id_country',{
                commonService: commonService,
                name:'customer_countries',
                have_default: true,
                // selected: e.data.id_country,
            });
            $( "#startdate" ).datepicker({
                dateFormat: 'dd/mm/yyyy',

            });
            $( "#expirationdate" ).datepicker({
                dateFormat: 'dd/mm/yyyy',

            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customer.slug = slugify(str);
        };

        //-----------------------
        jQuery(document).ready(function($) {
            $('#phone').keyup(function(event) {

          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;
          // format number
          $(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, "")
            ;
          });

          
        });
        });

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['name']);
            data['description'] = tinymce.get('description').getContent();
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_customer', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customers');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
