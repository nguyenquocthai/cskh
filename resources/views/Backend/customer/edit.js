WebWorldApp.controller('customers.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Chỉnh sửa customer';

        // Title block
        $scope.detail_block_title = 'Chi tiết customer';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_customer/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/customers');
                    return false;
                }
                $scope.customer = e.data;

                console.log($scope.customer);

                $('.db-note').html($scope.customer.viewrender);

                select2s('#id_partner',{
                    commonService: commonService,
                    name:'partners',
                    //have_default: true,
                    selected: e.data.id_partner,
                });

                select2s('#id_user',{
                    commonService: commonService,
                    name:'users',
                    //have_default: true,
                    selected: e.data.id_user,
                });
                select2s('#id_usergroup',{
                    commonService: commonService,
                    name:'user_groups',
                    //have_default: true,
                    selected: e.data.id_usergroup,
                });

                select2s('#id_service',{
                    commonService: commonService,
                    name:'services',
                    //have_default: true,
                    selected: e.data.id_service,
                });
                select2s('#id_country',{
                    commonService: commonService,
                    name:'customer_countries',
                    //have_default: true,
                    selected: e.data.id_country,
                });
                $( "#startdate" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                $( "#expirationdate" ).datepicker({
                    dateFormat: 'dd/mm/yyyy',

                });
                tinymce.get('description').setContent(html_entity_decode($scope.customer.description) || '');


                $('.image_review').attr('src', e.data.avatar+'?'+e.data.updated_at);

                var mohinh = e.data.enterprise;
                if(mohinh == '1'){
                    $("#dvPassport").show();
                }
                else{
                    $("#dvPassport").hide();
                }
                
            });
            // Tiny mce
            tinymce.remove();
            load_tinymce('#description', null);
        }

        //-------------------------------------------------------------------------------
        $('.btn_addrow_note').click(function(event) {

            var id_edit = $routeParams.id;
            var request = {};
            var data = {};
            var files = [];

            $("#form_add_note").serializeArray().map(function(x){data[x.name] = x.value;});
            data['name_note'] = $scope.customer.name_note;
            data['id_customer'] = $routeParams.id;
            request['noimg'] = 1;
            request['value'] = data;
            request['status_code'] = 'create_note';
            request['_method'] = "PUT";
            fileUpload.uploadFileToUrl(files, request, 'update_customer/'+ id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $("#name_note").val("");
                        location.reload();
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.db-note').on('click', '.delete_name_note', function(event) {
            var row_id = $(this).attr('data-id')+'note';
            var type = 'note';
            var row = $(this).closest('.item-note');
            confirmPopup('Xóa ghi chú', 'Bạn có muốn xóa ghi chú này không? ', function() {
                commonService.requestFunction('delete_customer/' + row_id + $rootScope.api_token, {}, function(e) {
                    switch (e.code) {
                        case 200:
                            location.reload();
                            break;
                        default:
                            break;
                    }
                });
            });
        });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customer.slug = slugify(str);
        };



        //-----------------------
        jQuery(document).ready(function($) {
            $('#phone').keyup(function(event) {

          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;

          // format number
          $(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, "")
            ;
          });
        });
        });

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['name']);
            data['description'] = tinymce.get('description').getContent();
            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_customer/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customers');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
