WebWorldApp.controller('customer_types.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới Loại khách hàng';

        // Title block
        $scope.detail_block_title = 'Chi tiết Loại khách hàng';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.customer_type = {};
            $scope.customer_type.status = '0';

            select2s('#id_country',{
                    commonService: commonService,
                    name:'customer_countries',
                    have_default: true,
                });
            select2s('#id_customerlevel',{
                    commonService: commonService,
                    name:'customers_levels',
                    have_default: true,
                    
                });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customer_type.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            request['noimg'] = 1;
            

            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_customer_type', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customer_types');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
