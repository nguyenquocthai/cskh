<?php $data_json = json_encode($data);?>
<div class="row">

    <div class="col-sm-12">

        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Thông tin</span>
                </div>

                

            </div>

            <div class="portlet-body form">
                <div class="form-group">
                    <label class="lang_label">
                        <span>Tên loại</span>
                        
                    </label>

                    <input name="name" data-lang="vn" type="text" placeholder="Tiêu đề" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="next-label">Quốc gia</label>
                    <select class="form-control" name="id_country" id="id_country"></select>
                </div>

                <div class="form-group">
                    <label class="next-label">Level</label>
                    <select class="form-control" name="id_customerlevel" id="id_customerlevel"></select>
                </div>

                <div class="form-group">
                    <label class="next-label">Trạng thái</label>
                    <select class="form-control" name="status">
                        <option value="0">Kích hoạt</option>
                        <option value="1">Tạm dừng</option>
                    </select>
                </div>
                

            </div>

        </div>

    </div>


</div>



<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
        $(this).attr('data-select', data_json[name]);
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input
</script>