<?php $data_json = json_encode($data);?>
<div class="row">

    <div class="col-sm-12">

        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Thông tin</span>
                </div>

                

            </div>

            <div class="portlet-body form">
                <div class="form-group">
                    <label class="lang_label">
                        <span>Tên dịch vụ</span>
                        
                    </label>

                    <input name="name" data-lang="vn" type="text" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="lang_label">
                        <span>Giá</span>
                        
                    </label>

                    <input name="price" id="price_service" data-lang="vn" type="text" class="form-control value_lang active">
                    
                </div>

                <div class="form-group">
                    <label class="next-label">Quốc gia</label>
                    <select class="form-control" name="id_country" id="id_country"></select>
                </div>

                <div class="form-group">
                    <label class="next-label">User</label>
                    <select class="form-control" name="id_user" id="id_user"></select>
                </div>

                <div class="form-group">
                    <label class="next-label">Trạng thái</label>
                    <select class="form-control" name="status">
                        <option value="0">Kích hoạt</option>
                        <option value="1">Tạm dừng</option>
                    </select>
                </div>
                

            </div>

        </div>

    </div>


</div>



<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
        $(this).attr('data-select', data_json[name]);
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input

    //format number
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    var price_service = addCommas($('#form_edit #price_service').val());

    //-----------------------
    $('#price_service').keyup(function(event) {

      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
</script>