WebWorldApp.controller('customer_types.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Loại khách hàng';

        // Title block
        $scope.detail_block_title = 'Loại khách hàng';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_customer_type/' + $routeParams.id, {}, function(e) {
                if (!e.data) {
                    $location.path('/admin/customer_types');
                    return false;
                }
                $scope.customer_type = e.data;

                select2s('#id_country',{
                    commonService: commonService,
                    name:'customer_countries',
                    have_default: true,
                    selected: e.data.id_country,
                });

                select2s('#id_customerlevel',{
                    commonService: commonService,
                    name:'customers_levels',
                    have_default: true,
                    selected: e.data.id_customerlevel,
                });
            });
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.customer_type.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            fileUpload.uploadFileToUrl(files, request, 'update_customer_type/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/customer_types');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
