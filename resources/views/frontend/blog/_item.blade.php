<article class="about-item style2">
    <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $blog->avatar,'data' => 'blog', 'time' => $blog->updated_at])}}" alt=""><a class="link" href="/chi-tiet-tin/{{$blog->slug}}" title="{{$blog->title}}"></a></figure>
    <div class="content">
        <h2 class="title"><a class="link" href="/chi-tiet-tin/{{$blog->slug}}" title="{{$blog->title}}">{{$blog->title}}</a></h2>
        <p class="summary">{{$blog->summary}}</p>
    </div>
</article>