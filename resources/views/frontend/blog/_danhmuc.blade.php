<div class="aside-block">
    <div class="aside-nav">
        <h4 class="title style2">{{@$langs['tin-tuc']}}</h4>
        <ul class="reset-list aside-nav__list">
            @if (count($blog_cats) != 0)
                @foreach ($blog_cats as $blog_cat)
                <li class="item">
                    <div class="collapse-title">
                        <button class="reset-btn collapse-btn collapsed"></button><a class="collapse-link {{@$danhmuc_active[$blog_cat->id]}}" href="/danh-muc-tin-tuc/{{$blog_cat->slug}}">{{$blog_cat->title}}</a>
                    </div>
                </li>
                @endforeach
            @else
           
            @endif
        </ul>
    </div>
</div>