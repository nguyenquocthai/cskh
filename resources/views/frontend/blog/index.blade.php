@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['tin-tuc']}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>{{@$langs['tro-ve']}}</button>
                <div class="aside-wrap js-blurOff">
                    @include('frontend.blog._danhmuc')
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <h1 class="title-detail">{{$blog_cats[0]->title}}</h1>

                @if (count($blogs) != 0)
                    @foreach ($blogs as $blog)
                    @include('frontend.blog._item')
                    @endforeach
                @else
                
                @endif

                {{ $blogs->fragment('foo')->links() }}
                
                <!-- <nav class="paginationk">
                    <ul class="reset-list paginationk-list">
                        <li class="item"><a class="link" href="#!"></a></li>
                        <li class="item"><a class="link" href="#!">1</a></li>
                        <li class="item active"><a class="link" href="#!">2</a></li>
                        <li class="item"><a class="link" href="#!">3</a></li>
                        <li class="item dots"><a class="link" href="#!">...</a></li>
                        <li class="item"><a class="link" href="#!">12</a></li>
                        <li class="item"><a class="link" href="#!"></a></li>
                    </ul>
                </nav> -->
            </div>
        </div>
    </div>
</main>
@endsection