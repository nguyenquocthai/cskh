@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/tin-tuc">{{@$langs['tin-tuc']}}</a></li>
                <li class="item"><a class="link" href="/danh-muc-tin-tuc/{{@$blog_cat->slug}}">{{@$blog_cat->title}}</a></li>
                <li class="item active">{{$blog->title}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>{{@$langs['tro-ve']}}</button>
                <div class="aside-wrap js-blurOff">
                    @include('frontend.blog._danhmuc')
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <article class="detail">
                    <h1 class="title-detail">{{$blog->title}}</h1>
                    <div class="content">{!!$blog->content!!}</div>
                    <div class="social-share right">
                        <div class="text">{{@$langs['chia-se']}}:</div>
                        <ul class="reset-list list">
                            <li class="item"><a class="link" href="#!"><i class="fa fa-facebook-official cl_fb"></i></a></li>
                            <li class="item"><a class="link" href="#!"><i class="fa fa-twitter-square cl_tt"></i></a></li>
                            <li class="item"><a class="link" href="#!"><i class="fa fa-google-plus-square cl_gg"></i></a></li>
                        </ul>
                    </div>
                    @if (count($blogs) != 0)
                    <div class="news-related">
                        <h1 class="title-detail">BẢNG TIN KHÁC</h1>
                        @foreach ($blogs as $blog)
                        @include('frontend.blog._item')
                        @endforeach
                    </div>
                    @else
                        
                    @endif
                </article>
            </div>
        </div>
    </div>
</main>
@endsection