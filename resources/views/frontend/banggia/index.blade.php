@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['bang-gia']}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>{{@$langs['tro-ve']}}</button>
                <div class="aside-wrap js-blurOff">
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <article class="about">
                    <h1 class="title-detail">{{$banggia->title}}</h1>
                    <div class="content">
                        {!!$banggia->content!!}
                    </div>

                    @if (count($banggias) != 0)
                    <div class="news-related">
                        <h1 class="title-detail">{{@$langs['bai-viet-khac']}}</h1>
                        @foreach ($banggias as $banggia)
                        <article class="about-item style2">
                            <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $banggia->avatar,'data' => 'banggia', 'time' => $banggia->updated_at])}}" alt=""><a class="link" href="/chi-tiet-bang-gia/{{$banggia->slug}}" title=""></a></figure>
                            <div class="content">
                                <h2 class="title"><a class="link" href="/chi-tiet-bang-gia/{{$banggia->slug}}" title="">{{$banggia->title}}</a></h2>
                                <p class="summary">{{$banggia->summary}}</p>
                            </div>
                        </article>
                        @endforeach
                    </div>
                    @else
                        
                    @endif
                </article>
            </div>
        </div>
    </div>
</main>
@endsection