<div class="aside-block">
    <div class="aside-nav">
        <h4 class="title style2">{{@$langs['gioi-thieu']}}</h4>
        <ul class="reset-list aside-nav__list">
            <li class="item">
                <div class="collapse-title">
                    <button class="reset-btn collapse-btn collapsed"></button><a class="collapse-link {{@$active_about}}" href="/gioi-thieu">{{@$langs['gioi-thieu']}}</a>
                </div>
            </li>

            <li class="item">
                <div class="collapse-title">
                    <button class="reset-btn collapse-btn collapsed"></button><a class="collapse-link {{@$active_chungnhan}}" href="/chung-nhan">{{@$langs['chung-nhan']}}</a>
                </div>
            </li>

            <li class="item">
                <div class="collapse-title">
                    <button class="reset-btn collapse-btn collapsed"></button><a class="collapse-link {{@$active_tuyendung}}" href="/tuyen-dung">{{@$langs['tuyen-dung']}}</a>
                </div>
            </li>
        </ul>
    </div>
</div>