@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['tim-kiem']}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>Trở về</button>
                <div class="aside-wrap js-blurOff">
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <h1 class="title-detail">{{@$langs['tu-khoa']}}: {{$keyword}}</h1>
                @if (count($item_projects) != 0)
                    @foreach ($item_projects as $item_project)
                    @include('frontend.item_project._items')
                    @endforeach
                @else
                <i>{{@$langs['khong-tim-thay']}}</i>
                @endif
            </div>
        </div>
    </div>
</main>
@endsection