<!-- NAVIGATION-->
<nav class="navigation js-nav">
    <div class="container">
        <button class="reset-btn navigation-close"><i class="fa fa-chevron-left mr-2"></i>Trở về</button>
        <div class="navigation-wrap js-blurOff">
            <ul class="reset-list navigation-list">
                <li class="item"><a class="link" id="index" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/">{{@$langs['trang-chu']}}</a></li>
                <li class="item"><a class="link" href="/gioi-thieu">{{@$langs['gioi-thieu']}}</a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham">{{@$langs['san-pham']}}</a></li>
                <li class="item"><a class="link" href="/dich-vu">{{@$langs['dich-vu']}}</a></li>
                <li class="item"><a class="link" href="/bang-gia">{{@$langs['bang-gia']}}</a></li>
                <li class="item"><a class="link" href="/ho-tro">{{@$langs['ho-tro']}}</a></li>
                <li class="item"><a class="link" href="/tin-tuc">{{@$langs['tin-tuc']}}</a></li>
                <li class="item"><a class="link" href="/lien-he">{{@$langs['lien-he']}}</a></li>
                <li class="item">
                    @if(@session('lang') == '')
                    <div class="dropdown language active{{@session('lang')}}">
                        <button class="reset-btn language-btn" id="dropdownLanguage" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/public/theme/img/root/lang-vi.png" alt=""><span>Tiếng việt</span></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                            <a class="dropdown-item ctr_lan" data-lang="_en" href="#!"><img src="/public/theme/img/root/lang-en.png" alt=""><span>English</span></a>
                        </div>
                    </div>
                    @else
                    <div class="dropdown language active{{@session('lang')}}">
                        <button class="reset-btn language-btn" id="dropdownLanguage" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/public/theme/img/root/lang-en.png" alt=""><span>English</span></button>
                        <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                            <a class="dropdown-item ctr_lan" data-lang="" href="#!"><img src="/public/theme/img/root/lang-vi.png" alt=""><span>Tiếng Việt</span></a>
                        </div>
                    </div>
                    @endif
                    <ul class="reset-list social-list">
                        <li class="item"><a class="link" href="{{@$info_web['facebook']}}"><i class="fa fa-facebook"></i></a></li>
                        <li class="item"><a class="link" href="{{@$info_web['instagram']}}"><i class="fa fa-instagram"></i></a></li>
                        <li class="item"><a class="link" href="{{@$info_web['google']}}"><i class="fa fa-youtube-square"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- HEADER-->
<header class="header">
    <div class="container header-top">
        <figure class="header-logo hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => @$info_web['logo_white'],'data' => 'setting', 'time' => @$info_web['updated_at']])}}" alt="{{@$info_web['name']}}"><a class="link" href="/" title="{{@$info_web['name']}}"></a></figure>
        <div class="header-right">
            <div class="header-contact">
                <div class="item contact-group"><a class="link" href="mailto:{{@$info_web['email']}}"><i class="fa fa-envelope"></i>{{@$info_web['email']}}</a><a class="link" href="tel:{{@$info_web['phone']}}"><i class="fa fa-phone-in-talk"></i>{{@$info_web['phone']}}</a></div>
                <div class="item log">
                    @if(!session('userSession'))
                    <a class="link" href="/create-account"><i class="fa fa-sign-in"></i>{{@$langs['dang-nhap']}}</a><a class="link" href="#!"><i class="fa fa-user"></i>{{@$langs['dang-ky']}}</a>
                    @else
                    <a class="link" href="/create-account"><i class="fa fa-user"></i>{{session('userSession')->name}}</a>
                    <a class="link" href="/logout"><i class="fa fa-sign-out"></i>{{@$langs['dang-xuat']}}</a>
                    @endif
                </div>
            </div>
            <div class="header-search">
                <form class="form-search" action="/tim-kiem">
                    <input name="keyword" class="form-control input" type="text" placeholder="{{@$langs['nhap-tim-kiem']}}">
                    <button class="reset-btn button"><i class="fa fa-search"></i>{{@$langs['tim-kiem']}}</button>
                </form>
                <div class="cart">
                    <div class="cart-icon show_cart"><span class="quantity count_cart">0</span></div>
                    <div class="cart-text"><span class="fb">{{@$langs['gio-hang-cua-ban']}}</span><span><b class="count_cart">0</b>&nbsp;{{@$langs['san-pham']}}</span></div>
                </div>
                <button class="reset-btn navigation-open js-navTrigger"><i class="fa fa-bars"></i></button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-bot">
            <div class="row">
                <div class="col-md-4 col-lg-3"><a class="link title js-asideTrigger" href="#!">{{@$langs['danh-muc-san-pham']}}</a></div>
                <div class="col-md-8 col-lg-9">
                    <ul class="reset-list list">
                        <li class="item"><a class="link" href="/thuong-hieu">{{@$langs['thuong-hieu']}}</a></li>

                        @if (count($thuonghieus) != 0)
                            @foreach ($thuonghieus as $thuonghieu)
                            <li class="item"><a class="link" href="/thuong-hieu/{{$thuonghieu->slug}}"><img src="{{BladeGeneral::GetImg(['avatar' => $thuonghieu->avatar,'data' => 'thuonghieu', 'time' => $thuonghieu->updated_at])}}" alt=""></a></li>
                            @endforeach
                        @else
                        
                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- MAIN CONTENT-->

<script>
    $('.ctr_lan').click(function(event) {
        event.preventDefault();
        var lang = $(this).attr('data-lang');
        var data = {};
        data['lang'] = lang;
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/chang_lang',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                $('.block-page-all').removeClass('active');
                location.reload();
            }
        });
    });
</script>