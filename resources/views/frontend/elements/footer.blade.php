<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 footer-col">
                <div class="footer-block">
                    <h4 class="footer-title">
                        <button class="reset-btn collapse-btn collapsed" data-toggle="collapse" data-target="#collapseFooterMenu1">{{@$langs['ve-chung-toi']}}</button>
                    </h4>
                    <div class="collapse" id="collapseFooterMenu1">
                        <ul class="reset-list footer-menu">
                            <li class="item"><a class="link" href="/gioi-thieu">{{@$langs['gioi-thieu']}}</a></li>
                            <li class="item"><a class="link" href="/chung-nhan">{{@$langs['chung-nhan']}}</a></li>
                            <li class="item"><a class="link" href="/tuyen-dung">{{@$langs['tuyen-dung']}}</a></li>
                            <li class="item"><a class="link" href="/bang-gia">{{@$langs['bang-gia']}}</a></li>
                            <li class="item"><a class="link" href="/tin-tuc">{{@$langs['tin-tuc']}}</a></li>
                            <li class="item cl_red"><a class="link" href="/lien-he">{{@$langs['gop-y-phan-anh']}}</a><a class="link tel" href="tel:{{@$info_web['phone']}}">{{@$info_web['phone']}}</a></li>
                        </ul>
                    </div>
                </div>
                <!-- PAYMENT ON DESKTOP-->
                <div class="footer-block d-none d-lg-block">
                    <h4 class="footer-title">{{@$langs['thong-tin-thanh-toan']}}</h4>
                    <ul class="reset-list payment-list">
                        <li class="item"><a class="item" href="/chi-tiet-ho-tro/phuong-thuc-thanh-toan"><img src="/public/theme/img/logo-thanh-toan.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 footer-col">
                <div class="footer-block">
                    <h4 class="footer-title">
                        <button class="reset-btn collapse-btn collapsed" data-toggle="collapse" data-target="#collapseFooterMenu2">{{@$langs['san-pham']}}</button>
                    </h4>
                    <div class="collapse" id="collapseFooterMenu2">
                        <ul class="reset-list footer-menu">
                            @if (count($product_cats) != 0)
                                @foreach ($product_cats as $product_cat)
                                    @if($product_cat->id_parent == 0)
                                    <li class="item"><a class="link" href="/danh-muc-san-pham/{{$product_cat->slug}}">{{$product_cat->title}}</a></li>
                                    @endif
                                @endforeach
                            @else
                            
                            @endif
                            
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 footer-col">
                <div class="footer-block">
                    <h4 class="footer-title">
                        <button class="reset-btn collapse-btn collapsed" data-toggle="collapse" data-target="#collapseFooterMenu3">{{@$langs['ho-tro']}}</button>
                    </h4>
                    <div class="collapse" id="collapseFooterMenu3">
                        <ul class="reset-list footer-menu">
                            @if (count($hotros) != 0)
                                @foreach ($hotros as $hotro)
                                <li class="item"><a class="link" href="/chi-tiet-ho-tro/{{$hotro->slug}}">{{$hotro->title}}</a></li>
                                @endforeach
                            @else
                            
                            @endif
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 footer-col">
                <div class="footer-block">
                    <h4 class="footer-title">{{@$langs['lien-he']}}</h4>
                    <ul class="reset-list footer-info">
                        <li class="item">
                            <i class="fa fa-home"></i>
                            <p class="text">{{@$info_web['address']}}</p>
                        </li>
                        <li class="item">
                            <i class="fa fa-phone"></i>
                            <p class="text"><a class="link" href="tel:{{@$info_web['phone']}}">{{@$info_web['phone']}}</a></p>
                        </li>
                        <li class="item">
                            <i class="fa fa-bank"></i>
                            <p class="text">MST: {{@$info_web['mst']}}</p>
                        </li>
                        <li class="item">
                            <i class="fa fa-envelope"></i>
                            <p class="text"><a class="link" href="mailto:{{@$info_web['email']}}">{{@$info_web['email']}}</a></p>
                        </li>
                        <li class="item">
                            <i class="fa fa-chrome"></i>
                            <p class="text"><a class="link" href="{{@$info_web['facebook']}}">{{@$info_web['facebook']}}</a></p>
                        </li>
                    </ul>
                </div>
                <!-- PAYMENT ON MOBILE, TABLET-->
                <div class="footer-block d-block d-lg-none">
                    <h4 class="footer-title">{{@$langs['thong-tin-thanh-toan']}}</h4>
                    <ul class="reset-list payment-list">
                        <li class="item"><a class="item" href="/chi-tiet-ho-tro/phuong-thuc-thanh-toan"><img src="/public/theme/img/logo-thanh-toan.png" alt=""></a></li>
                    </ul>
                </div>
                <!-- HAVE REGISTERED-->
                <figure class="img img-haveRegistered"><a class="link" href="/lien-he"><img src="/public/theme/img/logo-bo-cong-thuong.png" alt=""></a></figure>
            </div>
        </div>
    </div>
    <div class="footer-bot">
        <div class="container">
            <ul class="reset-list social-list">
                <li class="item"><a class="link" href="{{@$info_web['facebook']}}"><i class="fa fa-facebook"></i></a></li>
                <li class="item"><a class="link" href="{{@$info_web['google']}}"><i class="fa fa-youtube-square"></i></a></li>
                <li class="item"><a class="link" href="{{@$info_web['instagram']}}"><i class="fa fa-instagram"></i></a></li>
            </ul>
            <p class="copyright">{!!@$info_web['copyright']!!}</p>
        </div>
    </div>
</footer>