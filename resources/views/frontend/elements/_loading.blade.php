<style>
	/* loading */
	.block-page-all {
	    position: fixed;
	    width: 100%;
	    height: 100vh;
	    left: 0px;
	    top: 0px;
	    z-index: 2018;
	    display: none;
	}

	.block-page-all.active {
	    display: block;
	}

	.block-page-all .icon-mod {
	    background: #098d00;
	    color: #fff;
	    width: 70px;
	    height: 35px;
	    line-height: 35px;
	    text-align: center;
	    position: absolute;
	    border-radius: 50%;
	    left: 50%;
	    top: 50%;
	    transform: translate(-50%, -50%);
	}

	.block-page-all .icon-mod .fa{
		margin-top: 6px;
	    font-size: 20px;
	}

	.block-page-all .icon-mod2 {
	    width: 90px;
	    height: 90px;
	    position: absolute;
	    border-radius: 50%;
	    border: 3px inset #ddd;
	    left: 50%;
	    top: 50%;
	    margin-left: -45px;
	    margin-top: -45px;
	}

	@-webkit-keyframes rotating
	/* Safari and Chrome */

	{
	    from {
	        -webkit-transform: rotate(0deg);
	        -o-transform: rotate(0deg);
	        transform: rotate(0deg);
	    }
	    to {
	        -webkit-transform: rotate(360deg);
	        -o-transform: rotate(360deg);
	        transform: rotate(360deg);
	    }
	}

	@keyframes rotating {
	    from {
	        -ms-transform: rotate(0deg);
	        -moz-transform: rotate(0deg);
	        -webkit-transform: rotate(0deg);
	        -o-transform: rotate(0deg);
	        transform: rotate(0deg);
	    }
	    to {
	        -ms-transform: rotate(360deg);
	        -moz-transform: rotate(360deg);
	        -webkit-transform: rotate(360deg);
	        -o-transform: rotate(360deg);
	        transform: rotate(360deg);
	    }
	}

	.block-page-all .icon-mod2 {
	    -webkit-animation: rotating 1.7s linear infinite;
	    -moz-animation: rotating 1.7s linear infinite;
	    -ms-animation: rotating 1.7s linear infinite;
	    -o-animation: rotating 1.7s linear infinite;
	    animation: rotating 1.7s linear infinite;
	}
	/* End loading */
</style>
<div class="block-page-all">
    <span class="icon-mod"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></span>
    <span class="icon-mod2"></span>
</div>