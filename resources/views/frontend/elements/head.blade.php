		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" sizes="96x96" href="/public/img/upload/settings/{{@$info_web['favicon']}}">
		@include('frontend.elements._seo')
		@include('frontend.elements._plugin')
