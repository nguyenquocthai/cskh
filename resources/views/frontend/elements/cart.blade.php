<!-- PRODUCT MODAL-->
<section class="modal fade product-modal" id="modalProduct" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                	<i class="fa fa-shopping-cart mr-2"></i>
                	<span>{{@$langs['danh-sach-san-pham-yeu-cau-bao-gia']}}</span>
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="product-table">
                    
                </div>
            </div>
            <div class="modal-footer">
                <form class="form-request" id="send_cart">
                    <div class="row">
                        <div class="col-4">
                            <input value="{{@session('userSession')->email}}" name="email" class="form-control" type="text" placeholder="Email">
                        </div>
                        <div class="col-4">
                            <input value="{{@session('userSession')->name}}" name="name" class="form-control" type="text" placeholder="{{@$langs['nguoi-dai-dien']}}">
                        </div>
                        <div class="col-2">
                            <input value="{{@session('userSession')->phone}}" name="phone" class="form-control" type="text" placeholder="{{@$langs['dien-thoai']}}">
                        </div>
                        <div class="col-2">
                            <button class="reset-btn btn-main">{{@$langs['gui-yeu-cau']}}</button>
                        </div>
                    </div>
                </form>
                <div class="ctrl">
                    <button class="reset-btn back" data-dismiss="modal">
                    	<i class="fa fa-caret-left"></i>
                    	<span class="d-inline-block">{{@$langs['tiep-tuc-chon-san-pham-vao-bao-gia']}}</span>
                    </button>
                    <button class="reset-btn remove-all remove_cart_all"><i class="fa fa-remove-red"></i><span class="d-inline-block">{{@$langs['xoa-tat-ca-san-pham']}}</span></button>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    jQuery(document).ready(function($) {
        $('.count_cart').html('{{$count_carts}}')
    });

    // add_cart
	$(document).on('click', '.add_cart', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).closest('.product-item2').find('.quantity-val').val();
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/add_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

	// add_cart2
	$(document).on('click', '.add_cart2', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).closest('.content').find('.quantity-val').val();
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/add_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

	// update_cart
	$(document).on('blur', '.update_cart', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).val();
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    $.ajax({
	        type: 'POST',
	        url: '/update_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                return false
	            }
	        }
	    });
	});

	// remove_cart
	$(document).on('click', '.remove_cart', function(event) {
	    var here = $(this).closest('.product-table__row');
	    var id = $(this).attr('data-id');
	    var data = {};
	    data['id'] = id;
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/remove_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            here.remove();
	            $('.count_cart').html(result.count);
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

	// remove_cart_all
	$(document).on('click', '.remove_cart_all', function(event) {
	    var here = $('.product-table .product-table__row');
	    var data = {};
	    data['status_code'] = 'all';
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/remove_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            here.remove();
	            $('.count_cart').html(result.count);
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

	// show_cart
	$(document).on('click', '.show_cart', function(event) {
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'GET',
	        url: '/show_cart',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

    var send_cart = $('#send_cart').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            email:{
                required: true
            },
            name:{
                required: true
            },
            phone:{
                required: true
            }
        },
        messages: {
            email:{
                required: "{{@$langs['chua-nhap-email']}}"
            },
            name:{
                required: "{{@$langs['chua-nhap-ten']}}"
            },
            phone:{
                required: "{{@$langs['chua-nhap-so-dien-thoai']}}"
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#send_cart").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/send_cart',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    $('#modalProduct').modal('hide');
                    toastr.success(result.message);
                    $('.count_cart').html('0');
                    $('.block-page-all').removeClass('active');
                }
            });
            return false;
        }
    });
</script>