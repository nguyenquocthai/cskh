		<!-- theme -->
	    <link rel="stylesheet" href="/public/theme/vendors/bootstrap-4/bootstrap.min.css">
	    <link rel="stylesheet" href="/public/theme/vendors/font-awesome-4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" href="/public/theme/vendors/slick-1.8.1/slick.css">
	    <link rel="stylesheet" href="/public/theme/vendors/js/jquery-ui.min.css">
	    <link rel="stylesheet" href="/public/theme/vendors/magnific-popup/magnific-popup.css">
	    <link rel="stylesheet" href="/public/theme/css/main.css">

		<script src="/public/theme/vendors/js/jquery-3.3.1.min.js"></script>
	    <script src="/public/theme/vendors/js/popper.min.js"></script>
	    <script src="/public/theme/vendors/bootstrap-4/bootstrap.min.js"></script>
	    <script src="/public/theme/vendors/slick-1.8.1/slick.min.js"></script>
	    <script src="/public/theme/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
	    <script src="/public/theme/js/main.min.js"></script>
		<!-- /theme -->

		<!-- validate -->
		<script src="/public/vendor/jquery-validation-1.16.0/jquery.validate.min.js?v={{@$info_web['updated_at']}}"></script>
		<script src="/public/vendor/jquery-validation-1.16.0/additional-methods.min.js?v={{@$info_web['updated_at']}}"></script>
		<!-- /validate -->

		<!-- toastr -->
		<link rel="stylesheet" href="/public/assets/global/plugins/toastr/toastr.min.css?v={{@$info_web['updated_at']}}">
		<script src="/public/assets/global/plugins/toastr/toastr.min.js?v={{@$info_web['updated_at']}}"></script>
		<!-- /toastr -->

		<!-- datepicker -->
		<link rel="stylesheet" href="/public/vendor/air-datepicker-master/dist/css/datepicker.min.css?v={{@$info_web['updated_at']}}">
		<script src="/public/vendor/air-datepicker-master/dist/js/datepicker.min.js?v={{@$info_web['updated_at']}}"></script>
		<script src="/public/vendor/air-datepicker-master/dist/js/i18n/datepicker.en.js?v={{@$info_web['updated_at']}}"></script>
		<!-- /datepicker -->

		<!-- DataTables -->
		<link rel="stylesheet" href="/public/assets/global/plugins/datatables/datatables.min.css?v={{@$info_web['updated_at']}}">
		<link rel="stylesheet" href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css?v={{@$info_web['updated_at']}}">
		<script src="/public/assets/global/scripts/datatable.js?v={{@$info_web['updated_at']}}"></script>
		<script src="/public/assets/global/plugins/datatables/datatables.min.js" ></script>
		<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js?v={{@$info_web['updated_at']}}"></script>
		<!-- /DataTables -->

		<!-- select2 -->
		<link rel="stylesheet" href="/public/assets/global/plugins/select2-4.0.3/css/select2.min.css?v={{@$info_web['updated_at']}}">
		<link rel="stylesheet" href="/public/assets/global/plugins/select2-4.0.3/css/select2-bootstrap.css?v={{@$info_web['updated_at']}}">
		<script src="/public/assets/global/plugins/select2-4.0.3/js/select2.full.min.js?v={{@$info_web['updated_at']}}"></script>
		<script src="/public/assets/global/plugins/select2-4.0.3/js/i18n/en.js?v={{@$info_web['updated_at']}}"></script>
		<!-- /select2 -->

		<!-- profile -->
		<link rel="stylesheet" href="/public/css/profile/style.css?v={{@$info_web['updated_at']}}">
		<link rel="stylesheet" href="/public/css/profile/custom.css?v={{@$info_web['updated_at']}}">

		<!-- main -->
		<link rel="stylesheet" href="/public/css/frontend/style.css?v={{@$info_web['updated_at']}}">
		<script src="/public/js/function.js?v={{@$info_web['updated_at']}}"></script>
