@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['tuyen-dung']}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>Trở về</button>
                <div class="aside-wrap js-blurOff">
                    @include('frontend.about._list')
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <h1 class="title-detail">{{@$langs['thong-tin-tuyen-dung']}}</h1>
                @if (count($tuyendungs) != 0)
                    @foreach ($tuyendungs as $tuyendung)
                    <article class="about-item style2">
                        <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $tuyendung->avatar,'data' => 'tuyendung', 'time' => $tuyendung->updated_at])}}" alt=""><a class="link" href="/chi-tiet-tuyen-dung/{{$tuyendung->slug}}"></a></figure>
                        <div class="content">
                            <h2 class="title"><a class="link" href="/chi-tiet-tuyen-dung/{{$tuyendung->slug}}">{{$tuyendung->title}}</a></h2>
                            <p class="summary">{{$tuyendung->summary}}</p>
                        </div>
                    </article>
                    @endforeach
                @else
                
                @endif
                
            </div>
        </div>
    </div>
</main>
@endsection