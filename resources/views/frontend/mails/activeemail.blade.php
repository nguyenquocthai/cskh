<h3>Hello <i>{{ $obj->receiver }}</i></h3>
<h2>Please confirm your email</h2>

<p><b>Thank for signing up to Vivaland To set up your account, please confirm your email address by click the link below.</b></p>

<div>
<p><b>Activated link:</b>&nbsp;{{BladeGeneral::fullBaseUrl()}}/account_active/{{ $obj->token }}</p>
</div>
Thank You,
<br/>
<i>{{ $obj->receiver }}</i>
