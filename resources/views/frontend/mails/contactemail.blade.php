<h3>Hello&nbsp;<i>{{ $obj->receiver }}</i></h3>
<h2>You have new contact</h2>
<p>Name:&nbsp;<b>{{$obj->name_contact}}</b></p>
<p>Phone:&nbsp;<b>{{$obj->phone_contact}}</b></p>
<p>Email:&nbsp;<b>{{$obj->email_contact}}</b></p>
<p>Comment:&nbsp;<b>{{$obj->comment}}</b></p>
<p><b>Link to inbox:</b>&nbsp;{{BladeGeneral::fullBaseUrl()}}/contact-rental</p>