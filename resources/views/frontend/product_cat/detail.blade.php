@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham">{{@$langs['danh-muc-san-pham']}}</a></li>
                <li class="item active">{{$product_cat->title}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>{{@$langs['tro-ve']}}</button>
                <div class="aside-wrap js-blurOff">
                    {!!$widget_product_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <h1 class="title-detail">{{$product_cat->title}}</h1>
                @if (count($product_catsubs) != 0)
                    @foreach ($product_catsubs as $product_catsub)
                    <section class="product-panel style2">
                        <h2 class="title"><a class="link" href="/danh-muc-san-pham/{{$product_catsub->slug}}">{{$product_catsub->title}}</a></h2>
                        <div class="product-panel__inner">
                            <div class="row">
                                @foreach ($product_catsub->item_projects as $item_project)
                                <div class="col-sm-6 col-xl-4 product-item">
                                    <div class="inner">
                                        <a class="name link" href="/chi-tiet-san-pham/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a>
                                        <div class="wrap">
                                            <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt=""><a class="link" href="/chi-tiet-san-pham/{{$item_project->slug}}" title="{{$item_project->title}}"></a></figure>
                                            <p class="summary">{{$item_project->summary}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                    @endforeach
                @else
                
                @endif

            </div>
        </div>
    </div>
</main>
@endsection