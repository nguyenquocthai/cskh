@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham">{{@$langs['danh-muc-san-pham']}}</a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham/{{$product_catparent->slug}}">{{$product_catparent->title}}</a></li>
                <li class="item active">{{$product_cat->title}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>Trở về</button>
                <div class="aside-wrap js-blurOff">
                    {!!$widget_product_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <h1 class="title-detail">{{$product_cat->title}}</h1>
                <div class="js-tabParent">
                    <nav class="tab-nav js-tabNav">
                        <ul class="reset-list list">
                            <li class="item"><a class="reset-btn link js-tabLink active" href="#!" data-target="#tabProductList">{{@$langs['san-pham']}}</a></li>
                            <li class="item"><a class="reset-btn link js-tabLink" href="#!" data-target="#tabGeneral">{{@$langs['tong-quat']}}</a></li>
                            <li class="item"><a class="reset-btn link js-tabLink" href="#!" data-target="#tabDocument">{{@$langs['tai-lieu']}}</a></li>
                        </ul>
                    </nav>
                    <div class="tab-content js-tabContent">
                        <div class="tab-panel fade active" id="tabProductList">
                            @if (count($item_projects) != 0)
                                @foreach ($item_projects as $item_project)
                                @include('frontend.item_project._items')
                                @endforeach
                            @else
                            
                            @endif

                            {{ $item_projects->fragment('foo')->links() }}
                        </div>
                        <div class="tab-panel fade" id="tabGeneral">
                            <div class="product-general">
                                <figure class="img"><img src="{{BladeGeneral::GetImg(['avatar' => $product_cat->avatar,'data' => 'product_cat', 'time' => $product_cat->updated_at])}}" alt=""></figure>
                                <div class="content">{!!$product_cat->content!!}</div>
                            </div>
                            <div class="social-share right">
                                <div class="text">{{@$langs['chia-se']}}:</div>
                                <ul class="reset-list list">
                                    <li class="item"><a class="link" href="#!"><i class="fa fa-facebook-official cl_fb"></i></a></li>
                                    <li class="item"><a class="link" href="#!"><i class="fa fa-twitter-square cl_tt"></i></a></li>
                                    <li class="item"><a class="link" href="#!"><i class="fa fa-google-plus-square cl_gg"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-panel fade" id="tabDocument">
                            <div class="product-document">
                                <h3 class="title">{{@$langs['tai-lieu-di-kem']}}</h3>
                                <div class="product-document__table">
                                    <ul class="reset-list product-document__row head">
                                        <li class="item">&nbsp;</li>
                                        <li class="item">{{@$langs['ngon-ngu']}}</li>
                                        <li class="item">{{@$langs['tai-lieu']}}</li>
                                        <li class="item">{{@$langs['kich-thuoc']}}</li>
                                        <li class="item">&nbsp;</li>
                                    </ul>
                                    @if(count($tailieu) != null)
                                    <ul class="reset-list product-document__row">
                                        <li class="item" data-title="STT:">1</li>
                                        <li class="item" data-title="Ngôn ngữ:">Tiếng Việt</li>
                                        <li class="item" data-title="Tài liệu:">{{$product_cat->ten_tailieu}}</li>
                                        <li class="item" data-title="Kích thước:">{{$tailieu['size']}}</li>
                                        <li class="item" data-title="Tải về:"><a class="link" href="{{$tailieu['url']}}" download><i class="fa fa-download"></i>Download</a></li>
                                    </ul>
                                    @endif
                                    @if(count($tailieu_en) != null)
                                    <ul class="reset-list product-document__row">
                                        <li class="item" data-title="STT:">2</li>
                                        <li class="item" data-title="Ngôn ngữ:">English</li>
                                        <li class="item" data-title="Tài liệu:">{{$product_cat->ten_tailieu_en}}</li>
                                        <li class="item" data-title="Kích thước:">{{$tailieu_en['size']}}</li>
                                        <li class="item" data-title="Tải về:"><a class="link" href="{{$tailieu_en['url']}}" download><i class="fa fa-download"></i>Download</a></li>
                                    </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection