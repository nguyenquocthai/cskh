@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['danh-muc-san-pham']}}</li>
            </ul>
        </nav>

        @if (count($product_cats) != 0)
            @foreach ($product_cats as $key => $product_cat)
            <section class="product-panel style2">
                <h2 class="title"><a class="link" href="/danh-muc-san-pham/{{$product_cat->slug}}">{{$product_cat->title}}</a></h2>
                <div class="product-panel__inner">
                    <div class="row">

                        @if (count($product_cat->subcats) != 0)
                            @foreach ($product_cat->subcats as $subcat)
                            <div class="col-sm-6 col-md-4 col-lg-3 product-list">
                                <h3 class="title"><a class="link" href="/danh-muc-san-pham/{{$subcat->slug}}">{{$subcat->title}}</a></h3>
                                <ul class="reset-list list">
                                    @if (count($subcat->item_projects) != 0)
                                        @foreach ($subcat->item_projects as $item_project)
                                        <li class="item"><a class="link" href="/chi-tiet-san-pham/{{$item_project->slug}}">{{$item_project->title}}</a></li>
                                        @endforeach
                                    @else
                                    
                                    @endif
                                </ul>
                            </div>
                            @endforeach
                        @else
                        
                        @endif

                    </div>
                </div>
            </section>
            @endforeach
        @else
        
        @endif

    </div>
</main>
@endsection