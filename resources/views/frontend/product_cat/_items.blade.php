<?php
    function displayList($list, $child = 0, $paren_id = 0) {
?>      
    <?php if($child == 0) {?>
        <div class="aside-block <?php echo 'child'.$child;?>">
        <?php foreach($list as $item): $child = 0;?>
            
        <div class="aside-nav" data-id="<?php echo $item["id"]; ?>">
            <h4 class="title">
                <a href="/danh-muc-san-pham/<?php echo $item["slug"]; ?>" title=""><?php echo $item["title"]; ?></a>
            </h4>
        <?php if (array_key_exists("children", $item)): $child++;?>
        <?php displayList($item["children"], $child, $item['id']); ?>
        <?php endif; ?>
        </div>
        <?php endforeach; ?>
        </div>
    <?php } else if($child == 1){?>
        <ul class="reset-list aside-nav__list <?php echo 'child'.$child;?>">
        <?php foreach($list as $item): ?>
            
        <li class="item" data-id="<?php echo $item["id"]; ?>">
            <div class="collapse-title">
                <button class="reset-btn collapse-btn collapsed" data-toggle="collapse" data-target="#collapseAsideNav<?php echo $item['id']; ?>"></button>
                <a class="collapse-link" href="/danh-muc-san-pham/<?php echo $item["slug"]; ?>"><?php echo $item["title"]; ?></a>
            </div>
        <?php if (array_key_exists("children", $item)): $child++;?>
        <?php displayList($item["children"], $child, $item['id']); ?>
        <?php endif; ?>
        </li>
        <?php endforeach; ?>
        </ul>
    <?php } ?>
<?php } ?>
<?php displayList($list); ?>