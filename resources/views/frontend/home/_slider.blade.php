<section class="section banner-main js-sliderMain home_slider">
    @if (count($sliders) != 0)
        @foreach ($sliders as $slider)
        <figure class="img hasLink mb-0">
            <div class="img_box">
                <img src="{{BladeGeneral::GetImg(['avatar' => $slider->avatar,'data' => 'slider', 'time' => $slider->updated_at])}}" alt="">
                <a class="link" href="{{$slider->link}}"></a>
            </div>
        </figure>
        @endforeach
    @else
    
    @endif
</section>