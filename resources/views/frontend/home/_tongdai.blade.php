<div class="aside-block hasShadow">
    @if (count($tongdais) != 0)
        @foreach ($tongdais as $key => $tongdai)
        @if($key == 0)
        <div class="support">
            <img src="{{BladeGeneral::GetImg(['avatar' => $tongdai->avatar,'data' => 'tongdai', 'time' => $tongdai->updated_at])}}" alt="">
            <div class="contact-box hasShadow">
                <div class="name">{{$tongdai->name}}</div>
                <a class="phone" href="tel:{{$tongdai->phone}}">{{$tongdai->phone}}</a>
                <ul class="reset-list contact-app">
                    <li class="item"><a class="link" href="{{$tongdai->phone}}"><img src="/public/theme/img/skype.png" alt=""></a></li>
                    <li class="item"><a class="link" href="{{$tongdai->phone}}"><img src="/public/theme/img/zalo.png" alt=""></a></li>
                </ul>
            </div>
        </div>
        @else
        <div class="support">
            <h4 class="title">{{$tongdai->title}}</h4>
            <div class="contact-box hasShadow">
                <div class="name">{{$tongdai->name}}</div>
                <a class="phone" href="tel:{{$tongdai->phone}}">{{$tongdai->phone}}</a>
                <ul class="reset-list contact-app">
                    <li class="item"><a class="link" href="{{$tongdai->phone}}"><img src="/public/theme/img/skype.png" alt=""></a></li>
                    <li class="item"><a class="link" href="{{$tongdai->phone}}"><img src="/public/theme/img/zalo.png" alt=""></a></li>
                </ul>
            </div>
        </div>
        @endif
        @endforeach
    @else
    
    @endif
    
</div>