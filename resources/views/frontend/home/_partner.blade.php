<section class="section partner">
    <div class="partner-slider js-sliderPartner">
        @if (count($partners) != 0)
            @foreach ($partners as $partner)
            <figure class="item hasLink mb-0"><img src="{{BladeGeneral::GetImg(['avatar' => $partner->avatar,'data' => 'partner', 'time' => $partner->updated_at])}}" alt=""><a class="link" href="{{$partner->link}}"></a></figure>
            @endforeach
        @else
        
        @endif
        @if (count($partners) != 0)
            @foreach ($partners as $partner)
            <figure class="item hasLink mb-0"><img src="{{BladeGeneral::GetImg(['avatar' => $partner->avatar,'data' => 'partner', 'time' => $partner->updated_at])}}" alt=""><a class="link" href="{{$partner->link}}"></a></figure>
            @endforeach
        @else
        
        @endif
    </div>
</section>