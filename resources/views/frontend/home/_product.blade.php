<section class="section product-wrap">
    @if (count($product_cats) != 0)
        @foreach ($product_cats as $product_cat)
            @if(count($product_cat->subcats) != 0)
            <div class="product-panel">
                <div class="title-wrap">
                    <h4 class="title">{{$product_cat->title}}</h4>
                    <a class="link" href="/danh-muc-san-pham/{{$product_cat->slug}}">{{@$langs['xem-tat-ca']}}</a>
                </div>
                <div class="row">
                    @if (count($product_cat->subcats) != 0)
                        @foreach ($product_cat->subcats as $subcat)
                        <div class="col-sm-6 col-xl-4 product-item">
                            <div class="inner">
                                <a class="name link" href="/danh-muc-san-pham/{{$subcat->slug}}" title="{{$subcat->title}}">{{$subcat->title}}</a>
                                <div class="wrap">
                                    <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $subcat->avatar,'data' => 'product_cat', 'time' => $subcat->updated_at])}}" alt=""><a class="link" href="/danh-muc-san-pham/{{$subcat->slug}}" title="{{$subcat->title}}"></a></figure>
                                    <p class="summary">{{$subcat->summary}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                    
                    @endif
                </div>
            </div>
            @endif
        @endforeach
    @else
    
    @endif
</section>