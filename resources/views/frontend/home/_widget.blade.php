<aside class="col-lg-3 aside js-aside">
    <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>Trở về</button>
    <div class="aside-wrap js-blurOff">
        {!!$viewproduct_cats!!}
        @include('frontend.home._tongdai')
        <div class="aside-block">
            <figure class="mb-0 hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $banners[1]->avatar,'data' => 'banner', 'time' => $banners[1]->updated_at])}}" alt=""><a class="link" href="{{$banners[1]->link}}"></a></figure>
        </div>
    </div>
</aside>