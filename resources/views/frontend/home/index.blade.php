@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <div class="row">
            @include('frontend.home._widget')
            <div class="col-lg-9">

                @include('frontend.home._slider')

                @include('frontend.home._product')

                @include('frontend.home._banner0')

                @include('frontend.home._blog')

                @include('frontend.home._partner')
                
            </div>
        </div>
    </div>
</main>
@endsection