<section class="section news-index">
    <div class="title-wrap">
        <h4 class="title">{{@$langs['tin-tuc']}}</h4>
        <a class="link" href="/tin-tuc">{{@$langs['xem-tat-ca']}}</a>
    </div>
    <div class="news-index__list">
    	@if (count($blogs) != 0)
    	    @foreach ($blogs as $blog)
    	    <article class="news-index__item">
	            <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $blog->avatar,'data' => 'blog', 'time' => $blog->updated_at])}}" alt=""><a class="link" href="/chi-tiet-tin/{{$blog->slug}}" title="{{$blog->title}}"></a></figure>
	            <div class="content">
	                <div class="date">{{BladeGeneral::dateTime($blog->updated_at)}}</div>
	                <a class="name" href="/chi-tiet-tin/{{$blog->slug}}" title="{{$blog->title}}">{{$blog->title}}</a>
	                <p class="summary">{{$blog->summary}}</p>
	                <a class="more" href="/chi-tiet-tin/{{$blog->slug}}" title="{{$blog->title}}">{{@$langs['doc-them']}}<i class="fa fa-long-arrow-right ml-1"></i></a>
	            </div>
	        </article>
    	    @endforeach
    	@else
    	
    	@endif
    </div>
</section>