@extends('frontend.layouts.main')
@section('content')

<style>
    #pipay_form{
        width: 100%;
        max-width: 500px;
        margin: 0 auto;
    }
</style>
<main class="main">
    <div class="container">
        <form id="pipay_form">
            <div class="form-group">
                <label>Money</label>
                <input type="number" name="total_fee" class="form-control">
            </div>
            <div class="form-group">
                <label>Trans Service</label>
                <select class="form-control" name="trans_service">
                    <option value="Alipay">Alipay</option>
                    <option value="WeChat">WeChat</option>
                </select>
            </div>

            <button type="submit">Checkout</button>
            <div>{!!@session('checklink')!!}</div>
        </form>
    </div>
</main>

<script>
    var pipay_form = $('#pipay_form').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            total_fee:{
                required: true
            }
        },
        messages: {
            total_fee:{
                required: 'Money not null.'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#pipay_form").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/api-pipay',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    document.location.href = result.url;
                }
            });
            return false;
        }
    });
</script>
@endsection