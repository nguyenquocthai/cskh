@extends('frontend.layouts.main')
@section('content')
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="/public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Inbox</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <!--/head-block-->
    <div class="profile-block">
        <div class="content-index bg-gray">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News Board</button>

                        <h2 class="title text-uppercase">Messages</h2>

                        <div class="tab-container">
                            <div class="table-responsive load_thue_ban">
                                @include('frontend.dangtin._lienhebds')
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="modal_lienhe" tabindex="-1" role="dialog" aria-labelledby="modal_lienheLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_lienheLabel">Contact infomation</h4>
            </div>
            <div class="modal-body">
                <div class="load_lienhebds">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.load_thue_ban').on('click', '.xem_lienhe', function(event) {
        event.preventDefault();
        var id = $(this).attr('data-id');

        var data = {};
        data['id'] = id;
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_lienhebds',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                $('.block-page-all').removeClass('active');
                $('.load_lienhebds').html(result.data);
                $('#modal_lienhe').modal('show');
            }
        });
    });

    $('.load_thue_ban').on('click', '.delete_lienhe', function(event) {
        event.preventDefault();
        var here = $(this).closest('tr');
        var id = $(this).attr('data-id');

        var data = {};
        data['id'] = id;
        data['status_code'] = 'delete';
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_lienhebds',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                here.remove();
                $('.block-page-all').removeClass('active');
            }
        });
    });
</script>
@endsection