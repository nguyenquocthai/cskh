@if (count($item_albums) != 0)
    @foreach ($item_albums as $item_album)
    <div class="col-sm-3 item_album_item item_load">
	    <div class="item">
	        <img src="{{BladeGeneral::GetImg(['avatar' => $item_album->name,'data' => 'item_album', 'time' => $item_album->updated_at])}}" alt="">
	        <span class="delete_album_item" data-id="{{$item_album->id}}"><i class="far fa-trash-alt"></i></span>
	    </div>
	</div>
    @endforeach
@else

@endif