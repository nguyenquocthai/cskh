@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
<script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
<!--/head-block-->

<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Trang chủ</a></li>
                <li class="item active">Đăng tin thuê</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray store-dang-tin">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                        <h2 class="title text-uppercase">Đăng tin thuê <small class="d-block">Quý khách nhập thông tin nhà đất cần cho thuê vào các mục dưới đây</small></h2>

                        @include('frontend.dangtin._stepthueban')

                        <form id="create_item" autocomplete="off" class="panel-form">
                            <input type="hidden" name="id" value="{{$item_project->id}}">
                            <input type="hidden" name="type" value="0">
                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fas fa-book"></i> Thông tin cho thuê</span></h4>

                                <div class="form-group">
                                    <label class="text mb-md-0">Tiêu đề</label>
                                    <div class="input">
                                        <input value="{{$item_project->title}}" type="text" name="title" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Danh mục</label>
                                            <div class="input">
                                                <select class="form-control" name="category_id" id="category_id">
                                                    <option value="">-- Danh mục --</option>
                                                    @foreach($categories as $category)
                                                    <option @if($item_project->category_id == $category->id) selected @endif value="{{ $category->id }}">{{ $category->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Đối tượng thuê</label>
                                            <div class="input">
                                                <select class="form-control" name="doituong" id="doituong">
                                                    <option value="">-- Đối tượng --</option>
                                                    <option value="0">Nam & Nữ</option>
                                                    <option value="1">Nam</option>
                                                    <option value="2">Nữ</option>
                                                </select>
                                                <script>
                                                    $('#doituong').val('{{$item_project->doituong}}').trigger('change');
                                                </script>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Tỉnh / Thành phố</label>
                                            <div class="input">
                                                <select id="tinhthanh" name="tinhthanh" class="form-control local">
                                                    <option value="">-- Tỉnh / Thành phố --</option>
                                                    @if (count($provinces) != 0)
                                                        @foreach ($provinces as $province)
                                                        <option @if($item_project->tinhthanh == $province->id) selected @endif value="{{$province->id}}">{{$province->name}}</option>
                                                        @endforeach
                                                    @else
                                                    
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Quận / Huyện</label>
                                            <div class="input load_quanhuyen">
                                                <select id="quanhuyen" name="quanhuyen" class="form-control local">
                                                    <option value="">-- Quận / Huyện --</option>
                                                    @if (count($districts) != 0)
                                                        @foreach ($districts as $district)
                                                        <option @if($item_project->quanhuyen == $district->id) selected @endif value="{{$district->id}}">{{$district->name}}</option>
                                                        @endforeach
                                                    @else
                                                    
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Phường / Xã</label>
                                            <div class="input load_phuongxa">
                                                <select id="phuongxa" name="phuongxa" class="form-control local">
                                                    <option value="">-- Phường / Xã --</option>
                                                    @if (count($wards) != 0)
                                                        @foreach ($wards as $ward)
                                                        <option @if($item_project->phuongxa == $ward->id) selected @endif value="{{$ward->id}}">{{$ward->name}}</option>
                                                        @endforeach
                                                    @else
                                                    
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Đường</label>
                                            <div class="input load_duong local">
                                                <select id="duong" name="duong" class="form-control">
                                                    <option value="">-- Đường --</option>
                                                    @if (count($streets) != 0)
                                                        @foreach ($streets as $street)
                                                        <option @if($item_project->duong == $street->id) selected @endif value="{{$street->id}}">{{$street->name}}</option>
                                                        @endforeach
                                                    @else
                                                    
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="text mb-md-0">Địa chỉ</label>
                                    <div class="input">
                                        <input value="{{$item_project->address}}" id="address" type="text" name="address" class="form-control">
                                        <input value="{{$item_project->map}}" type="hidden" name="map" class="toadobando">
                                    </div>
                                </div>

                                <div class="form-group toadobando_box">
                                    <div class="input">
                                        <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                                        <div id="googleMap" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Giá thuê</label>
                                            <div class="input">
                                                <input id="price" value="{{$item_project->price}}" type="number" name="price" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Diện tích</label>
                                            <div class="input">
                                                <div class="input-group mb-3">
                                                    <input value="{{$item_project->area}}" type="number" class="form-control" name="area" id="area">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Đơn vị</label>
                                            <div class="input">
                                                @include('frontend.dangtin._donvithue')
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Tổng tiền</label>
                                            <div class="input">
                                                <div class="input-group mb-3">
                                                    <input value="{{$item_project->allprice_mark}}" id="allprice_mark" readonly type="text" class="form-control" name="allprice_mark">

                                                    <input value="{{$item_project->allprice}}" id="allprice" type="hidden" name="allprice">

                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">/tháng</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Mô tả ngắn</span></h4>

                                <div class="input">
                                    <textarea name="short_content" id="short_content" class="form-control">{!!$item_project->short_content!!}</textarea>
                                </div>
                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Thông tin liên hệ</span></h4>

                                @include('frontend.dangtin._lienhetin')

                                <div class="form-group">
                                    <div class="input">
                                        <label class="mt-2"><input @if($item_project->email_flg == 1) checked @endif name="email_flg" value="1" type="checkbox"> Nhận email phản hồi</label>
                                    </div>
                                </div>
                                <a target="_blank" href="/thong-tin-nguoi-dung"><i class="fas fa-link"></i><i> Chỉnh sửa thông tin liên hệ</i></a>
                                
                            </div>

                            <div class="panel-form--footer">
                                <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Tiếp tục</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $('#price').blur(function(event) {
        allprice();
    });

    $('#area').blur(function(event) {
        allprice();
    });

    $('#donvi').change(function(event) {
        allprice();
    });

    function allprice(argument) {
        var price = parseFloat($('#price').val());
        var area = parseFloat($('#area').val());
        var donvi = parseInt($('#donvi').val());
        var allprice = 0;
        var allprice_mark = "";

        if (donvi == 0) {
            allprice = 0;
            allprice_mark = "Thỏa thuận";
        } else if (donvi == 1){
            if (isNaN(price)) {
                $('#price').addClass('error_input');
            } else {
                allprice = price*1000;
            }
        } else if (donvi == 2){
            if (isNaN(price)) {
                $('#price').addClass('error_input');
            } else {
                allprice = price*1000000;
            }
        } else if (donvi == 3){
            if (isNaN(area)) {
                $('#area').addClass('error_input');
            } else if(isNaN(price)){
                $('#price').addClass('error_input');
            } else {
                allprice = area*price*1000;
            }
        } else if (donvi == 4){
            if (isNaN(area)) {
                $('#area').addClass('error_input');
            } else if(isNaN(price)){
                $('#price').addClass('error_input');
            } else {
                allprice = area*price*1000000;
            }
        }
        if (allprice != "Thỏa thuận" && allprice != "") {
            allprice_mark = nFormatter(allprice,2);
        }
        $('#allprice_mark').val(allprice_mark);
        $('#allprice').val(allprice);
    }
</script>

<script>
    var $scope = {};

    tinymce.init({
        selector: '#short_content'
    });

    jQuery(document).ready(function($) {
        // GOOGLE MAP
        var map = "{{$item_project->map}}"
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (typeof google === "undefined") {
            location.reload();
        }

        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
                $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        }
    };

    $('#create_item').on('change', '.local', function(event) {
        var tinhthanh = $('#tinhthanh option:selected').html();

        var quanhuyen = $('#quanhuyen option:selected').html();
        if (quanhuyen == '-- Quận / Huyện --') {
            quanhuyen = '';
        } else {
            quanhuyen = quanhuyen + ', ';
        }

        var phuongxa = $('#phuongxa option:selected').html();
        if (phuongxa == '-- Phường / Xã --') {
            phuongxa = '';
        } else {
            phuongxa = phuongxa + ', ';
        }

        var duong = $('#duong option:selected').html();
        if (duong == '-- Đường --') {
            duong = '';
        } else {
            duong = duong + ', ';
        }

        var address = duong + phuongxa + quanhuyen + tinhthanh;
        $('#address').val(address);

        var input = document.getElementById('pac-input');
        $('#pac-input').val($('#address').val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

    $("#address").blur(function(){
        var input = document.getElementById('pac-input');
        $('#pac-input').val($(this).val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

</script>   

<script>
    $('#tinhthanh, #quanhuyen, #phuongxa, #duong, #category_id, #id_project, #donvi').select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('#tinhthanh, #category_id, #id_project').change(function(event) {
        if ($(this).val() != "") {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        } else {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        }
    });

    $('#tinhthanh').change(function(event) {
        $('#phuongxa').val('');
        $('#duong').val('');
        var data = {};
        data['status_code'] = "quanhuyen";
        data['id'] = $(this).val();

        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_dangtin',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                $('.load_quanhuyen').html(result.value);
                $('#quanhuyen').select2({
                    theme: "bootstrap",
                    width: '100%'
                });
                $('#quanhuyen').val('').trigger('change');
                $('.block-page-all').removeClass('active');
            }
        });
    });

    $('#create_item').on('change', '#quanhuyen', function(event) {
        var data = {};
        data['status_code'] = "phuongxa";
        data['id'] = $(this).val();

        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_dangtin',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                $('.load_phuongxa').html(result.value);
                $('#phuongxa').select2({
                    theme: "bootstrap",
                    width: '100%'
                });

                $('.load_duong').html(result.value1);
                $('#duong').select2({
                    theme: "bootstrap",
                    width: '100%'
                });
                $('.block-page-all').removeClass('active');
            }
        });
    });

    var create_item = $('#create_item').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
            $(element).addClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        },
        rules: {
            title:{
                required: true
            },
            address:{
                required: true
            },
            tinhthanh:{
                required: true
            },
            quanhuyen:{
                required: true
            },
            phuongxa:{
                required: true
            },
            duong:{
                required: true
            },
            category_id:{
                required: true
            },
            map:{
                required: true
            },
            short_content:{
                required: true
            },
            doituong:{
                required: true
            }
        },
        messages: {
            title:{
                required: 'Tên dự án'
            },
            address:{
                required: 'Địa chỉ dự án'
            },
            tinhthanh:{
                required: 'Tỉnh thành'
            },
            quanhuyen:{
                required: 'Quận huyện'
            },
            phuongxa:{
                required: 'Phường xã'
            },
            duong:{
                required: 'Đường'
            },
            map:{
                required: 'Bản đồ'
            },
            category_id:{
                required: 'Danh mục'
            },
            short_content:{
                required: 'Mô tả ngắn'
            },
            doituong:{
                required: 'Mô tả ngắn'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#create_item").serializeArray().map(function(x){data[x.name] = x.value;});
            data['status_code'] = "edit_b1";
            data['slug'] = ChangeToSlug(data['title']);
            data['short_content'] = tinymce.get('short_content').getContent();

            var search_val = {};
            search_val[0] = $('#category_id option:selected').html();
            search_val[1] = $('#id_project option:selected').html();
            search_val[2] = $('#tinhthanh option:selected').html();
            search_val[3] = $('#quanhuyen option:selected').html();
            search_val[4] = $('#phuongxa option:selected').html();
            search_val[5] = $('#duong option:selected').html();
            search_val[6] = data['title'];
            data['search_val'] = search_val;

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    toastr.success(result.message);
                    document.location.href = '/tao-tin-thue-b2/'+result.id;
                }
            });
            return false;
        }
    });
</script>  
@endsection