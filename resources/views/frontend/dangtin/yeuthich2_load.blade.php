<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">{{@$langs['san-pham']}}</th>
            <th class="text-uppercase">{{@$langs['hinh-dai-dien']}}</th>
            <th class="text-uppercase">{{@$langs['gia-hang']}}</th>
            <th class="text-uppercase">{{@$langs['ngay-tao']}}</th>
            <th class="text-uppercase">{{@$langs['xoa']}}</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @foreach (@$item_projects as $item_project)
        <tr>
            <td>
                <p class="mb-0"><a href="/chi-tiet-san-pham/{{$item_project->slug}}" title=""><b>{{@$item_project->title}}</b></a></p>
                <p class="mb-0">Date update: {{date('d/m/Y', strtotime(@$item_project->updated_at))}}</p>
            </td>

            <td><figure class="img"><img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt=""></figure></td>

            <td>
                {{BladeGeneral::priceFormat($item_project->price)}}
            </td>

            <td>
                <span class="d-inline-block"><span class="hidden">{{$item_project->created_at}}</span>{{BladeGeneral::dateTime($item_project->created_at)}}</span>
            </td>

            <td>
                <a href="javascript:;" class="reset-btn btn-ctrl remove_favorite" data-group="1" data-id="{{@$item_project->id}}"><i class="far fa-trash-alt"></i>{{@$langs['bo-thich']}}</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 3, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 0,1,4 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>