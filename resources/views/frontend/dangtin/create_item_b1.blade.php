@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Post rental information</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
        <script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
        <!--/head-block-->

        <div class="content-index bg-gray store-dang-tin">

            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News Board</button>

                        <h2 class="title text-uppercase">Post rental information <small class="d-block">Please complete the steps below</small></h2>

                        @include('frontend.dangtin._stepthueban')
                        
                        <form id="create_item" autocomplete="off" class="panel-form">
                            <input type="hidden" name="id" value="{{$item_project->id}}">
                            
                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fas fa-book"></i> Info of your property</span></h4>

                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Title</label>
                                            <div class="input">
                                                <input type="text" name="title" class="form-control">
                                                <input type="hidden" name="type" value="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Please contact</label>
                                            <select class="form-control noprice" name="noprice">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                            <script>
                                                $('.noprice').change(function(event) {
                                                    if($(this).val() == '1'){
                                                        $('.price_row').hide();
                                                    } else {
                                                        $('.price_row').show();
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Street address</label>
                                            <input class="form-control local" type="text" name="duong" id="duong">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Postal code</label>
                                            <input class="form-control local" type="text" id="phuongxa" name="phuongxa">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Province</label>
                                            <input class="form-control local" type="text" id="tinhthanh" name="tinhthanh">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">City</label>
                                            <input class="form-control local" type="text" id="quanhuyen" name="quanhuyen">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Address</label>
                                            <input class="form-control" id="address" type="text" name="address">
                                            <input type="hidden" name="map" class="toadobando">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Location</label>
                                            <select class="form-control" name="location" id="location">
                                                <option value="">-- Choose --</option>
                                                @if (count($locations) != 0)
                                                    @foreach ($locations as $location)
                                                    <option value="{{$location->title}}">{{$location->title}}</option>
                                                    @endforeach
                                                @else
                                                
                                                @endif
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group toadobando_box">
                                    <div class="input">
                                        <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                                        <div id="googleMap" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Type of property </label>
                                            <div class="input">
                                                <select class="form-control" name="category_id" id="category_id">
                                                    <option value="">-- Choose --</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">For</label>
                                            <select class="form-control" name="doituong" id="doituong">
                                                <option value="">-- Choose --</option>
                                                <option value="0">All</option>
                                                <option value="1">Men Only</option>
                                                <option value="2">Women Only</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Size</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" name="area" id="area">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">sqft</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Parking</label>
                                            <select class="form-control" name="baixe" id="baixe">
                                                <option value="">-- Choose --</option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Capacity</label>
                                            <div class="input">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" name="succhua" id="succhua">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">person</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="text mb-md-0">Bedroom</label>
                                                <select name="phongngu" class="form-control">
                                                    <option value="">-- Choose --</option>
                                                    <option value="studio">Studio</option>
                                                    <option value="bed">1 Bed</option>
                                                    <option value="1+den">1 + Den</option>
                                                    <option value="2 beds">2 Beds</option>
                                                    <option value="2+den">2 + Den</option>
                                                    <option value="3 or more">3 Or More</option>
                                                    <option value="others">Others</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="text mb-md-0">Bathroom</label>
                                                <select name="phongtam" class="form-control">
                                                    <option value="">-- Choose --</option>
                                                    <option value="shared">Shared</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3 or more">3 Or More</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row price_row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Price per month</label>
                                            <div class="input">
                                                <input id="price" value="" type="number" name="price" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Deposit</label>
                                            <input type="text" name="datcoc" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Availability</label>
                                            <input id="sangco_time" class="form-control" type="text" name="sangco_time">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Good time to contact</label>
                                            <div class="input">
                                                <input value="{{@$item_project->lh_time_contact}}" type="text" name="lh_time_contact" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Additional information</span></h4>

                                <div class="form-group">
                                    <label class="text mb-md-0">Please tell us more about your property</label>
                                    <textarea name="short_content" id="short_content" class="form-control"></textarea>
                                </div>
                                
                            </div>

                            <div class="panel-form--footer">
                                <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Next Step</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<script>

    // add value to input
    var data_json = {!!$json_item_project!!};
    $('#create_item input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#create_item select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
    });

    $('#create_item textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });
    // add value to input

    var $scope = {};

    tinymce.init({
        selector: '#short_content'
    });

    $('#sangco_time').datepicker({
        dateFormat: 'yyyy-mm-dd',
        language: 'en'
    })

    jQuery(document).ready(function($) {
        // GOOGLE MAP
        var map = "{{$item_project->map}}"
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (typeof google === "undefined") {
            location.reload();
        }

        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
                $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        }
    };

    $('#create_item').on('blur', '.local', function(event) {
        var tinhthanh = $('#tinhthanh').val();
        if (tinhthanh == '') {
            tinhthanh = '';
        } else {
            tinhthanh = tinhthanh + ', ';
        }

        var quanhuyen = $('#quanhuyen').val();
        if (quanhuyen == '') {
            quanhuyen = '';
        } else {
            quanhuyen = quanhuyen + ', ';
        }

        var phuongxa = $('#phuongxa').val();

        var duong = $('#duong').val();
        if (duong == '') {
            duong = '';
        } else {
            duong = duong + ', ';
        }

        var address = duong + quanhuyen + tinhthanh + phuongxa;
        $('#address').val(address);

        var input = document.getElementById('pac-input');
        $('#pac-input').val($('#address').val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

    $("#address").blur(function(){
        var input = document.getElementById('pac-input');
        $('#pac-input').val($(this).val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

</script>   

<script>
    $('#category_id, #id_project, #donvi').select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('#category_id, #id_project').change(function(event) {
        if ($(this).val() != "") {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        } else {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        }
    });

    var create_item = $('#create_item').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
            $(element).addClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        },
        rules: {
            title:{
                required: true
            },
            address:{
                required: true
            },
            
            category_id:{
                required: true
            },
            map:{
                required: true
            },
            short_content:{
                required: true
            },
            doituong:{
                required: true
            },
            area:{
                required: true
            },
            baixe:{
                required: true
            },
            succhua:{
                required: true
            },
            price:{
                required: true
            },
            datcoc:{
                required: true
            },
            sangco:{
                required: true
            },
            lh_phone:{
                required: true
            },
            lh_email:{
                required: true
            },
            location:{
                required: true
            },
            phongtam:{
                required: true
            },
            phongngu:{
                required: true
            }
        },
        messages: {
            title:{
                required: 'Name of project'
            },
            address:{
                required: 'Project address'
            },
            
            map:{
                 required: 'Map'
            },
            category_id:{
                required: 'Category'
            },
            short_content:{
                required: 'Short description'
            },
            doituong:{
                required: 'Object'
            },
            area:{
                area: 'area'
            },
            baixe:{
                baixe: 'parking lot'
            },
            succhua:{
                succhua: 'capacity'
            },
            price:{
                price: 'price'
            },
            datcoc:{
                datcoc: 'Deposit'
            },
            sangco:{
                sangco: 'Status'
            },
            lh_phone:{
                lh_phone: 'phone contact'
            },
            lh_email:{
                lh_email: 'email  contact'
            },
            location:{
                location: 'location'
            },
            phongtam:{
                phongtam: 'bedroom'
            },
            phongngu:{
                phongngu: 'Bathroom'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#create_item").serializeArray().map(function(x){data[x.name] = x.value;});
            data['status_code'] = "edit_b1";
            data['slug'] = ChangeToSlug(data['title']);
            data['short_content'] = tinymce.get('short_content').getContent();

            var search_val = {};
            search_val[0] = $('#category_id option:selected').html();
            search_val[1] = $('#tinhthanh').val();
            search_val[2] = $('#quanhuyen').val();
            search_val[3] = $('#phuongxa').val();
            search_val[4] = $('#duong').val();
            search_val[5] = data['title'];
            search_val[6] = $('#location').val();
            data['search_val'] = search_val;

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    toastr.success(result.message);
                    document.location.href = '/create-rental-step2/'+result.id;
                }
            });
            return false;
        }
    });
</script>  
@endsection