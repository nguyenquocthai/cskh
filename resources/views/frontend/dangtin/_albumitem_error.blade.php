@if (count($image_errors) != 0)
	<p class="title-mod"><b>There are {{count($image_errors)}} files that cannot be uploaded !</b> <span class="clear_error"><i class="fas fa-eraser"></i> Clear</span></p>
    @foreach ($image_errors as $image_error)
    <ul>
    	<li>File name: {{$image_error['file_name']}}</li>
    	<li>Size: {{BladeGeneral::formatBytes($image_error['size'])}}</li>
    	<li>Error: {!!$image_error['error']!!}</li>
    </ul>
    @endforeach
@else

@endif