<div class="col-sm-3 item_img_flat">
    <div class="item">
        <img src="{{BladeGeneral::GetImg(['avatar' => $img_flat->avatar,'data' => 'imgflat', 'time' => $img_flat->updated_at])}}" alt="">
        <span class="delete_img_flat" data-id="{{$img_flat->id}}"><i class="far fa-trash-alt"></i></span>
    </div>
</div>