@extends('frontend.layouts.main')
@section('content')
@include('frontend.dangtin._head')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Post rental information</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray store-dang-tin">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News Board</button>

                        <h2 class="title text-uppercase">Post rental information <small class="d-block">Please complete the steps below</small></h2>

                        @include('frontend.dangtin._stepthueban')

                        <div action="" class="panel-form">
                            <div class="block">
                                <div class="row">
                                    
                                    <div class="col-sm-9">
                                        <label class="d-block caption_img">Album images</label>
                                        <div class="load_album_item">
                                            <div class="row">
                                                <div class="col-sm-3 btn_album_item item_album_item">
                                                    <form enctype="multipart/form-data" class="item">
                                                        <i class="fas fa-upload"></i>
                                                        <input name="album[]" type="file" multiple id="upload_album_item">
                                                    </form>
                                                </div>

                                                @include('frontend.dangtin._albumitem')
                                            </div>
                                        </div>

                                        <div class="error_log">
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <form id="form_dangtin" class="block">
                                <input type="hidden" value="{{$item_project->id}}" id="key_edit_project_item">

                                <h4 class="title-line flats_title"><span class="text"><i class="fas fa-clipboard-list"></i> Utilities</span></h4>
                                <div class="form-group">
                                    <div class="code-box-gid">
                                        <section class="grid-demo2 content-store">
                                            <div class="controls cf">
                                                <div class="control search">
                                                    <div class="control-icon">
                                                        <i class="material-icons">&#xE8B6;</i>
                                                    </div>
                                                    <input class="control-field search-field2 form-control " type="text" placeholder="Search..." />
                                                </div>
                                                <div class="control filter">
                                                    <div class="control-icon">
                                                        <i class="material-icons">&#xE152;</i>
                                                    </div>
                                                    <div class="select-arrow">
                                                        <i class="material-icons">&#xE313;</i>
                                                    </div>
                                                    <select class="control-field filter2-field2 form-control">
                                                        <option value="" selected>All</option>
                                                        <option value="red">Red</option>
                                                        <option value="blue">Blue</option>
                                                        <option value="green">Green</option>
                                                    </select>
                                                </div>
                                                <div class="control sort">
                                                    <div class="control-icon">
                                                        <i class="material-icons">&#xE164;</i>
                                                    </div>
                                                    <div class="select-arrow">
                                                        <i class="material-icons">&#xE313;</i>
                                                    </div>
                                                    <select class="control-field sort-field2 form-control">
                                                        <option value="order" selected>Drag</option>
                                                        <option value="title">Title (drag disabled)</option>
                                                        <option value="color">Color (drag disabled)</option>
                                                    </select>
                                                </div>
                                                <div class="control layout">
                                                    <div class="control-icon">
                                                        <i class="material-icons">&#xE871;</i>
                                                    </div>
                                                    <div class="select-arrow">
                                                        <i class="material-icons">&#xE313;</i>
                                                    </div>
                                                    <select class="control-field layout-field2 form-control">
                                                        <option value="left-top" selected>Left Top</option>
                                                        <option value="left-top-fillgaps">Left Top (fill gaps)</option>
                                                        <option value="right-top">Right Top</option>
                                                        <option value="right-top-fillgaps">Right Top (fill gaps)</option>
                                                        <option value="left-bottom">Left Bottom</option>
                                                        <option value="left-bottom-fillgaps">Left Bottom (fill gaps)</option>
                                                        <option value="right-bottom">Right Bottom</option>
                                                        <option value="right-bottom-fillgaps">Right Bottom (fill gaps)</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="grid2 load_tienich">

                                            </div>
                                            <div class="grid-footer1">
                                                <button class="btn btn-primary themtienich" type="button"><i class="fas fa-search-plus"></i> Load utilities</button>
                                            </div>
                                            <div class="grid-footer">
                                                <button class="add-more-items2 btn btn-primary"><i class="fas fa-plus"></i> Add more items</button>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <div class="panel-form--footer">
                                    <a href="/create-rental-step1/{{$item_project->id}}" class="btn btn-info">Back</a>

                                    <button id="submit-item" class="btn btn-primary">Continue</button>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
<!--/head-block-->

<script src="/public/js/cmuuri.js"></script>
@include('frontend.dangtin._food')
<script>
    var idproject = '{{$item_project->id}}';
    
    $('#avatar_item_project').change(function(event) {
        var file = this.files[0];
        if (typeof(file) === 'undefined') {
            return false;
        }
        var fileType = file["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            toastr.error('Incorrect image format', null, {timeOut: 4000});
            return false;
        }
        if (file.size > 3000000) {
            toastr.error('Up to 3MB', null, {timeOut: 4000});
            return false;
        }

        var images = $(this).closest('.file_upload_box').find('.image_review');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                images.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }

        var fd = new FormData();
        fd.append("status_code", "change_avatar");
        fd.append("id", idproject);
        if (typeof(file) !== 'undefined') {
            fd.append("avatar", file);
        }

        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_crate_item',
            data: fd,
            dataType: 'json',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                $('.block-page-all').removeClass('active');
            }
        });
    });

    $('#upload_album_item').change(function(event) {
        var files = $(this).get(0).files;
        var size = 0;
        for (i = 0; i < files.length; i++)
        {
            size = size + files[i].size;
        }

        if (size == 0) {
            return false;
        }

        if (size >= 15000000) {
            toastr.error('<p>Total capacity: '+bytesToSize(size)+'</p><p>Max: '+bytesToSize(15000000)+'</p>');
            return false;
        }

        var fd = new FormData($(this).parents('form')[0]);
        fd.append("status_code", "upload_album_item");
        fd.append("id", idproject);

        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_crate_item',
            data: fd,
            dataType: 'json',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false;
                }
                // $('.btn_album_item').after(result.value);
                $('.load_album_item .row').append(result.value);
                if (result.error_log != "") {
                    $('.error_log').html(result.error_log);
                } else {
                    $('.error_log').html("");
                }
                $('.block-page-all').removeClass('active');
            }
        });
    });

    $(document).on('click', '.clear_error', function(event) {
        $(this).closest('.error_log').html("");
    });

    $(document).on('click', '.delete_album_item', function(event) {
        var here = $(this).closest('.item_album_item');
        var data = {};
        data['status_code'] = 'delete_album_item';
        data['id'] = $(this).attr('data-id');
        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_crate_item',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                here.remove();
                $('.block-page-all').removeClass('active');
            }
        });
    });

    tinymce.init({
        selector: '#mytextarea'
    });

    var form_dangtin = $('#form_dangtin').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
            $(element).addClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        },
        rules: {
        },
        messages: {
        },
        submitHandler: function (form) {

            // get tien ich
            var tienichs = [];
            $('.load_tienich .card').each(function(index, el) {
                var vitri = $(this).find('.card-id').html();
                var id_tq = $(this).find('.card-id_tq').html();
                var data = $(this).find('.card-id_data i').attr('class');
                var title = $(this).find('.card-title').html();
                var title_en = $(this).find('.card-title_en').html();

                var item_ti = {
                    vitri:vitri,
                    id_tq:id_tq,
                    data:data,
                    title:title,
                    title_en:title_en,
                };
                tienichs.push(item_ti);
            });
            var jsonti = JSON.stringify(tienichs);

            var data = {};
            $("#form_dangtin").serializeArray().map(function(x){thongtin[x.name] = x.value;});

            data['id'] = idproject;
            data['tienichs'] = jsonti;
            data['status_code'] = "dangtinb2";

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    document.location.href = '/create-rental-step3/'+result.id;
                }
            });
            return false;
        }
    });

</script>
@endsection