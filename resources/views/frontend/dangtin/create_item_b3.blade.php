@extends('frontend.layouts.main')
@section('content')
@include('frontend.dangtin._head')
@include('frontend.nguoidung._plugin')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Post rental information</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray store-dang-tin">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content dangtin_b3">
                        <h2 class="title text-uppercase">Post rental information <small class="d-block"><b>Attention: </b>Complete the warning items (<i class="fas fa-exclamation-triangle"></i>) below to. Thank you</small></h2>
                        @include('frontend.dangtin._stepthueban')
                        @if($item_project->step == 3 && $item_project->active != 0)
                        <div class="alert alert-success" role="alert">
                            Your post has been sent to admin. Please wait for approval.
                        </div>
                        @endif
                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fas fa-database"></i> Listing information</span></h4>
                            
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Category</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($item_project->title != null)
                                    <tr>
                                        <td>Property name</td>
                                        <td>{{$item_project->title}}</td>
                                        <td>
                                            @if($item_project->title != null) 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->quanhuyen != "")
                                    <tr>
                                        <td>Cty</td>
                                        <td>{{$item_project->quanhuyen}}</td>
                                        <td>@if($item_project->quanhuyen != "") 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->tinhthanh != "")
                                    <tr>
                                        <td>Province</td>
                                        <td>{{$item_project->tinhthanh}}</td>
                                        <td>@if($item_project->tinhthanh != "") 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->map != "")
                                    <tr>
                                        <td>Map</td>
                                        <td>{{$item_project->map}}</td>
                                        <td>@if($item_project->map != "") 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->address != "")
                                    <tr>
                                        <td>Address</td>
                                        <td>{{$item_project->address}}</td>
                                        <td>@if($item_project->address != "") 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->short_content != "")
                                    <tr>
                                        <td>Short description</td>
                                        <td>{!!$item_project->short_content!!}</td>
                                        <td>@if($item_project->short_content != "") 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($item_project->price != "")
                                    <tr>
                                        <td>Rent cost</td>
                                        <td>
                                            @if($item_project->price != 0)
                                            {{$item_project->price}}$ / month
                                            
                                            @endif
                                        </td>
                                        <td>
                                            @if($item_project->price != 'null') 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fas fa-user"></i> Contact information</span></h4>
                            
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Target</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                        @if($nguoidung->name != null)
                                        <tr>
                                            <td>Name contact</td>
                                            <td>{{$nguoidung->name}}</td>
                                            <td>
                                                @if($nguoidung->name != null) 
                                                <i class="fas fa-check"></i>
                                                @else <i class="fas fa-exclamation-triangle"></i>@endif
                                            </td>
                                        </tr>
                                        @endif
                                        @if($nguoidung->address != null)
                                        <tr>
                                            <td>Address</td>
                                            <td>{{$nguoidung->address}}</td>
                                            <td>
                                                @if($nguoidung->address != null) 
                                                <i class="fas fa-check"></i>
                                                @else <i class="fas fa-exclamation-triangle"></i>@endif
                                            </td>
                                        </tr>
                                        @endif
                                        @if($nguoidung->phone != null)
                                        <tr>
                                            <td>Phone number</td>
                                            <td>{{$nguoidung->phone}}</td>
                                            <td>@if($nguoidung->phone != null) 
                                                <i class="fas fa-check"></i>
                                                @else <i class="fas fa-exclamation-triangle"></i>@endif
                                            </td>
                                        </tr>
                                        @endif
                                        @if($nguoidung->email != null)
                                        <tr>
                                            <td>Email</td>
                                            <td>{{$nguoidung->email}}</td>
                                            <td>@if($nguoidung->email != null) 
                                                <i class="fas fa-check"></i>
                                                @else <i class="fas fa-exclamation-triangle"></i>@endif
                                            </td>
                                        </tr>
                                        @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="block">
                            <h4 class="title-line"><span class="text"><i class="fas fa-image"></i> Image</span></h4>
                            
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Target</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Images</td>
                                        <td><a target="_blank" href="{{BladeGeneral::GetImg(['avatar' => @$item_project->album->name,'data' => 'item_album', 'time' => @$item_project->album->updated_at])}}" title="">
                                            <img width="100" class="image_review" src="{{BladeGeneral::GetImg(['avatar' => @$item_project->album->name,'data' => 'item_album', 'time' => @$item_project->album->updated_at])}}" alt="...">
                                        </a></td>
                                        <td>
                                            @if($item_project->album != null) 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i>@endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Album image</td>
                                        <td><a target="_blank" href="/create-rental-step2/{{$item_project->id}}" title="">Have {{$item_project->album_count}} picture in the album</a></td>
                                        <td>
                                            @if($item_project->album_count) 
                                            <i class="fas fa-check"></i>
                                            @else <i class="fas fa-exclamation-triangle"></i> No album
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        @if($item_project->step != 3)
                        <div class="panel-form--footer">
                            <a href="/create-rental-step2/{{$item_project->id}}" class="btn btn-primary">Back</a>
                            <button data-id="{{$item_project->id}}" type="button" id="submit-item_project" class="btn btn-success">Finish</button>
                        </div>
                        @endif

                    </section>
                </div>
            </div>
        </div>
    </div>
</main>


<script>
    $('#submit-item_project').click(function(event) {
        var data = {};
        $('.block-page-all').addClass('active');
        data['status_code'] = 'submit_item_project';
        data['id'] = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '/api_crate_item',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                document.location.href = '/create-rental-step3/'+result.id;
            }
        });
    });
</script>
@endsection