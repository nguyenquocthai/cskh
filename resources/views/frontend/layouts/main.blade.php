<!DOCTYPE html>
<html lang="vi">
	<head>
		@include('frontend.elements.head')
    </head>

    <body>
        @yield('content')
    </body>
</html>