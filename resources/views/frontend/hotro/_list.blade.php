<div class="aside-block">
    <div class="aside-nav">
        <h4 class="title style2">{{@$langs['ho-tro']}}</h4>
        <ul class="reset-list aside-nav__list">
            @if (count($hotros) != 0)
                @foreach ($hotros as $hotro)
                <li class="item">
                    <div class="collapse-title">
                        <button class="reset-btn collapse-btn collapsed"></button><a class="collapse-link {{@$list_active[$hotro->id]}}" href="/chi-tiet-ho-tro/{{$hotro->slug}}">{{$hotro->title}}</a>
                    </div>
                </li>
                @endforeach
            @else
            
            @endif
        </ul>
    </div>
</div>