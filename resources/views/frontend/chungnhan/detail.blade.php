@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/chung-nhan">{{@$langs['chung-nhan']}}</a></li>
                <li class="item active">{{$chungnhan->title}}</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>{{@$langs['tro-ve']}}</button>
                <div class="aside-wrap js-blurOff">
                    @include('frontend.about._list')
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">
                <article class="chungnhan">
                    <h1 class="title-detail">{{$chungnhan->title}}</h1>
                    <div class="content">
                        {!!$chungnhan->content!!}
                    </div>
                </article>
            </div>
        </div>
    </div>
</main>
@endsection