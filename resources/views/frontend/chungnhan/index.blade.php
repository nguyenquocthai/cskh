@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="fa fa-home"></i></a></li>
                <li class="item active">Chứng nhận</li>
            </ul>
        </nav>
        <div class="row">
            <aside class="col-lg-3 aside js-aside">
                <button class="reset-btn aside-close"><i class="fa fa-angle-left mr-2"></i>Trở về</button>
                <div class="aside-wrap js-blurOff">
                    @include('frontend.about._list')
                    {!!$viewproduct_cats!!}
                    @include('frontend.home._tongdai')
                </div>
            </aside>
            <div class="col-lg-9">

                @if (count($chungnhans) != 0)
                    @foreach ($chungnhans as $chungnhan)
                    <article class="about-item">
                        <figure class="img hasLink"><img src="{{BladeGeneral::GetImg(['avatar' => $chungnhan->avatar,'data' => 'chungnhan', 'time' => $chungnhan->updated_at])}}" alt=""><a class="link" href="/chi-tiet-chung-nhan/{{$chungnhan->slug}}"></a></figure>
                        <div class="content">
                            <h2 class="title text-uppercase"><a class="link" href="/chi-tiet-chung-nhan/{{$chungnhan->slug}}">{{$chungnhan->title}}</a></h2>
                            <p class="summary">{{$chungnhan->summary}}</p>
                        </div>
                    </article>
                    @endforeach
                @else
                
                @endif
                
            </div>
        </div>
    </div>
</main>
@endsection