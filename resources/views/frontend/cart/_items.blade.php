<ul class="reset-list product-table__row head">
    <li class="item">STT</li>
    <li class="item">{{@$langs['san-pham']}}</li>
    <li class="item">{{@$langs['so-luong']}}</li>
    <li class="item">{{@$langs['xoa']}}</li>
</ul>

@if (count($carts) != 0)
    @foreach ($carts as $cart)
    <ul class="reset-list product-table__row">
        <li class="item" data-title="STT:">1</li>
        <li class="item" data-title="{{@$langs['san-pham']}}:">
            <figure class="img"><img src="{{BladeGeneral::GetImg(['avatar' => $cart['item_project']->avatar,'data' => 'item_project', 'time' => $cart['item_project']->updated_at])}}" alt=""></figure>
            <div class="content">
                <div class="name">{{$cart['item_project']->title}}</div>
                <div class="des">{{$cart['item_project']->summary}}</div>
            </div>
        </li>
        <li class="item" data-title="{{@$langs['so-luong']}}:">
            <input data-id="{{$cart['id']}}" class="form-control update_cart" type="text" value="{{$cart['soluong']}}">
        </li>
        <li class="item" data-title="{{@$langs['xoa']}}:">
            <button data-id="{{$cart['id']}}" class="reset-btn remove_cart"><i class="fa fa-remove-black"></i></button>
        </li>
    </ul>
    @endforeach
@else

@endif
<ul class="reset-list product-table__row foot">
    <li class="item"><span class="text">{{@$langs['tong-cong']}}:</span><span class="value count_cart">0</span></li>
</ul>