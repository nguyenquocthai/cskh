<aside class="panel-aside">
    <button class="reset-btn panel-aside__close d-lg-none js-panelAsideTrigger"><i class="fa fa-times"></i></button>

    <div class="inner js-blurOff">
        <h4 class="title-line">
            <span class="text"><i class="far fa-newspaper"></i>{{@$langs['ho-so']}}</span>
        </h4>
        <ul class="panel-nav reset-list">
            <li class="item"><a class="{{@$favorites}}" href="/favorites"><i class="fa fa-angle-right"></i>{{@$langs['danh-sach-yeu-thich']}}</a></li>

            <li class="item"><a class="{{@$active_thongtintaikhoan}}" href="/info-account"><i class="fa fa-angle-right"></i>{{@$langs['thong-tin-ca-nhan']}}</a></li>
            <li class="item"><a class="<?php if($_SERVER['REQUEST_URI']=="/change-pass") echo "active"; ?>" href="/change-pass"><i class="fa fa-angle-right"></i>{{@$langs['doi-mat-khau']}}</a></li>
            <li class="item"><a href="/logout"><i class="fa fa-angle-right"></i>{{@$langs['dang-xuat']}}</a></li>
        </ul>
    </div>
</aside>