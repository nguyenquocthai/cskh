@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')

<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>{{@$langs['trang-chu']}}</a></li>
                <li class="item active">{{@$langs['danh-sach-yeu-thich']}}</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">

                        <h2 class="title text-uppercase">{{@$langs['danh-sach-yeu-thich']}}</h2>

                        <div class="tab-container">

                            <div class="table-responsive load_thue_ban">
                                @include('frontend.dangtin.yeuthich2_load')
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    //remove_favorite
    $('.remove_favorite').click(function(event) {
        var here = $(this).closest('tr');
        var data = {};
        data['id'] = $(this).attr('data-id');
        data['group'] = $(this).attr('data-group');
        
        $('.block-page-all').addClass('active');
        $.ajax({
            type: "POST",
            url: '/add_favorite',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
            },
            success: function(result) {
                switch (result.code) {
                    case 201:
                        here.remove();
                        toastr.success(result.message);
                        break;

                    default:
                        toastr.error(result.error);
                        break;
                }
                $('.block-page-all').removeClass('active');
            }
        });
    });
</script>
@endsection