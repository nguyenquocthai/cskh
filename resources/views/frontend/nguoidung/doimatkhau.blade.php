@extends('frontend.layouts.main')
@section('content')

@include('frontend.nguoidung._plugin')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Edit account</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray">
            
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News board</button>

                        <h2 class="title text-uppercase">Change the password</h2>
                        
                        <form id="user_form" class="panel-form" enctype="multipart/form-data">
                            <input type="hidden" name="status_code" value="changpass">
                            <div class="block">

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Old password <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="pass_old" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">New password <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input id="password" name="pass_new" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Re password <span class="cl_red">(*)</span></label>
                                    <div class="input">
                                        <input name="pass_new_re" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="panel-form--footer">
                                <button type="submit" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Save</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_form = $('#user_form').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            pass_old:{
                required: true
            },
            pass_new:{
                required: true,
                minlength : 3,
                maxlength: 15
            },
            pass_new_re:{
                equalTo : "#password",
            },
        },
        messages: {
            pass_old:{
                required: 'Old password cannot be empty.'
            },
            pass_new:{
                required: 'New password cannot be empty.',
                minlength: 'Please enter 3 - 15 characters',
                maxlength: 'Please enter 3 - 15 characters'
            },
            pass_new_re:{
                equalTo: 'Password incorrect, please try again'
            },
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#user_form").serializeArray().map(function(x){data[x.name] = x.value;});

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/suataikhoan',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    $('.block-page-all').removeClass('active');
                    toastr.success(result.message);
                }
            });
            return false;
        }
    });
</script>
@endsection