@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
@php
    $gioithieu = [];
    if ($nguoidung->gioithieu != null) {
        $gioithieu = json_decode($nguoidung->gioithieu);
    }
@endphp
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>{{@$langs['trang-chu']}}</a></li>
                <li class="item active">{{@$langs['chinh-sua-tai-khoan']}}</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <div class="content-index bg-gray">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>News Board</button>

                        <h2 class="title text-uppercase">{{@$langs['thay-doi-thong-tin-ca-nhan']}}</h2>
                        
                        <form autocomplete="off" id="user_form" class="panel-form" enctype="multipart/form-data">
                            <div class="d-flex flex-wrap block">
                                <div class="left">
                                    <h4 class="title-line"><span class="text"><i class="fa fa-picture-o"></i>{{@$langs['hinh-dai-dien']}}</span></h4>
                                    
                                    <figure class="img-upload file_upload_box">
                                        <label class="img">
                                            <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $nguoidung->avatar,'data' => 'nguoidung', 'time' => $nguoidung->updated_at])}}" alt="">
                                            <input accept="image/*" id="avatar_user" type="file" class="d-none file_image" name="avatar">
                                        </label>
                                        <button type="button" class="reset-btn btn-main text-uppercase upload_trigir">Upload</button>
                                    </figure>
                                </div>

                                <script>
                                    $('.upload_trigir').click(function(event) {
                                        $('#avatar_user').trigger('click');
                                    });
                                </script>

                                <div class="right">
                                    <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Personal information</span> <span class="badge badge-info">ID: {{md5($nguoidung->id)}}</span></h4>

                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">{{@$langs['ho-ten']}} <span class="cl_red">(*)</span></label>
                                        <div class="input">
                                            <input type="text" name="name" class="form-control" value="{{$nguoidung->name}}">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">{{@$langs['gioi-tinh']}}</label>
                                        <div class="input">
                                            @if($nguoidung->gioitinh == 3)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> {{@$langs['nam']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> {{@$langs['nu']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> {{@$langs['khac']}}
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 1)
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="male" value="1"> {{@$langs['nam']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> {{@$langs['nu']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> {{@$langs['khac']}}
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 0)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> {{@$langs['nam']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="female" value="0"> {{@$langs['nu']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="other" value="2"> {{@$langs['khac']}}
                                            </label>
                                            @endif

                                            @if($nguoidung->gioitinh == 2)
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="male" value="1"> {{@$langs['nam']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input type="radio" name="gioitinh" id="female" value="0"> {{@$langs['nu']}}
                                            </label>
                                            <label class="mt-2 mr-3">
                                                <input checked type="radio" name="gioitinh" id="other" value="2"> {{@$langs['khac']}}
                                            </label>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Phone number <span class="cl_red">(*)</span></label>
                                        <div class="input">
                                            <input name="phone" value="{{$nguoidung->phone}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Email <span class="cl_red">(*)</span></label>
                                        <div class="input">
                                            <input name="email" value="{{$nguoidung->email}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Facebook</label>
                                        <div class="input">
                                            <input name="facebook" value="{{@$gioithieu->facebook}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="panel-form__row">
                                        <label class="text mb-md-0">Address <span class="cl_red">(*)</span></label>
                                        <div class="input">
                                            <input name="address" value="{{$nguoidung->address}}" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-form--footer">
                                <button type="submit" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">{{@$langs['luu']}}</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>

        <script>
            $('.ngaysinh').datepicker({
                autoClose:true,
                dateFormat: 'yyyy-mm-dd',
                language: 'en'
            })
            var user_form = $('#user_form').validate({
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass);
                },
                rules: {
                    name:{
                        required: true
                    },
                    email:{
                        required: true
                    },
                    address:{
                        required: true
                    },
                    phone:{
                        required: true
                    },
                    hometown:{
                        required: true
                    },
                    first_language:{
                        required: true
                    },
                    relationship_status:{
                        required: true
                    }
                },
                messages: {
                    name:{
                        required: 'No name entered.'
                    },
                    email:{
                        required: 'No email entered yet'
                    },
                    address:{
                        required: 'No address entered'
                    },
                    phone:{
                        required: 'Phone number not entered'
                    },
                    hometown:{
                        required: 'Hometown not entered'
                    },
                    first_language:{
                        required: 'First language not entered'
                    },
                    relationship_status:{
                        required: 'Relationship status not entered'
                    }
                },
                submitHandler: function (form) {
            
                    var data = {};
                    $("#user_form").serializeArray().map(function(x){data[x.name] = x.value;});

                    $('.block-page-all').addClass('active');
                    $.ajax({
                        type: 'POST',
                        url: '/suataikhoan',
                        data: data,
                        dataType: 'json',
                        error: function(){
                            $('.block-page-all').removeClass('active');
                            toastr.error(result.error);
                        },
                        success: function(result) {
                            if (result.code == 300) {
                                toastr.error(result.error);
                                $('.block-page-all').removeClass('active');
                                return false;
                            }
                            $('.block-page-all').removeClass('active');
                            toastr.success(result.message);
                        }
                    });
                    return false;
                }
            });

            $('#avatar_user').change(function(event) {
                var file = this.files[0];
                if (typeof(file) === 'undefined') {
                    return false;
                }
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
                    return false;
                }
                if (file.size > 1000000) {
                    toastr.error('The right shape is smaller 1MB', null, {timeOut: 4000});
                    return false;
                }

                var images = $(this).closest('.file_upload_box').find('.image_review');
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        images.attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);
                }

                var fd = new FormData();
                fd.append("status_code", "change_avatar");
                if ($(".file_image").get(0)){
                    fd.append("avatar", $(".file_image").get(0).files[0]);
                }

                $('.block-page-all').addClass('active');
                $.ajax({
                    type: 'POST',
                    url: '/suataikhoan',
                    data: fd,
                    dataType: 'json',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    success: function(result) {
                        $('.block-page-all').removeClass('active');
                    }
                });
            });
        </script>
    </div>
</main>
@endsection