@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="fa fa-home"></i></a></li>
                <li class="item active">{{@$langs['lien-he']}}</li>
            </ul>
        </nav>
        <div class="contact-mapbox"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.2386426709877!2d106.76096851480158!3d10.869445192258421!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529528147af15%3A0x33bf313f5d842d33!2sPhan+Khang+Electric+Co.!5e0!3m2!1sen!2s!4v1565171566007!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        <div class="row">
            <div class="col-lg-6 mb-4 mb-lg-0">
                <h3 class="title-detail contact-title">{{@$info_web['name']}}</h3>
                <div class="contact-info__row">
                    <div class="caption">{{@$langs['dia-chi']}}:</div>
                    <div class="text">{{@$info_web['address']}}</div>
                </div>
                <div class="contact-info__row">
                    <div class="caption">{{@$langs['dien-thoai']}}:</div>
                    <div class="text">{{@$info_web['phone']}}</div>
                </div>
                <div class="contact-info__row">
                    <div class="caption">MST:</div>
                    <div class="text">{{@$info_web['mst']}}</div>
                </div>
                <div class="contact-info__row">
                    <div class="caption">Email:</div>
                    <div class="text">{{@$info_web['email']}}</div>
                </div>
                <div class="contact-info__row">
                    <div class="caption">Facebook:</div>
                    <div class="text">{{@$info_web['facebook']}}</div>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="title-detail contact-title black">{{@$langs['lien-he-truc-tuyen']}}</h3>
                <form class="form-contact" id="send_contact">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-contact__row">
                                <input class="form-control" name="name" type="text" placeholder="{{@$langs['ho-ten']}}">
                            </div>
                            <div class="form-contact__row">
                                <input class="form-control" name="email" type="text" placeholder="Email">
                            </div>
                            <div class="form-contact__row">
                                <input class="form-control" name="phone" type="text" placeholder="{{@$langs['dien-thoai']}}">
                            </div>
                            <div class="form-contact__row">
                                <div class="captcha-box">
                                    <input class="form-control" name="check_code" type="text" placeholder="{{@$langs['ma-kiem-tra']}}">
                                    <label class="captcha">{{session('check_code')}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <textarea name="content" class="form-control txt" placeholder="{{@$langs['noi-dung']}}"></textarea>
                        </div>
                        <div class="col-sm-12">
                            <button class="reset-btn btn-main">{{@$langs['xac-nhan']}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<script>
    var html_send_contact = $('#send_contact').html();
    var send_contact = $('#send_contact').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            name:{
                required: true
            },
            phone:{
                required: true
            },
            email:{
                required: true
            },
            content:{
                required: true
            }
        },
        messages: {
            name:{
                required: 'Chưa nhập tên.'
            },
            phone:{
                required: 'Chưa nhập số điện thoại.'
            },
            email:{
                required: 'Chưa nhập email.'
            },
            content:{
                required: 'Chưa nhập nội dung.'
            }
            
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#send_contact").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/send_contact',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    $('.block-page-all').removeClass('active');
                    toastr.success(result.message);
                    $('#send_contact').html(html_send_contact);
                }
            });
            return false;
        }
    });
</script>
@endsection