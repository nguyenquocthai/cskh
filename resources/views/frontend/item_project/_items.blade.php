<div class="product-item2">
    <figure class="img"><a class="name link" href="/chi-tiet-san-pham/{{$item_project->slug}}" title="{{$item_project->title}}">
        <img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt="">
    </a></figure>
    <div class="content">
        <div class="product-item2__col">
            <p class="name"><a class="name link" href="/chi-tiet-san-pham/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a></p>
            <p class="code">{{$item_project->masp}}</p>
            <p class="des">{{$item_project->summary}}</p>
        </div>
        <div class="product-item2__col">
            <p>{{@$langs['tinh-trang']}}: {{$item_project->tinhtrang}}</p>
            <p>{{@$langs['gia-hang']}}: {{$item_project->giahang}}</p>
            <p>{{@$langs['chiet-khau']}}: {{$item_project->chietkhau}}</p>
            <p>{{@$langs['gia-ban']}}:&nbsp;<span class="price">{{BladeGeneral::priceFormat($item_project->price)}}</span></p>
        </div>
        <div class="product-item2__col">
            <div class="quantity js-quantity">
                <button class="quantity-btn js-quantityTrigger" type="button" data-type="plus">+</button>
                <input class="quantity-val js-quantityVal" type="text" value="1">
                <button class="quantity-btn js-quantityTrigger" type="button" data-type="minus">-</button>
            </div>
            <button data-id="{{$item_project->id}}" class="reset-btn btn-main add_cart">{{@$langs['yeu-cau-bao-gia']}}</button>
        </div>
    </div>
</div>