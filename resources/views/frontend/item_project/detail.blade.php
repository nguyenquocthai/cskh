@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <div class="container">
        <nav class="breadcrumbk">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="fa fa-home"></i></a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham">{{@$langs['danh-muc-san-pham']}}</a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham/{{@$product_cat0->slug}}">{{@$product_cat0->title}}</a></li>
                <li class="item"><a class="link" href="/danh-muc-san-pham/{{@$product_cat1->slug}}">{{@$product_cat1->title}}</a></li>
                <li class="item active">{{$item_project->title}}</li>
            </ul>
        </nav>
        <div class="product-detail">
            <h1 class="title-detail">{{$item_project->title}}</h1>
            <div class="block top">
                <figure class="img"><img src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item_project', 'time' => $item_project->updated_at])}}" alt=""></figure>
                <div class="content">
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['ma-san-pham']}}:</span><span class="val"><strong>{{$item_project->masp}}</strong></span></div>
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['thuong-hieu']}}:</span><span class="val">{{$thuonghieu->title}}</span></div>
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['tinh-trang']}}:</span><span class="val">{{$item_project->tinhtrang}}</span></div>
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['gia-hang']}}:</span><span class="val">{{$item_project->giahang}}</span></div>
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['chiet-khau']}}:</span><span class="val">{{$item_project->chietkhau}}</span></div>
                    <div class="d-flex align-items-baseline mb-2"><span class="cap">{{@$langs['gia-ban']}}:</span><span class="val price">{{BladeGeneral::priceFormat($item_project->price)}}</span></div>
                    <div class="mb-3">
                        <button class="reset-btn btn-cyan">{{@$langs['dang-nhap-de-co-gia-tot-nhat']}}</button>
                    </div>
                    <div class="mb-3 quantity js-quantity">
                        <button class="quantity-btn js-quantityTrigger" type="button" data-type="plus">+</button>
                        <input class="quantity-val js-quantityVal" type="text" value="1">
                        <button class="quantity-btn js-quantityTrigger" type="button" data-type="minus">-</button>
                    </div>
                    <div class="d-flex justify-content-between mb-3">
                        <button data-id="{{$item_project->id}}" class="reset-btn btn-main add_cart2"><i class="fa fa-envelope-o"></i>{{@$langs['yeu-cau-bao-gia']}}</button>

                        @if($yeuthich == 0)
                        <button data-id="{{@$item_project->id}}" data-group="1" class="reset-btn btn-gray add_favorite">
                            <i class="fa fa-heart"></i>
                            <span>{{@$langs['yeu-thich']}}</span>
                        </button>
                        @else
                        <button data-id="{{@$item_project->id}}" data-group="1" class="reset-btn btn-gray add_favorite active">
                            <i class="fa fa-heart"></i>
                            <span>{{@$langs['bo-thich']}}</span>
                        </button>
                        @endif

                    </div>
                    <div class="mb-3"><a class="link" href="/chi-tiet-ho-tro/huong-dan-mua-hang-truc-tuyen"><i class="fa fa-reply"></i>{{@$langs['huong-dan-mua-hang']}}</a></div>
                </div>
            </div>
            <div class="block">
                <div class="social-share right">
                    <a class="download" href="/public/img/upload/item_projects/{{ $item_project->tailieu}}"><i class="fa fa-book"></i><span class="cap">{{@$langs['tai-lieu']}}</span></a>
                    <div class="text">{{@$langs['chia-se']}}:</div>
                    <ul class="reset-list list">
                        <li class="item"><a class="link" href="#!"><i class="fa fa-facebook-official cl_fb"></i></a></li>
                        <li class="item"><a class="link" href="#!"><i class="fa fa-twitter-square cl_tt"></i></a></li>
                        <li class="item"><a class="link" href="#!"><i class="fa fa-google-plus-square cl_gg"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="block">
                <h3 class="title-detail-sm">{{@$langs['mo-ta-san-pham']}}</h3>
                <div>{{$item_project->summary}}</div>
            </div>
            <div class="block">
                <h3 class="title-detail-sm">{{@$langs['thong-so-ky-thuat']}}</h3>
                <div>{!!$item_project->content!!}</div>
            </div>
            <div class="product-related block">
                <h3 class="title-detail-sm">{{@$langs['co-the-ban-quan-tam']}}</h3>

                @if (count($item_projects) != 0)
                    @foreach ($item_projects as $item_project)
                    @include('frontend.item_project._items')
                    @endforeach
                @else
                
                @endif

            </div>
        </div>
    </div>
</main>

<script>
    //add_favorite
    $('body').on('click', '.add_favorite', function (event) {
        var here = $(this);
        var data = {};
        data['id'] = $(this).attr('data-id');
        data['group'] = $(this).attr('data-group');

        $('.block-page-all').addClass('active');
        $.ajax({
            type: "POST",
            url: '/add_favorite',
            data: data,
            dataType: 'json',
            error: function () {
                $('.block-page-all').removeClass('active');
            },
            success: function (result) {
                switch (result.code) {
                    case 200:
                        $('.add_favorite[data-id="' + data['id'] + '"]').addClass('active');
                        $('.add_favorite[data-id="' + data['id'] + '"] span').html('Bỏ thích');
                        toastr.success(result.message);
                        break;

                    case 201:
                        $('.add_favorite[data-id="' + data['id'] + '"]').removeClass('active');
                        $('.add_favorite[data-id="' + data['id'] + '"] span').html('Yêu thích');
                        toastr.success(result.message);
                        break;

                    default:
                        toastr.error(result.error);
                        break;
                }
                $('.block-page-all').removeClass('active');
            }
        });
    });
</script>
@endsection