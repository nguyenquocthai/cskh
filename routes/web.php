<?php
//-< FRONTEND >--------------------------------------------------------------------------
Route::get('/pipay', 'Frontend\HomeController@pipay');
Route::post('/api-pipay', 'Frontend\HomeController@api_pipay');



//-< FRONTEND >--------------------------------------------------------------------------


//-< BACKEND >--------------------------------------------------------------------------
Route::get('/', 'Backend\AdminController@login');
Route::get('login', 'Backend\AdminController@login');
Route::group(['prefix' => 'admin', 'middleware' => 'AdminLogin'], function() {

    Route::post('/session', 'Backend\SessionController@index');

    Route::get('/logout', 'Backend\AdminController@logout');

    Route::get('/', 'Backend\AdminController@index');
    Route::get('{multi}', 'Backend\AdminController@index');

    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::get( $name.'/new', 'Backend\AdminController@index' );
    	Route::get( $name.'/edit/{multi}', 'Backend\AdminController@index' );
	}
});

// API -------------------------------------------------------------
Route::post('/api/adminusers/change_pass', 'Backend\api\AdminusersController@change_pass');
Route::group(['prefix' => 'api', 'middleware' => ['api']], function () {

    // Login
    Route::get('login', 'Backend\api\AuthController@login');
    Route::get('adminselect2s', 'Backend\api\Adminselect2sController@index');

    Route::post('adminusers', 'Backend\api\AdminusersController@change_pass');

    Route::get('save_session', 'Backend\api\AuthController@save_session');

    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::resource('admin'.$name.'s', 'Backend\api\Admin'.$name.'sController');
	}
});

//-< BACKEND >--------------------------------------------------------------------------

// clear cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "view is cleared";
});