<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class blogController extends BaseFrontendController
{
    public function __construct()
    {
        $this->boot();

        $this->middleware(function ($request, $next) {
            $blog_cats = DB::table('blog_cats')
                ->select($this->array_select('blog_cats'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0]
                ])
                ->orderBy('position', 'desc')
                ->get();
            View::share('blog_cats', $blog_cats);

            $viewproduct_cats = $this->get_product_cat();
            View::share('viewproduct_cats', $viewproduct_cats);

            return $next($request);
        });
    }

    //-------------------------------------------------------------------------------
    public function index()
    {

        $blogs = DB::table('blogs')
            ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->paginate(6);

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.blog.index')->with(compact('blogs'));
    }

    //-------------------------------------------------------------------------------
    public function cat($slug)
    {

        $blog_cat = DB::table('blog_cats')
            ->select($this->array_select('blog_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$blog_cat) {
            return redirect('/');
        }

        $blogs = DB::table('blogs')
            ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_blog_cat', '=', $blog_cat->id]
            ])
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        $danhmuc_active[$blog_cat->id] = "active";

        return view('frontend.blog.cat')->with(compact('blogs', 'blog_cat', 'danhmuc_active'));
    }

    //-------------------------------------------------------------------------------
    public function detail($slug)
    {
        $select_detail = $this->array_select('blogs');
        $select_detail[] = 'content'.@session('lang').' as content';

        $blog = DB::table('blogs')
            ->select($select_detail)
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$blog) {
            return redirect('/');
        }

        $blogs = DB::table('blogs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_blog_cat', '=', $blog->id_blog_cat],
                ['id', '!=', $blog->id]
            ])
            ->offset(0)
            ->limit(3)
            ->orderBy('position', 'desc')
            ->get();

        $blog_cat = DB::table('blog_cats')
            ->select($this->array_select('blog_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', $blog->id_blog_cat]
            ])
            ->first();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        $danhmuc_active[$blog->id_blog_cat] = "active";

        return view('frontend.blog.detail')->with(compact('blog', 'danhmuc_active', 'blog_cat', 'blogs'));
    }
}