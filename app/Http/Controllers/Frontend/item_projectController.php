<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class item_projectController extends BaseFrontendController
{

    public function __construct()
    {
        $this->boot();

        $this->middleware(function ($request, $next) {

            $viewproduct_cats = $this->get_product_cat();
            View::share('viewproduct_cats', $viewproduct_cats);

            return $next($request);
        });
    }

    public function detail($slug)
    {   
        $select_detail = $this->array_select('item_projects');
        $select_detail[] = 'content'.@session('lang').' as content';
        $item_project = DB::table('item_projects')
            ->select($select_detail)
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

       	if (!$item_project) {
       		return redirect('/');
       	}

        $thuonghieu = DB::table('thuonghieus')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', $item_project->id_thuonghieu]
            ])
            ->first();

        if (!$thuonghieu) {
            $thuonghieu = new \stdClass();
            $thuonghieu->title = 'Chưa cập nhật';
        }

        $list_cats = json_decode($item_project->id_product_cat);

        $product_cat0 = DB::table('product_cats')
    	    ->select($this->array_select('product_cats'))
    	    ->where([
    	        ['del_flg', '=', 0],
    	        ['status', '=', 0],
    	        ['id_parent', '=', 0]
    	    ])
    	    ->whereIn('id',$list_cats)
    	    ->first();

    	$product_cat1 = DB::table('product_cats')
    	    ->select($this->array_select('product_cats'))
    	    ->where([
    	        ['del_flg', '=', 0],
    	        ['status', '=', 0],
    	        ['id_parent', '!=', 0]
    	    ])
    	    ->whereIn('id',$list_cats)
    	    ->first();

        $item_projects = DB::table('item_projects')
            ->select($this->array_select('item_projects'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $item_project->id],
                ['id_product_cat', 'like', '%"'.$product_cat0->id.'"%']
            ])
            ->offset(0)
            ->limit(4)
            ->orderBy('position', 'desc')
            ->get();

        $yeuthich = DB::table('yeuthichs')
            ->where([
                ['idsp', '=', $item_project->id],
                ['iduser', '=', @session('userSession')->id],
                ['group', '=', 1],
            ])
            ->count();

        return view('frontend.item_project.detail')->with(compact('item_project', 'product_cat0', 'product_cat1', 'thuonghieu', 'item_projects', 'yeuthich'));
    }
}