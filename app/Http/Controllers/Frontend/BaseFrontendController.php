<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErrorCodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Model\setting;
use Illuminate\Support\Facades\DB;
use App\Model\activitie;
use App\Model\plan_child;
use App\Model\plan;

class BaseFrontendController extends Controller
{

    public function __construct()
    {
        $this->boot();
    }

    //-------------------------------------------------------------------------------
    public function boot()
    {

        $this->middleware(function ($request, $next) {
            $lang = session('lang');
            if ($lang == null) {
                session(['lang' => '']);
                $lang = session('lang');
            } else {
                $lang = session('lang');
            }
            View::share('lang', $lang);

            $db_langs = DB::table('db_langs')
                ->select('*')
                ->where('del_flg', '=', 0)
                ->get();
                
            $langs = [];
            foreach ($db_langs as $row_langs) {
                $langs[$row_langs->slug] = $row_langs->{'title' . $lang};
            }
            View::share('langs', $langs);

            if (session('cart')) {
                $carts = session('cart');
            } else {
                $carts = [];
            }

            View::share('count_carts', count($carts));

            $abouts = DB::table('abouts')
                ->select($this->array_select('abouts'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0]
                ])
                ->orderBy('position', 'desc')
                ->get();
            View::share('abouts', $abouts);

            $hotros = DB::table('hotros')
                ->select($this->array_select('hotros'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0]
                ])
                ->orderBy('position', 'desc')
                ->get();
            View::share('hotros', $hotros);

            $tongdais = DB::table('tongdais')
                ->select($this->array_select('tongdais'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0]
                ])
                ->orderBy('position', 'desc')
                ->get();
            View::share('tongdais', $tongdais);

            return $next($request);
        });

        date_default_timezone_set("Asia/Bangkok");

        // Get info web
        $ShopSetting_data = setting::where('key','=','info_website')->first();
        $ShopSetting = json_decode($ShopSetting_data, true);
        $info_web = json_decode($ShopSetting['value'], true);
        $info_web['updated_at'] = $this->slug(@$ShopSetting_data->updated_at);
        View::share('info_web', $info_web);

        $sliders = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        View::share('sliders', $sliders);

        $product_cats = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(12)
            ->orderBy('position', 'desc')
            ->get();
        View::share('product_cats', $product_cats);

        $thuonghieus = DB::table('thuonghieus')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(5)
            ->orderBy('position', 'desc')
            ->get();
        View::share('thuonghieus', $thuonghieus);
    }
}
