<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class cartController extends BaseFrontendController
{
    public function index()
    {
        return view('frontend.cart.index');
    }

    //-------------------------------------------------------------------------------
    public function add_cart(Request $request)
    {
        try {

            $item_cart = $request->all();
            $item_cart['time'] = time();

            if (session('cart')) {
                $carts = session('cart');
                $check = 0;
                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id){
                        $carts[$key]['soluong'] = $carts[$key]['soluong'] + $request->soluong;
                        $carts[$key]['time'] = time();
                        $check = 1;
                    }
                }

                if ($check == 0) {
                    $carts[] = $item_cart;
                }

                session(['cart' => $carts]);

            } else {
                $carts = [];
                $carts[] = $item_cart;
                session(['cart' => $carts]);
            }

            $data['code'] = 200;
            $data['viewcart'] = $this->viewcart();
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update_cart(Request $request)
    {
        try {

            if (session('cart')) {
                $carts = session('cart');
                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id){
                        $carts[$key]['soluong'] = $request->soluong;
                    }
                }

                session(['cart' => $carts]);

            } else {
                $carts = [];
                $carts[] = $item_cart;
                session(['cart' => $carts]);
            }

            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function remove_cart(Request $request)
    {
        try {

            if ($request->status_code == 'all') {
                session()->forget('cart');
            }

            if (session('cart')) {
                $carts = session('cart');
                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id){
                        unset($carts[$key]);
                    }
                }

                session(['cart' => $carts]);
            } else {
                $carts = [];
            }
    
            $data['code'] = 200;
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show_cart()
    {
        try {

            if (session('cart')) {
                $carts = session('cart');
            } else {
                $carts = [];
            }

            $data['code'] = 200;
            $data['viewcart'] = $this->viewcart();
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function viewcart()
    {
        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        usort($carts, function($a, $b) {
            return $b['time'] - $a['time'];
        });
        foreach ($carts as $key => $cart) {
            $item_project = DB::table('item_projects')
                ->select($this->array_select('item_projects'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $cart['id']]
                ])
                ->first();

            $carts[$key]['item_project'] = $item_project;
        }

        $view = View::make('frontend/cart/_items', ['carts' => $carts]);
        $data = $view->render();
        return $data;
    }

    //-------------------------------------------------------------------------------
    public function send_cart(Request $request)
    {
        try {

            if (session('cart')) {
                $carts = session('cart');
            } else {
                $carts = [];
            }

            if (count($carts) == 0) {
                $data['code'] = 300;
                $data['error'] = 'Chưa chọn sản phẩm';
                return response()->json($data, 200);
            }

            $insert_value = $request->all();
            $insert_value['position'] = $this->GetPos('carts',"");
            $insert_value['created_at'] = date("Y-m-d H:i:s");
            $insert_value['updated_at'] = date("Y-m-d H:i:s");
            $id_carts = DB::table('carts')->insertGetId($insert_value);

            foreach ($carts as $key => $cart) {
                DB::table('cart_details')->insert(
                    [
                        'id_cart' => $id_carts,
                        'id_item_project' => $cart['id'],
                        'soluong' => $cart['soluong'],
                        'updated_at' => date("Y-m-d H:i:s"),
                        'created_at' => date("Y-m-d H:i:s")
                    ]
                );
            }
            
            session()->forget('cart');
            $data['code'] = 200;
            $data['message'] = 'Gửi thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}