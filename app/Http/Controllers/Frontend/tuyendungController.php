<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class tuyendungController extends BaseFrontendController
{

    public function __construct()
    {
        $this->boot();

        $this->middleware(function ($request, $next) {

            $viewproduct_cats = $this->get_product_cat();
            View::share('viewproduct_cats', $viewproduct_cats);

            return $next($request);
        });
    }

    public function index()
    {

        $tuyendungs = DB::table('tuyendungs')
            ->select($this->array_select('tuyendungs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        $active_tuyendung = "active";

        return view('frontend.tuyendung.index')->with(compact('seopage', 'tuyendungs', 'active_tuyendung'));
    }

    public function detail($slug)
    {

        $tuyendung = DB::table('tuyendungs')
            ->select($this->array_select('tuyendungs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$tuyendung) {
            return redirect('/');
        }

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        $active_tuyendung = "active";

        return view('frontend.tuyendung.detail')->with(compact('seopage', 'tuyendung', 'active_tuyendung'));
    }
}