<?php

namespace App\Http\Controllers\Frontend\api;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use App\Exceptions\SessionUser;
use Session;
use App\Model\ShopUser;
use App\Model\ShopOrder;
use App\Model\ShopOrderItem;


class UserController extends BaseFrontendController
{
    private $sessionUser;

    public function __construct(SessionUser $sessionUser)
    {
        $this->sessionUser = $sessionUser;
    }

    public function store (Request $request)
    {
        try {

            $error_validate = ShopUser::validate();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $shop_user = new ShopUser;
            $shop_user->fullname = $request->fullname;
            $shop_user->email = $request->email;
            $shop_user->password = bcrypt($request->password);
            if ($request->gender) {
                $shop_user->gender = $request->gender;
            }
            if ($request->phone) {
                $shop_user->phone = $request->phone;
            }
            if ($request->mail_flg) {
                $shop_user->mail_flg = $request->mail_flg;
            }
            if ($request->birthday) {
                $date = str_replace('/', '-', $request->birthday);
                $birthday = date('Y-m-d', strtotime($date));
                $shop_user->birthday = $birthday;
            }
            $shop_user->del_flg = 0;
            $shop_user->save();

            $this->sessionUser->set($shop_user);

            $data['code'] = 200;
            $data['message'] = 'Tạo tài khoản thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function update(Request $request, $id)
    {
        try {

            $error_validate = ShopUser::validate_update();
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $session_user = $this->sessionUser->get();

            // save
            $shop_user = new ShopUser();
            $shop_user->exists = true;

            $shop_user->id = $id;

            $shop_user->fullname = $request->fullname;
            $shop_user->address = $request->address;
            $shop_user->province_id = $request->province_id;
            $shop_user->district_id = $request->district_id;
            $shop_user->phone = $request->phone;
            $shop_user->email = $session_user->email;
            $shop_user->del_flg = 0;

            $shop_user->save();

            $this->sessionUser->set($shop_user);

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function login (Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $user = ShopUser::where([
            ['email', '=', @$request->email],
            ['del_flg', '=', 0],
        ])->first();

        if (!$user) {
            $data['code'] = 300;
            $data['error'] = 'Sai email hoặc mật khẩu';
            return response()->json($data, 200);
        }

        if (password_verify(@$request->password, $user->password)) {

            $data['code'] = 200;
            // set data for session
            $this->sessionUser->set($user);
            $data['msg'] = 'Đăng nhập thành công';
            $data['linkback'] = "";
            if ($request->linkback != "") {
                $linkback = str_replace("_","/",$request->linkback);
                $data['linkback'] = $linkback;
            }

            $db_user = ShopUser::find($user->id);
            $db_user->count_log = $user->count_log+1;
            $db_user->save();

            return response()->json($data, 200);
        } else {
            $data['code'] = 300;
            $data['error'] = 'Sai email hoặc mật khẩu';
            return response()->json($data, 200);
        }
    }

    public function login_social(Request $request)
    {
        try {
            if ($request->email == "") {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy email. Vui lòng chọn tài khoản khác';
                return response()->json($data, 200);
            }
            $email = $request->email;

            $user = ShopUser::where([
                ['email', '=', @$request->email],
                ['del_flg', '=', 0],
            ])->first();

            if (!$user) {
                $user = new ShopUser;
                $user->fullname = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->id);
                $user->del_flg = 0;
                $user->save();
            }

            $data['code'] = 200;
            // set data for session
            $this->sessionUser->set($user);
            $data['msg'] = 'Đăng nhập thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Có lỗi xảy ra vui lòng thử lại';
            return response()->json($data, 200);
        }
    }


    public function method_delivery (Request $request)
    {
        try {
            Session::put('method_delivery', $request->all());
            $data['code'] = 200;
            $data['msg'] = 'thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Sai email hoặc mật khẩu';
            return response()->json($data, 200);
        }
    }

    public function user_edit_profile(Request $request)
    {
        try {
            $session_user = $this->sessionUser->get();

            if ($request->status_code == "change_avatar") {

                $shop_user = DB::table('shop_users')
                    ->select('avatar')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $session_user->id]
                    ])
                    ->first();

                if ($shop_user) {
                    $avatar_name = $shop_user->avatar;
                }

                $files = $request->data_files;
                $avatar = $files[0];
                $avatar_link['path'] = config('general.avatar_path');
                $avatar_link['url'] = config('general.avatar_url');
                $options['file_name'] = $avatar_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }
                $file_name = $upload ? $upload['file_name'] : $avatar_name;

                // save
                $shop_user = new ShopUser();
                $shop_user->exists = true;
                $shop_user->id = $session_user->id;
                $shop_user->avatar = $file_name;
                $shop_user->updated_at = date("Y-m-d H:i:s");
                $shop_user->save();

                $data['code'] = 200;
                $data['message'] = 'Cập nhật thành công';
                return response()->json($data, 200);
            }

            if ($request->changpass) {
                $get_password = DB::table('shop_users')
                    ->select('password')
                    ->where([
                        ['del_flg', '=', 0],
                        ['id', '=', $request->id_user],
                        ['del_flg', '=', 0],
                    ])
                    ->first();

                if (password_verify($request->password, $get_password->password)) {

                    $new_password = bcrypt($request->new_password);

                } else {
                    $data['code'] = 300;
                    $data['error'] = 'Mật khẩu cũ không chính xác.';
                    return response()->json($data, 200);
                }

                if ($request->new_password == '') {
                    $data['code'] = 300;
                    $data['error'] = 'Chưa nhập mật khẩu mới.';
                    return response()->json($data, 200);
                }

                if ($request->new_password != $request->pree_password) {
                    $data['code'] = 300;
                    $data['error'] = 'Lập lại mật khẩu không chính xác.';
                    return response()->json($data, 200);
                }
            } 

            // save
            $shop_user = new ShopUser();
            $shop_user->exists = true;
            $shop_user->id = $request->id_user;
            $shop_user->fullname = $request->fullname;
            $shop_user->phone = $request->phone;
            $shop_user->gender = $request->gender;
            if ($request->birthday) {
                $date = str_replace('/', '-', $request->birthday);
                $birthday = date('Y-m-d', strtotime($date));
                $shop_user->birthday = $birthday;
            }
            if ($request->changpass) {
                $shop_user->password = $new_password;
            }
            $shop_user->del_flg = 0;
            $shop_user->save();

            $find_users = DB::table('shop_users')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $shop_user->id]
                ])
                ->first();

            $this->sessionUser->set($find_users);

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function user_get_district($id)
    {
        try {

            $m_districts = DB::table('m_districts')
                ->select('id', 'm_district_nm')
                ->where('m_province_id', '=', $id)
                ->orderBy('id', 'desc')
                ->get();

            $data['code'] = 200;
            $data['value'] = $m_districts;
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    public function check_order(Request $request)
    {
        try {
            // get shop_orders
            $shop_orders = ShopOrder::where([
                ['del_flg', '=', 0],
                ['customer_email', '=', session('userSession')->email],
                ['order_no', '=', $request->order_code]
            ])->with(['ShopOrderItem'=> function ($ShopOrderItem) {
            }])->first();

            if (!$shop_orders) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy đơn hàng.';
                return response()->json($data, 200);
            }

            $view = View::make('frontend/profile/_check_order', ['shop_orders' => $shop_orders]);
            $data['value'] = $view->render();

            $data['code'] = 200;
            $data['message'] = 'Lấy dữ liệu thành công.';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi dữ liệu.';
            return response()->json($data, 200);
        }
    }

    public function user_edit_address(Request $request)
    {

        try {
            $session_user = $this->sessionUser->get();

            // save
            $shop_user = new ShopUser();
            $shop_user->exists = true;
            $shop_user->id = $session_user->id;
            $shop_user->congty = $request->congty;
            $shop_user->fullname = $request->fullname;
            $shop_user->phone = $request->phone;
            $shop_user->province_id = $request->province_id;
            $shop_user->address = $request->address;
            $shop_user->del_flg = 0;
            $shop_user->save();

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}