<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class thuonghieuController extends BaseFrontendController
{

    public function __construct()
    {
        $this->boot();

        $this->middleware(function ($request, $next) {

            $viewproduct_cats = $this->get_product_cat();
            View::share('viewproduct_cats', $viewproduct_cats);

            return $next($request);
        });
    }

    public function index()
    {
        $thuonghieus = DB::table('thuonghieus')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        foreach ($thuonghieus as $key => $value) {
            $item_projects = DB::table('item_projects')
                ->select($this->array_select('item_projects'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id_thuonghieu', '=', $value->id]
                ])
                ->offset(0)
                ->limit(3)
                ->orderBy('position', 'desc')
                ->get();

            if (count($item_projects) != 0) {
                $thuonghieus[$key]->item_projects = $item_projects;
            } else {
                $thuonghieus[$key]->item_projects = [];
            }
        }

        return view('frontend.thuonghieu.index')->with(compact('thuonghieus'));
    }

    public function detail($slug)
    {   
        $thuonghieu = DB::table('thuonghieus')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$thuonghieu) {
            return redirect('/');
        }

        $item_projects = DB::table('item_projects')
            ->select($this->array_select('item_projects'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_thuonghieu', '=', $thuonghieu->id]
            ])
            ->orderBy('position', 'desc')
            ->get();

        return view('frontend.thuonghieu.detail')->with(compact('item_projects', 'thuonghieu'));
    }
}