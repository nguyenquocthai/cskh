<?php

namespace App\Http\Controllers\Frontend\ajax;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\ShoppingCart;
use App\Model\ShopProduct;

class ShoppingCartController extends BaseFrontendController
{
    private $shoppingCart;

    public function __construct(ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;
    }

    // ------------------------------------------------------------------
    public function store (Request $request)
    {
        $query = $this->ProductQuery();
        array_push($query['select'], 'shop_products.quantity');

        $validator = Validator::make($request->all(), [
            'product_id' => [
                'required',
            ],
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            $data['code'] = 300;
            $data['error'] = $validator->errors();
            return response()->json($data, 200);
        }

        $lists = $this->shoppingCart->get();

        $arrInput = $request->all();

        if (!isset($arrInput['type'])) {
           $arrInput['type'] = "";
        }

        if (!$lists) {
            $product_find = DB::table('shop_products')
                ->join('TBdonvis', 'TBdonvis.id', '=', 'shop_products.donvi')
                ->select($query['select'])
                ->where('shop_products.id', '=', $request->product_id)
                ->first();
            $quantity = $product_find->quantity - $arrInput['amount'];
            if ($quantity < 0) {
                $data['code'] = 300;
                if ($product_find->quantity == 0) {
                    $data['error'] = $product_find->name.' đã bán hết.';
                } else {
                    $data['error'] = $product_find->name.' Chỉ còn '.$product_find->quantity.' '.$product_find->name_donvi;
                }
                return response()->json($data, 200);
            }
            $lists =
                [
                    $arrInput['product_id'] => [
                        'product_id' => $arrInput['product_id'],
                        'amount' => $arrInput['amount'],
                        'type' => $arrInput['type'],
                    ]
                ];
        } else {
            if (isset($lists[$arrInput['product_id']])) {
                $lists[$arrInput['product_id']]['amount'] += $arrInput['amount'];

                $product_find = DB::table('shop_products')
                    ->join('TBdonvis', 'TBdonvis.id', '=', 'shop_products.donvi')
                    ->select($query['select'])
                    ->where('shop_products.id', '=', $lists[$arrInput['product_id']])
                    ->first();

                $quantity = $product_find->quantity - $lists[$arrInput['product_id']]['amount'];
                if ($quantity < 0) {
                    $data['code'] = 300;
                    if ($product_find->quantity == 0) {
                        $data['error'] = $product_find->name.' đã bán hết.';
                    } else {
                        $data['error'] = $product_find->name.' Chỉ còn '.$product_find->quantity.' '.$product_find->name_donvi;
                    }
                    return response()->json($data, 200);
                }

            } else {

                $product_find = DB::table('shop_products')
                    ->join('TBdonvis', 'TBdonvis.id', '=', 'shop_products.donvi')
                    ->select($query['select'])
                    ->where('shop_products.id', '=', $arrInput['product_id'])
                    ->first();

                $quantity = $product_find->quantity - $arrInput['amount'];
                if ($quantity < 0) {
                    $data['code'] = 300;
                    if ($product_find->quantity == 0) {
                        $data['error'] = $product_find->name.' đã bán hết.';
                    } else {
                        $data['error'] = $product_find->name.' Chỉ còn '.$product_find->quantity.' '.$product_find->name_donvi;
                    }
                    return response()->json($data, 200);
                }

                $lists[$arrInput['product_id']] = [
                    'product_id' => $arrInput['product_id'],
                    'amount' => $arrInput['amount'],
                    'type' => $arrInput['type'],
                ];
            }
        }

        // set data for session
        $this->shoppingCart->set($lists);

        $res = $this->get_data_product($lists);
        $data['code'] = 200;
        $data['message'] = 'Lưu session thành công';
        $data['data'] = $res;

        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => [
                'required',
            ],
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            $data['code'] = 300;
            $data['error'] = $validator->errors();
            return response()->json($data, 200);
        }
        $res = [];

        $arrInput = $request->all();

        $list = $this->shoppingCart->get();

        if ($list) {
            if (isset($list[$id])) {
                if ($arrInput['amount'] <= 0) {
                    unset($list[$id]);
                } else {
                    $list[$id]['amount'] = $arrInput['amount'];
                }
            } elseif ($arrInput['amount'] > 0) {
                // create item
                $list[$arrInput['product_id']] = [
                    'product_id' => $arrInput['product_id'],
                    'amount' => $arrInput['amount']
                ];
            }
            $this->shoppingCart->set($list);
        }

        if (!$list) {
            $res = ['empty' => 1];
        } else {
            $res = $this->get_data_product($list);
        }

        $data['code'] = 200;
        $data['message'] = 'Cập nhật thành công';
        $data['data'] = $res;

        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $res = [];
        $list = $this->shoppingCart->get();
        if ($list) {
            if (isset($list[$id])) {
                unset($list[$id]);
                $this->shoppingCart->set($list);
            }
        }

        if (!$list) {
            $res = ['empty' => 1];
        } else {
            $res = $this->get_data_product($list);
        }

        $data['code'] = 200;
        $data['message'] = 'Xóa thành công';
        $data['data'] = $res;

        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    private function get_data_product($lists = []) {
        $query = $this->ProductQuery();

        $lists = $lists ?: [];

        foreach($lists as $list) {

            $product = DB::table('shop_products')
                ->join('TBdonvis', 'TBdonvis.id', '=', 'shop_products.donvi')
                ->select($query['select'])
                ->where('shop_products.id', '=', $list['product_id'])
                ->first();

            if ($product) {
                $product->avatar = config('general.shop_product_url') . $product->avatar;
            }

            if($product->newprice == 0 || $product->newprice == null){
                if ($product->price != null) {
                    $price = $product->price * $list['amount'];
                } else {
                    $price = 0;
                }
            } else {
                $price = $product->newprice * $list['amount'];
            }

            $lists[$list['product_id']]['avatar'] = $product->avatar;
            $lists[$list['product_id']]['name'] = $product->name;
            $lists[$list['product_id']]['donvi'] = $product->name_donvi;
            $lists[$list['product_id']]['price'] = $price;
        }

        return $lists;
    }
}