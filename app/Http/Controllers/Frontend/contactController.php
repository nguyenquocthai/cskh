<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class contactController extends BaseFrontendController
{

    public function index()
    {
        $check_code = $this->RandomString(5);
        session(['check_code' => $check_code]);
        return view('frontend.contact.index')->with(compact('check_code'));
    }

    //-------------------------------------------------------------------------------
    public function send_contact(Request $request)
    {
        try {
            
            $db_table = $request->all();
            unset($db_table['check_code']);

            if ($request->check_code != @session('check_code')) {
                $data['code'] = 300;
                $data['error'] = 'Sai mã xác nhận.';
                return response()->json($data, 200);
            }

            $id = $this->DB_insert($db_table, 'contacts');
            session()->forget('check_code');
            
            $data['code'] = 200;
            $data['message'] = 'Đã gửi liên hệ';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}