<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseFrontendController
{
    public function index()
    {
        $viewproduct_cats = $this->get_product_cat();

        $product_cats = DB::table('product_cats')
            ->select($this->array_select('product_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_parent', '=', 0]
            ])
            ->orderBy('position', 'asc')
            ->get();

        foreach ($product_cats as $key => $value) {
            $product_cats1 = DB::table('product_cats')
                ->select($this->array_select('product_cats'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id_parent', '=', $value->id]
                ])
                ->offset(0)
                ->limit(6)
                ->orderBy('position', 'asc')
                ->get();

            if(count($product_cats1) != 0){
                $product_cats[$key]->subcats = $product_cats1;
            } else {
                $product_cats[$key]->subcats = [];
            }
        }

        $sliders = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(12)
            ->orderBy('position', 'desc')
            ->get();

        $banners = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->get();

        $blogs = DB::table('blogs')
            ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(4)
            ->orderBy('position', 'desc')
            ->get();

        $partners = DB::table('partners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->offset(0)
            ->limit(20)
            ->orderBy('position', 'desc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.home.index')->with(compact('viewproduct_cats', 'sliders', 'product_cats', 'banners', 'blogs', 'partners'));
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function chang_lang(Request $request)
    {
        try {
            session(['lang' => $request->lang]);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function pipay(Request $request)
    {
        return view('frontend.home.pipay');
    }

    //-------------------------------------------------------------------------------
    public function api_pipay(Request $request)
    {
        try {

            $db_value = $request->all();
            if ($request->trans_service == 'Alipay') {
                $url = $this->alipay($db_value);
            }

            elseif($request->trans_service == 'WeChat'){
                $url = $this->wechat($db_value);
            } 

            else{
                $data['code'] = 300;
                $data['error'] = 'Trans service not found.';
                return response()->json($data, 200);
            }

            $data['code'] = 200;
            $data['url'] = $url;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function alipay($db_value)
    {
        $total_fee = $db_value['total_fee'];
        $merchant = '103';
        $partner_trans_id = time();
        $success_url = 'https://intl-acq-test.pipay.com/alipayOnlineQRCode/successResponse.php';
        $failed_url = 'https://intl-acq-test.pipay.com/alipayOnlineQRCode/failedResponse.php';
        $t = time();
        $pre_sign = 'oykcs305g5lfnrga4bia55w743fw';
        $str = $t.$merchant.$pre_sign.$partner_trans_id.$total_fee;
        $hash = hash('sha256', $str);
        $url = 'https://intl-acq-test.pipay.com/AlipayOnlinePayment/PaymentTran?merchant_id='.$merchant.'&total_fee='.$total_fee.'&device_timestamp='.$t.'&hash='.$hash.'&return_url=https://alipay-test.pipay.com/successResponse.php&partner_trans_id='.$partner_trans_id.'&business_type=1&refer_url=https://global.alipay.com/doc/global/acquire_cancel&success_url=http://alipay-pg-test.pipay.com/alipay/successResponse.php&failed_url=http://alipay-pg-test.pipay.com/alipay/failedResponse.php&check_in_time=2019-06-25&check_out_time=2019-06-27';

        $partner_trans_id = '1568527938256';
        $str = $partner_trans_id.$pre_sign;
        $hash = hash('sha256', $str);
 
        $checklink = '<a href="https://intl-acq-test.pipay.com/AlipayOnlinePayment/singleTradeQuery?partner_trans_id='.$partner_trans_id.'&hash='.$hash.'">Query Single Query</a>';

        session(['checklink'=>$checklink]);

        return $url;
    }

    //-------------------------------------------------------------------------------
    public function wechat($db_value)
    {
        $total_fee = $db_value['total_fee'];
        $merchant = '111';
        $pre_sign = 'ukw5w50btxpvmej8yz9gp7sfkje02pft';
        
        $partner_trans_id = time().'';
        $success_url = 'https://intl-acq-test.pipay.com/weChatOnlineQRCode/successResponse.php';
        $failed_url = 'https://intl-acq-test.pipay.com/weChatOnlineQRCode/failedResponse.php';
        $trans_service = 'WeChat';
        $t = time();
        $str = $t.$merchant.$pre_sign.$partner_trans_id.$total_fee;
        $hash = hash('sha256', $str);
        $url = 'https://intl-acq-test.pipay.com/WeChatOnlineQRCodePayments/nativePay?&merchant_id='.$merchant.'&trans_service='.$trans_service.'&total_fee='.$total_fee.'&device_timestamp='.$t.'&hash='.$hash .'&partner_trans_id='.$partner_trans_id.'&success_url='.$success_url.'&fail_url='.$failed_url;

        return $url;
    }
}