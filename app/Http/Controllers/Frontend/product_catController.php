<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class product_catController extends BaseFrontendController
{
    public function index()
    {
        $product_cats = DB::table('product_cats')
            ->select($this->array_select('product_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_parent', '=', 0]
            ])
            ->orderBy('position', 'asc')
            ->get();

        foreach ($product_cats as $key => $value) {
            $find_item_project = DB::table('item_projects')
                ->select($this->array_select('item_projects'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id_product_cat', 'like', '%"'.$value->id.'"%']
                ])
                ->first();

            $product_cats1 = DB::table('product_cats')
                ->select($this->array_select('product_cats'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id_parent', '=', $value->id]
                ])
                ->orderBy('position', 'asc')
                ->get();

            if(count($product_cats1) != 0){
                $product_cats[$key]->subcats = $product_cats1;

                foreach ($product_cats1 as $key2 => $value2) {
                    $item_projects = DB::table('item_projects')
                        ->select($this->array_select('item_projects'))
                        ->where([
                            ['del_flg', '=', 0],
                            ['status', '=', 0],
                            ['id_product_cat', 'like', '%"'.$value2->id.'"%']
                        ])
                        ->orderBy('position', 'desc')
                        ->get();

                    if (count($item_projects) != 0) {
                        $product_cats[$key]->subcats[$key2]->item_projects = $item_projects;
                    } else {
                        $product_cats[$key]->subcats[$key2]->item_projects = [];
                    }
                }
            } else {
                $product_cats[$key]->subcats = [];
            }
            
        }

        return view('frontend.product_cat.index')->with(compact('product_cats'));
    }

    public function detail($slug)
    {   
        $widget_product_cats = $this->get_product_cat();
        $product_cat = DB::table('product_cats')
            ->select($this->array_select('product_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        if (!$product_cat) {
            return redirect('/');
        }

        if ($product_cat->id_parent == 0) {
            $product_catsubs = DB::table('product_cats')
                ->select($this->array_select('product_cats'))
                ->where([
                    ['del_flg', '=', 0],
                    ['id_parent', '=', $product_cat->id]
                ])
                ->orderBy('position', 'asc')
                ->get();

            if(count($product_catsubs) != 0){
                foreach ($product_catsubs as $key => $value) {
                    $item_projects = DB::table('item_projects')
                        ->select($this->array_select('item_projects'))
                        ->where([
                            ['del_flg', '=', 0],
                            ['status', '=', 0],
                            ['id_product_cat', 'like', '%"'.$value->id.'"%']
                        ])
                        ->offset(0)
                        ->limit(6)
                        ->orderBy('position', 'desc')
                        ->get();

                    if (count($item_projects) != 0) {
                        $product_catsubs[$key]->item_projects = $item_projects;
                    } else {
                        $product_catsubs[$key]->item_projects = [];
                    }
                }
            }
            return view('frontend.product_cat.detail')->with(compact('widget_product_cats', 'product_catsubs', 'product_cat'));
        } else {
            $product_catparent = DB::table('product_cats')
                ->select($this->array_select('product_cats'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $product_cat->id_parent]
                ])
                ->first();

            $item_projects = DB::table('item_projects')
                ->select($this->array_select('item_projects'))
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id_product_cat', 'like', '%"'.$product_cat->id.'"%']
                ])
                ->orderBy('position', 'desc')
                ->paginate(6);

            $tailieu = [];
            $tailieu_path = config('general.product_cat_path').$product_cat->tailieu;
            if(file_exists($tailieu_path) && $product_cat->tailieu != null) {
                $tailieu['url'] = config('general.product_cat_url').$product_cat->tailieu;
                $tailieu['size'] = $this->formatBytes(filesize($tailieu_path));
            }

            $tailieu_en = [];
            $tailieu_en_path = config('general.product_cat_path').$product_cat->tailieu_en;
            if(file_exists($tailieu_en_path) && $product_cat->tailieu_en != null) {
                $tailieu_en['url'] = config('general.product_cat_url').$product_cat->tailieu_en;
                $tailieu_en['size'] = $this->formatBytes(filesize($tailieu_en_path));
            }

            $tailieu_en_path = config('general.product_cat_path').$product_cat->tailieu_en;


            return view('frontend.product_cat.detail_catsub')->with(compact('widget_product_cats', 'item_projects', 'product_catparent', 'product_cat', 'tailieu', 'tailieu_en'));
        }
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function chang_lang(Request $request)
    {
        try {
            session(['lang' => $request->lang]);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}