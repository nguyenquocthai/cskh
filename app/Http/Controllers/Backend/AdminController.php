<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model;
use App\Model\FunctionAuthority;

class AdminController extends BaseAdminController
{
    public function index () {
        // if (Auth::user()->list_rule != null)
        //     $list_rules = json_decode(Auth::user()->list_rule, true);
        // else
        //     $list_rules = [];

        // $list_url = [];
        // foreach($list_rules as $list_rule) {
        //     $function = FunctionAuthority::where([
        //         ['id', '=', $list_rule['id']],
        //     ])->first();

        //     if($function) {
        //         $list_url[] = $function->role_json;
        //     }
        // }
        // $role = Auth::user()->role;
        
        return view('Backend/Admin/index');
    }

    public function login (Request $request) {

        if (Auth::check()) {
            return redirect('admin');
        }
        return view('Backend/Admin/login');
    }

    public function logout (Request $request) {
        Auth::logout();
        session()->forget('admin');
        session()->forget('admin_style');
        return redirect('/admin');
    }

}
