<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\CustomerType;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admincustomer_typesController extends BaseAdminController
{

    public function index (Request $request) {

        $customer_types = DB::table('customer_types')
            ->join('customer_countries', 'customer_countries.id', '=', 'customer_types.id_country')
            ->join('customers_levels', 'customers_levels.id', '=', 'customer_types.id_customerlevel')
            ->select('customer_types.*','customer_countries.name as quocgia','customers_levels.name as phancap')
            ->where([
                ['customer_types.del_flg', '=', 0]
            ])
            ->orderBy('customer_types.id', 'desc')
            ->get();

        $output = [];
        
        foreach ($customer_types as $key => $customer_type) {
            $quocgia = $customer_type->quocgia;
            $phancap = $customer_type->phancap;
            $row = $this->GetRow($customer_type,$quocgia,$phancap);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $customer_type = DB::table('customer_types')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $customer_type->status = (string)$customer_type->status;

            if ($customer_type) {
                $customer_type->avatar = config('general.customer_type_url') . $customer_type->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $customer_type;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = CustomerType::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }
            if($request['value']['id_country'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn quốc gia';
                return response()->json($data, 200);
            }
            if($request['value']['id_customerlevel'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn phân cấp';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customer_type');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = CustomerType::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'customer_type', $id);

            if ($request->status_code == 'edit') {

                $customer_type = DB::table('customer_types')
                ->join('customer_countries', 'customer_countries.id', '=', 'customer_types.id_country')
                ->join('customers_levels', 'customers_levels.id', '=', 'customer_types.id_customerlevel')
                ->select('customer_types.*','customer_countries.name as quocgia','customers_levels.name as phancap')
                ->where([
                    ['customer_types.del_flg', '=', 0],
                    ['customer_types.id', '=', $id]
                ])
                ->first();

                $quocgia = $customer_type->quocgia;
                $phancap = $customer_type->phancap;

                $edit_db['row'] = $this->GetRow($edit_db['row'],$quocgia,$phancap);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $customer_type = DB::table('customer_types')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$customer_type) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            $partner_count = DB::table('partners')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_customertype', '=', $customer_type->id],
                ])
                ->count();
            if ($partner_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết đối tác.';
                return response()->json($data, 200);
            }

            CustomerType::where([
                ['id', $customer_type->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.customer_type_path').$customer_type->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customer_type,$quocgia,$phancap)
    {
        $row = [];
        $row[] = $customer_type->id;
        
        $row[] = '<a  title="">'.$customer_type->name.'</a>';
        $row[] = $quocgia;
        $row[] = $phancap;
        // $row[] = $this->GetImg([
        //     'avatar'=> $customer_type->avatar,
        //     'data'=> 'customer_type',
        //     'time'=> $customer_type->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$customer_type->updated_at.'</span>'.date('d/m/Y', strtotime($customer_type->updated_at));
        $view = View::make('Backend/customer_type/_status', ['status' => $customer_type->status]);
        $row[] = $view->render();
        $view = View::make('Backend/customer_type/_actions', ['id' => $customer_type->id,'page' => 'customer_type']);
        $row[] = $view->render();

        return $row;
    }
}
