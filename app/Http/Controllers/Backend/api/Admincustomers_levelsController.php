<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\CustomersLevel;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admincustomers_levelsController extends BaseAdminController
{

    public function index (Request $request) {

        $customers_levels = DB::table('customers_levels')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();



        $output = [];
        
        foreach ($customers_levels as $key => $customers_level) {
            
            $row = $this->GetRow($customers_level);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $customers_level = DB::table('customers_levels')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $customers_level->status = (string)$customers_level->status;

            if ($customers_level) {
                $customers_level->avatar = config('general.customers_level_url') . $customers_level->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $customers_level;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = CustomersLevel::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customers_level');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = CustomersLevel::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'customers_level', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $customers_level = DB::table('customers_levels')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$customers_level) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            $customer_type_count = DB::table('customer_types')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_customerlevel', '=', $customers_level->id],
                ])
                ->count();
            if ($customer_type_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết loại khách hàng.';
                return response()->json($data, 200);
            }

            $user_count = DB::table('users')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_customerlavel', '=', $customers_level->id],
                ])
                ->count();
            if ($user_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết user.';
                return response()->json($data, 200);
            }

            CustomersLevel::where([
                ['id', $customers_level->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.customers_level_path').$customers_level->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customers_level)
    {
        $row = [];
        $row[] = $customers_level->id;
        
        $row[] = '<a  title="">'.$customers_level->name.'</a>';
        $row[] = number_format($customers_level->discount).'%';
        $row[] = number_format($customers_level->moneylandmark);
        $row[] = number_format($customers_level->numbercontracts);
        // $row[] = $this->GetImg([
        //     'avatar'=> $customers_level->avatar,
        //     'data'=> 'customers_level',
        //     'time'=> $customers_level->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$customers_level->updated_at.'</span>'.date('d/m/Y', strtotime($customers_level->updated_at));
        $view = View::make('Backend/customers_level/_status', ['status' => $customers_level->status]);
        $row[] = $view->render();
        $view = View::make('Backend/customers_level/_actions', ['id' => $customers_level->id,'page' => 'customers_level']);
        $row[] = $view->render();

        return $row;
    }
}
