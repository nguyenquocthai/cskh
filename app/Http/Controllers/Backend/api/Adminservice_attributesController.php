<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\ServiceAttribute;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminservice_attributesController extends BaseAdminController
{

    public function index (Request $request) {

        $service_attributes = DB::table('service_attributes')
            ->join('services', 'services.id', '=', 'service_attributes.id_service')
            ->join('users', 'users.id', '=', 'service_attributes.id_user')
            ->select('service_attributes.*','services.name as dichvu','users.name as taikhoan')
            ->where([
                ['service_attributes.del_flg', '=', 0]
            ])
            ->orderBy('service_attributes.id', 'desc')
            ->get();

        $output = [];
        
        foreach ($service_attributes as $key => $service_attribute) {
            $dichvu = $service_attribute->dichvu;
            $taikhoan = $service_attribute->taikhoan;
            $row = $this->GetRow($service_attribute,$dichvu,$taikhoan);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $service_attribute = DB::table('service_attributes')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $service_attribute->status = (string)$service_attribute->status;

            if ($service_attribute) {
                $service_attribute->avatar = config('general.service_attribute_url') . $service_attribute->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $service_attribute;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = ServiceAttribute::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            if($request['value']['id_service'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn loại dịch vụ';
                return response()->json($data, 200);
            }
            if($request['value']['id_user'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn user';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'service_attribute');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = ServiceAttribute::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'service_attribute', $id);

            if ($request->status_code == 'edit') {

                $service_attribute = DB::table('service_attributes')
                ->join('services', 'services.id', '=', 'service_attributes.id_service')
                ->join('users', 'users.id', '=', 'service_attributes.id_user')
                ->select('service_attributes.*','services.name as dichvu','users.name as taikhoan')
                ->where([
                    ['service_attributes.del_flg', '=', 0],
                    ['service_attributes.id', '=', $id]
                ])
                ->first();
                $dichvu = $service_attribute->dichvu;
                $taikhoan = $service_attribute->taikhoan;

                $edit_db['row'] = $this->GetRow($edit_db['row'],$dichvu,$taikhoan);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $service_attribute = DB::table('service_attributes')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$service_attribute) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            ServiceAttribute::where([
                ['id', $service_attribute->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.service_attribute_path').$service_attribute->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($service_attribute,$dichvu,$taikhoan)
    {
        $row = [];
        $row[] = $service_attribute->id;
        
        $row[] = '<a  title="">'.$service_attribute->name.'</a>';
        $row[] = $dichvu;
        $row[] = $taikhoan;
        // $row[] = $this->GetImg([
        //     'avatar'=> $service_attribute->avatar,
        //     'data'=> 'service_attribute',
        //     'time'=> $service_attribute->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$service_attribute->updated_at.'</span>'.date('d/m/Y', strtotime($service_attribute->updated_at));
        $view = View::make('Backend/service_attribute/_status', ['status' => $service_attribute->status]);
        $row[] = $view->render();
        $view = View::make('Backend/service_attribute/_actions', ['id' => $service_attribute->id,'page' => 'service_attribute']);
        $row[] = $view->render();

        return $row;
    }
}
