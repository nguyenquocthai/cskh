<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\Customer;
use App\Model\Note;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmincustomersController extends BaseAdminController
{

    public function index (Request $request) {

        $customers = DB::table('customers')
            ->join('customer_countries', 'customer_countries.id', '=', 'customers.id_country')
            ->join('partners', 'partners.id', '=', 'customers.id_partner')
            ->select('customers.*','customer_countries.name as quocgia','partners.name as doitac')
            ->where([
                ['customers.del_flg', '=', 0]
            ])
            ->orderBy('customers.id', 'desc')
            ->get();

        $output = [];
        
        foreach ($customers as $key => $customer) {
            $quocgia = $customer->quocgia;
            $doitac = $customer->doitac;
            $row = $this->GetRow($customer,$quocgia,$doitac);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $customer = DB::table('customers')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $customer->status = (string)$customer->status;

            if ($customer) {
                $customer->avatar = config('general.customer_url') . $customer->avatar;
            }

            $note1 = DB::table('notes')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_customer', '=', $id]
                ])
                ->get();

            $view = View::make('Backend/customer/_note', ['note' => $note1]);
            $viewrender1 = $view->render();
            $customer->viewrender = $viewrender1;
            $customer->note = $note1;
            $data['code'] = 200;
            $data['data'] = $customer;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = Customer::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }
            if($request['value']['id_country'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn quốc gia';
                return response()->json($data, 200);
            }
            if($request['value']['id_customerlevel'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn phân cấp';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customer');

            return response()->json($save_db, 200);

            

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {   
        // echo "<pre>";
        //         print_r($request->all());
        //         echo "</pre>";die();
        try {

            if ($request->status_code == 'create_note') {
                
                DB::table('notes')->insert([
                    'name_note' => $request->value['name_note'],
                    'id_customer' => $request->value['id_customer'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'del_flg' => 0
                 ]);

                $data['code'] = 200;
                $data['message'] = 'Lưu thành công';
                return response()->json($data, 200);

            }else if ($request->status_code == 'edit') {
                 // validate
                $error_validate = Customer::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
                $db_customer = $request->all();
                unset($db_customer['value']['name_note']);

                $edit_db = $this->EditDB($db_customer,'customer', $id);

                $customer = DB::table('customers')
                ->join('customer_countries', 'customer_countries.id', '=', 'customers.id_country')
                ->join('partners', 'partners.id', '=', 'customers.id_partner')
                ->select('customers.*','customer_countries.name as quocgia','partners.name as doitac')
                ->where([
                    ['customers.del_flg', '=', 0],
                    ['customers.id', '=', $id]
                ])
                ->first();

                $quocgia = $customer->quocgia;
                $doitac = $customer->doitac;

                $edit_db['row'] = $this->GetRow($edit_db['row'],$quocgia,$doitac);
                
                return response()->json($edit_db, 200);
            }

            

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {
        // echo "<pre>";
        //         print_r(substr($id, 0, 1));
        //         echo "</pre>";die();
        try {

            if(strlen($id) > 1){
                $id = substr($id, 0, 1);
                $note = DB::table('notes')
                    ->select('*')
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->first();

                if (!$note) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                Note::where([
                    ['id', $note->id]
                ])->update(['del_flg' => 1]);

                $image_path = config('general.note_path').@$note->avatar;
                if(isset($image_path))
                File::delete($image_path);

                $data['code'] = 200;
                $data['message'] = 'Xóa thành công';
                return response()->json($data, 200);
            }else{
                $customer = DB::table('customers')
                    ->select('*')
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->first();

                if (!$customer) {
                    $data['code'] = 300;
                    $data['error'] = 'Không tìm thấy.';
                    return response()->json($data, 200);
                }

                Customer::where([
                    ['id', $customer->id]
                ])->update(['del_flg' => 1]);

                $image_path = config('general.customer_path').$customer->avatar;
                if(isset($image_path))
                File::delete($image_path);

                $data['code'] = 200;
                $data['message'] = 'Xóa thành công';
                return response()->json($data, 200);
            }

            

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customer,$quocgia,$doitac)
    {
        $row = [];
        $row[] = $customer->id;
        
        $row[] = '<a  title="">'.$customer->name.'</a>';
        $row[] = $quocgia;
        $row[] = $doitac;
        $row[] = $this->GetImg([
            'avatar'=> $customer->avatar,
            'data'=> 'customer',
            'time'=> $customer->updated_at
        ]);
        $row[] = '<span class="hidden">'.$customer->updated_at.'</span>'.date('d/m/Y', strtotime($customer->updated_at));
        $view = View::make('Backend/customer/_status', ['status' => $customer->status]);
        $row[] = $view->render();
        $view = View::make('Backend/customer/_actions', ['id' => $customer->id,'page' => 'customer']);
        $row[] = $view->render();

        return $row;
    }
}
