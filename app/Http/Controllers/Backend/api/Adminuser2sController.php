<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\User;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Adminuser2sController extends BaseAdminController
{

    public function index (Request $request) {
        
        $user2s = User::where([
            ['del_flg', 0],
            ['status', 2]
        ])->get();
        

        $output = [];
        
        foreach ($user2s as $key => $user2) {
            
            $row = $this->GetRow($user2);
            $output[] = $row;
        }
        

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }
    

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        

            try {

            // validate
            $error_validate = User::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            if($request['value']['password'] == null){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng nhập password';
                return response()->json($data, 200);
            }
            if($request['value']['id_usergroup'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn phòng ban';
                return response()->json($data, 200);
            }

            if($request['value']['id_customerlavel'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn phân cấp';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'user');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = User::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'user', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function change_pass(Request $request)
    {
        try {

            $find_user = DB::table('users')
                ->select('id','password')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', @Auth::user()->id],
                    ['remember_token', '=', @Auth::user()->remember_token],
                ])
                ->first();
            $error = [];

            if ($request->password_old == "") {
                $error[] = 'Chưa nhập mật khẩu củ.';
            } elseif (!Hash::check($request->password_old, $find_user->password)) {
                $error[] = 'Sai mật khẩu củ.';
            }

            if ($request->password_new == "") {
                $error[] = 'Chưa nhập mật khẩu mới.';
            } elseif (strlen($request->password_new) < 8) {
                $error[] = 'Mật khẩu mới phải nhiều hơn 8 ký tự.';
            }

            if (count($error) != 0) {
                $data_error = "";
                foreach ($error as $key => $value) {
                    $data_error .= "<div>".$value."</div>";
                }
                $data['code'] = 300;
                $data['error'] = $data_error;
                return response()->json($data, 200);
            }

            if ($find_user) {
                DB::table('users')
                    ->where('id', $find_user->id)
                    ->update(['password' => bcrypt($request->password_new)]);

                $data['code'] = 200;
                $data['message'] = 'Đổi mật khẩu thành công.';
                return response()->json($data, 200);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Sai thông tin.';
                return response()->json($data, 200);
            }

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $user2 = DB::table('users')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $user2->status = (string)$user2->status;

            if ($user2) {
                $user2->avatar = config('general.user_url') . $user2->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $user2;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $user2 = User::where('id', $id)->update(['del_flg' => 1]);
            $image_path = config('general.user_path').$user2->avatar;
            if(isset($image_path)){
                File::delete($image_path);
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($user2)
    {
        $row = [];
        $row[] = $user2->id;
        $row[] = $user2->name;
        $row[] = $user2->email;

        $row[] = $this->GetImg([
            'avatar'=> $user2->avatar,
            'data'=> 'user',
            'time'=> $user2->updated_at
        ]);

        $row[] = '<span class="hidden">'.$user2->updated_at.'</span>'.date("d/m/Y", strtotime($user2->updated_at));

        $view = View::make('Backend/user2/_status', ['status' => $user2->status]);
        $row[] = $view->render();

        $view = View::make('Backend/user2/_actions', ['id' => $user2->id,'page' => 'user']);
        $row[] = $view->render();

        return $row;
    }

}