<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\Service;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminservicesController extends BaseAdminController
{

    public function index (Request $request) {

        $services = DB::table('services')
            ->join('customer_countries', 'customer_countries.id', '=', 'services.id_country')
            ->join('users', 'users.id', '=', 'services.id_user')
            ->select('services.*','services.name as dichvu','users.name as taikhoan')
            ->where([
                ['services.del_flg', '=', 0]
            ])
            ->orderBy('services.id', 'desc')
            ->get();

        $output = [];
        
        foreach ($services as $key => $service) {
            $dichvu = $service->dichvu;
            $taikhoan = $service->taikhoan;
            $row = $this->GetRow($service,$dichvu,$taikhoan);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $service = DB::table('services')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $service->status = (string)$service->status;

            if ($service) {
                $service->avatar = config('general.service_url') . $service->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $service;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = Service::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            if($request['value']['id_country'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn quốc gia';
                return response()->json($data, 200);
            }
            if($request['value']['id_user'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn user';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'service');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = Service::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'service', $id);

            if ($request->status_code == 'edit') {

                $service = DB::table('services')
                ->join('customer_countries', 'customer_countries.id', '=', 'services.id_country')
                ->join('users', 'users.id', '=', 'services.id_user')
                ->select('services.*','services.name as dichvu','users.name as taikhoan')
                ->where([
                    ['services.del_flg', '=', 0],
                    ['services.id', '=', $id]
                ])
                ->first();
                $dichvu = $service->dichvu;
                $taikhoan = $service->taikhoan;

                $edit_db['row'] = $this->GetRow($edit_db['row'],$dichvu,$taikhoan);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $service = DB::table('services')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$service) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            Service::where([
                ['id', $service->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.service_path').$service->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($service,$dichvu,$taikhoan)
    {
        $row = [];
        $row[] = $service->id;
        
        $row[] = '<a  title="">'.$service->name.'</a>';
        $row[] = $dichvu;
        $row[] = $taikhoan;
        // $row[] = $this->GetImg([
        //     'avatar'=> $service->avatar,
        //     'data'=> 'service',
        //     'time'=> $service->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$service->updated_at.'</span>'.date('d/m/Y', strtotime($service->updated_at));
        $view = View::make('Backend/service/_status', ['status' => $service->status]);
        $row[] = $view->render();
        $view = View::make('Backend/service/_actions', ['id' => $service->id,'page' => 'service']);
        $row[] = $view->render();

        return $row;
    }
}
