<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\UserGroup;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminuser_groupsController extends BaseAdminController
{

    public function index (Request $request) {

        $user_groups = DB::table('user_groups')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();



        $output = [];
        
        foreach ($user_groups as $key => $user_group) {
            
            $row = $this->GetRow($user_group);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $user_group = DB::table('user_groups')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $user_group->status = (string)$user_group->status;

            if ($user_group) {
                $user_group->avatar = config('general.user_group_url') . $user_group->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $user_group;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = UserGroup::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }
            
            if($request['value']['id_country'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn quốc gia';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'user_group');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = UserGroup::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'user_group', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $user_group = DB::table('user_groups')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            //kiem tra khoa ngoai

            $user_count = DB::table('users')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_usergroup', '=', $user_group->id],
                ])
                ->count();
            if ($user_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết user.';
                return response()->json($data, 200);
            }

            if (!$user_group) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            UserGroup::where([
                ['id', $user_group->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.user_group_path').$user_group->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($user_group)
    {
        $row = [];
        $row[] = $user_group->id;
        
        $row[] = '<a  title="">'.$user_group->name.'</a>';
        // $row[] = $this->GetImg([
        //     'avatar'=> $user_group->avatar,
        //     'data'=> 'user_group',
        //     'time'=> $user_group->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$user_group->updated_at.'</span>'.date('d/m/Y', strtotime($user_group->updated_at));
        $view = View::make('Backend/user_group/_status', ['status' => $user_group->status]);
        $row[] = $view->render();
        $view = View::make('Backend/user_group/_actions', ['id' => $user_group->id,'page' => 'user_group']);
        $row[] = $view->render();

        return $row;
    }
}
