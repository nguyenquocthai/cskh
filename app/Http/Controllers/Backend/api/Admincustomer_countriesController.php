<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\CustomerCountry;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admincustomer_countriesController extends BaseAdminController
{

    public function index (Request $request) {

        $customer_countries = DB::table('customer_countries')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();



        $output = [];
        
        foreach ($customer_countries as $key => $customer_countrie) {
            
            $row = $this->GetRow($customer_countrie);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $customer_countrie = DB::table('customer_countries')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $customer_countrie->status = (string)$customer_countrie->status;

            if ($customer_countrie) {
                $customer_countrie->avatar = config('general.customer_countrie_url') . $customer_countrie->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $customer_countrie;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = CustomerCountry::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customer_countrie');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = CustomerCountry::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'customer_countrie', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $customer_countrie = DB::table('customer_countries')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            //kiem tra khoa ngoai

            $customer_type_count = DB::table('customer_types')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_country', '=', $customer_countrie->id],
                ])
                ->count();
            if ($customer_type_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết loại khách hàng.';
                return response()->json($data, 200);
            }

            $user_group_count = DB::table('user_groups')
                ->select('id')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_country', '=', $customer_countrie->id],
                ])
                ->count();
            if ($user_group_count > 0){
                $data['code'] = 300;
                $data['error'] = 'Còn mục liên kết phòng ban.';
                return response()->json($data, 200);
            }

            if (!$customer_countrie) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            CustomerCountry::where([
                ['id', $customer_countrie->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.customer_countrie_path').$customer_countrie->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customer_countrie)
    {
        $row = [];
        $row[] = $customer_countrie->id;
        
        $row[] = '<a  title="">'.$customer_countrie->name.'</a>';
        // $row[] = $this->GetImg([
        //     'avatar'=> $customer_countrie->avatar,
        //     'data'=> 'customer_countrie',
        //     'time'=> $customer_countrie->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$customer_countrie->updated_at.'</span>'.date('d/m/Y', strtotime($customer_countrie->updated_at));
        $view = View::make('Backend/customer_countrie/_status', ['status' => $customer_countrie->status]);
        $row[] = $view->render();
        $view = View::make('Backend/customer_countrie/_actions', ['id' => $customer_countrie->id,'page' => 'customer_countrie']);
        $row[] = $view->render();

        return $row;
    }
}
