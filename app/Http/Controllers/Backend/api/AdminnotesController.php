<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\Note;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminnotesController extends BaseAdminController
{

    public function index (Request $request) {

        $notes = DB::table('notes')
            ->join('customers', 'customers.id', '=', 'notes.id_user')
            ->select('notes.*','customers.name as user')
            ->where([
                ['notes.del_flg', '=', 0]
            ])
            ->orderBy('notes.id', 'desc')
            ->get();

        $output = [];
        
        foreach ($notes as $key => $note) {
            $user = $note->user;
            $row = $this->GetRow($note,$user);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $note = DB::table('notes')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $note->status = (string)$note->status;

            if ($note) {
                $note->avatar = config('general.note_url') . $note->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $note;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = Note::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }
            

            $save_db = $this->SaveDB($request->all(),'note');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = Note::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'note', $id);

            if ($request->status_code == 'edit') {

                $note = DB::table('notes')
                ->join('customers', 'customers.id', '=', 'notes.id_user')
                ->select('notes.*','customers.name as user')
                ->where([
                    ['notes.del_flg', '=', 0],
                    ['notes.id', '=', $id]
                ])
                ->first();

                $user = $note->user;
                $edit_db['row'] = $this->GetRow($edit_db['row'],$user);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $note = DB::table('notes')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$note) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            Note::where([
                ['id', $note->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.note_path').@$note->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($note,$user,$phancap)
    {
        $row = [];
        $row[] = $note->id;
        
        $row[] = '<a  title="">'.$note->name.'</a>';
        $row[] = $user;
        // $row[] = $this->GetImg([
        //     'avatar'=> $note->avatar,
        //     'data'=> 'note',
        //     'time'=> $note->updated_at
        // ]);
        $view = View::make('Backend/note/_actions', ['id' => $note->id,'page' => 'note']);
        $row[] = $view->render();

        return $row;
    }
}
