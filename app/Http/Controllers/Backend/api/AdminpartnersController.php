<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\Partner;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminpartnersController extends BaseAdminController
{

    public function index (Request $request) {

        $partners = DB::table('partners')
        ->join('customer_types', 'customer_types.id', '=', 'partners.id_customertype')
            ->select('partners.*','customer_types.name as typename')
            ->where([
                ['partners.del_flg', '=', 0]
            ])
            ->orderBy('partners.id', 'desc')
            ->get();


        $output = [];
        
        foreach ($partners as $key => $partner) {
            $typename = $partner->typename;
            $row = $this->GetRow($partner,$typename);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $partner = DB::table('partners')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $partner->status = (string)$partner->status;

            if ($partner) {
                $partner->avatar = config('general.partner_url') . $partner->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $partner;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = Partner::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            if($request['value']['id_customertype'] == 0){
                $data['code'] = 300;
                $data['error'] = 'Vui lòng chọn loại khách hàng';
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'partner');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = Partner::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'partner', $id);

            if ($request->status_code == 'edit') {
                
                $partner = DB::table('partners')
                ->join('customer_types', 'customer_types.id', '=', 'partners.id_customertype')
                ->select('partners.*','customer_types.name as typename')
                ->where([
                    ['partners.del_flg', '=', 0],
                    ['partners.id', '=', $id]
                ])
                ->first();
                
                $typename = $partner->typename;

                $edit_db['row'] = $this->GetRow($edit_db['row'],$typename);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $partner = DB::table('partners')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$partner) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            Partner::where([
                ['id', $partner->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.partner_path').$partner->avatar;
            if(isset($image_path))
            File::delete($image_path);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($partner,$typename)
    {
        $row = [];
        $row[] = $partner->id;
        
        $row[] = '<a  title="">'.$partner->name.'</a>';
        $row[] = $partner->phone;
        $row[] = $typename;
        // $row[] = $this->GetImg([
        //     'avatar'=> $partner->avatar,
        //     'data'=> 'partner',
        //     'time'=> $partner->updated_at
        // ]);
        $row[] = $partner->startdate;
        $view = View::make('Backend/partner/_status', ['status' => $partner->status]);
        $row[] = $view->render();
        $view = View::make('Backend/partner/_actions', ['id' => $partner->id,'page' => 'partner']);
        $row[] = $view->render();

        return $row;
    }
}
