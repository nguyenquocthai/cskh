<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\blog;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminblogsController extends BaseAdminController
{

    public function index (Request $request) {

        $blogs = DB::table('blogs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($blogs as $key => $blog) {
            $vitri = $key+1;
            $row = $this->GetRow($blog, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $blog = DB::table('blogs')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            $blog->status = (string)$blog->status;

            if ($blog) {
                $blog->avatar = config('general.blog_url') . $blog->avatar;
            }

            $data['code'] = 200;
            $data['data'] = $blog;
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = blog::validate();
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'blog');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = blog::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'blog', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $blog = DB::table('blogs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$blog) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            blog::where([
                ['id', $blog->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.blog_path').$blog->avatar;
            rename($image_path, config('general.blog_path')."del_".$blog->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($blog, $vitri)
    {
        $row = [];
        $row[] = $blog->id;
        $row[] = $blog->position;
        $row[] = $vitri;
        $row[] = '<a href="/admin/blog/edit/'.$blog->id.'" title="">'.$blog->title.'</a>';
        $row[] = $this->GetImg([
            'avatar'=> $blog->avatar,
            'data'=> 'blog',
            'time'=> $blog->updated_at
        ]);
        $row[] = '<span class="hidden">'.$blog->updated_at.'</span>'.date('d/m/Y', strtotime($blog->updated_at));
        $view = View::make('Backend/blog/_status', ['status' => $blog->status]);
        $row[] = $view->render();
        $view = View::make('Backend/blog/_actions', ['id' => $blog->id,'page' => 'blog']);
        $row[] = $view->render();

        return $row;
    }
}
