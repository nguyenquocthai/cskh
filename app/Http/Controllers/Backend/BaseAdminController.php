<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErrorCodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\historys;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use View;

class BaseAdminController extends Controller
{
    
    public function __construct()
    {
        $this->boot();
    }

    public function boot()
    {
        date_default_timezone_set("Asia/Bangkok");
        $name_apps = config('general.name_apps');
        $json_name_apps = json_encode($name_apps);
        View::share('json_name_apps', $json_name_apps);
        View::share('name_apps', $name_apps);
    }
}
