<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Model\MProvince;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;

class MProvincesController extends BaseRestController
{
    public function search_select2 () {

        $provinces = MProvince::get();
        $total_count = count($provinces);
        $output = [
            'incomplete_results' => false,
            'data' => $provinces,
            'total_count' => $total_count,
        ];
        return response()->json($output, 200);
    }
}
