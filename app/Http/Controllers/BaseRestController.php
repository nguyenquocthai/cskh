<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\ErrorCodes;

class BaseRestController extends Controller
{
	public $components = ['RequestHandler'];
	protected $resp = [
        'code' => ErrorCodes::E_OK,
        'message' => '',
        'data' => [],
        'error' => [],
    ];

    public function resp($code = null, $msg = null, $data = [])
    {
        if (!empty($code)) {
            $this->resp['code'] = $code;
        }
        if (!empty($msg)) {
            $this->resp['message'] = $msg;
        }
        if ($this->resp['code'] == ErrorCodes::E_OK) {
            $this->resp['data'] = $data;
        } else {
            $this->resp['error'] = $data;
        }
    }

    public function response()
    {
        // $this->set(array(
        //     'response' => $this->resp,
        //     '_serialize' => array('response')
        // ));
        // $this->response->header();
        echo json_encode($this->resp);
        exit;
        // $this->response->body(json_encode($this->resp));
        // $this->response->send();
        // $this->_stop();
    }

    public function exception($e)
    {
        CakeLog::write('alert', AppError::HEADER_ERROR_LOG);
        if (method_exists($e, 'getTrace')) {
            $trace = $e->getTrace();
        }
        $class = isset($trace[0]['class'])? $trace[0]['class']: '';
        $func = isset($trace[0]['function'])? $trace[0]['function']: '';
        CakeLog::write('error', empty($e->getMessage()) ? 'E_SYSTEM_ERROR' : $e->getMessage() . " <###> " . $class . ':' . $func . ':' . $e->getLine());
        $code = empty($e->getCode()) ? ErrorCodes::E_SYSTEM_ERROR : $e->getCode();
        $msg = empty($e->getMessage()) ? 'E_SYSTEM_ERROR' : $e->getMessage();
        $this->resp($code, $msg);
    }

    public function init_log()
    {
        $message = '';
        $message .= "- " . $this->request->here;
        $message .= "\n                            - Client_IP: " . CakeRequest::clientIp();
        $message .= "\n                            - METHOD: " . CakeRequest::method();

        if($this->Session->check("Auth.User"))
        {
            $u = $this->Session->read("Auth.User");
            $message .= "\n                            - User_id: " . $u['id'];
        }
        if (CakeRequest::method() === 'GET') {
            $message .= "\n                            - REQUEST - GET: " . json_encode($this->request->data);
        } else {
            $message .= "\n                            - REQUEST - POST: " . json_encode($this->request->data);
        }
        CakeLog::write('notice', $message);
    }

    public function error404()
    {
        $this->resp(ErrorCodes::E_NOT_FOUND, 'E_NOT_FOUND');
        return json_encode(['response' => $this->resp]);
    }
}
