<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Model\MLand;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;

class MLandsController extends BaseRestController
{

    public function search_select2 () {

        $lands = MLand::get()->toArray();

        $this->resp(ErrorCodes::E_OK, null, $lands);
        $this->response();
    }


}
