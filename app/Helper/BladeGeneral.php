<?php

class BladeGeneral
{
    //-------------------------------------------------------------------------------
    public static function checkrule_admin($list_urls, $url_main, $role) {
        $result = 300;
        if ($role == 1) {
            $result = 200;
        } else {
            foreach ($list_urls as $list_url) {
                if(strpos($list_url, $url_main) != false) {
                    $result = 200;
                    break;
                }
            }
        }
        return $result;
    }

    //-------------------------------------------------------------------------------
    public static function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision) . ' ' . $units[$pow]; 
    } 
    
    //-------------------------------------------------------------------------------
    public static function chat_img($chat) {

        foreach ($chat['profile'] as $profile) {
            if ($chat['kind_profile_flg'] == $profile['kind_profile_flg']) {
                $base_url = config('general.shop_user_url');

                if ($profile['kind_profile_flg'] == 1)
                    $base_url = config('general.shop_player_url');

                return BladeGeneral::image($base_url . $profile['avatar']);
            }
        }
    }

    //-------------------------------------------------------------------------------
    public static function get_session_user() {
        $user = Session::get('userSession');
        if($user)
            return $user;
        else
            return '';
    }

    //-------------------------------------------------------------------------------
    public static function get_method_delivery($name) {
        $method = Session::get('method_delivery');
        switch ($name) {
            case 'ship':
                return $method['ship'];
                break;
            case 'customer_time_time_receive':
                return $method['customer_time_time_receive'];
                break;
            default:
                return $method['customer_note'];
                break;
        }
    }

    //-------------------------------------------------------------------------------
    public static function time_booking($duo) {
        $date_now = date('d-m-Y H:i');
        switch ($duo['flg']) {
            case 2:
                $start = date('Y-m-d H:i', strtotime($duo['book_time']));
                if (strtotime($date_now) >= strtotime($start))
                    return 'd-block';
                else
                    return 'd-none';
                break;
            default:
                return 'd-block';
                break;
        }
    }


    //-------------------------------------------------------------------------------
    public static function fullBaseUrl()
    {
        //$domain = env("HTTP_HOST");
        $domain = $_SERVER['SERVER_NAME'];
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            // this is HTTPS
            return 'https://' . $domain;
        } else {
            // this is HTTP
            return 'http://' . $domain;
        }
    }

    public static function image($link = null, $options = []) {

        // $type = isset($options['type'])? $options['type']: null;
        // unset($options['type']);

        // if (@getimagesize(BladeGeneral::fullBaseUrl() . $link)) {
        //     //return Html::image($link . '?=' . Date('U'));
        //     return $link . '?=' . Date('U');
        // } else {
        //     $no_image = '';
        //     $no_image = '/public/img/no-image.png';
        //     return $no_image;
        // }
        return $link . '?=' . Date('U');
    }

    //--------------------------------------------------------------------------
    public static function GetImg($option = "") {

        $image_path = config('general.'.@$option['data'].'_path').@$option['avatar'];

        if (file_exists($image_path) && $option['avatar'] != "") {
            $src = config('general.'. $option['data'] .'_url') . $option['avatar'] . '?'.strtotime(@$option['time']);
        } else{
            if (@$option['data'] == 'nguoidung') {
                $src = '/public/img/user-no-image.png';
            } else {
                $src = '/public/img/no-image.png';
            }
        }

        return $src;
    }

    //-------------------------------------------------------------------------------
    public static function dateTime($date_time) {

        $lang = session('lang');

        if ($lang == '_vn' || !$lang) {
            if (isset($date_time) && $date_time != '') {
                return date('d/m/Y H:i:s', strtotime($date_time));
            }
        } else {
            return $date_time;
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function date($date, $lang = "") {

        if($lang == ""){
            $lang = '_vn';
        }

        if ($lang == '_vn' || !$lang) {
            if (isset($date) && $date != '') {
                return date('d/m/Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                return date('Y-m-d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function date2($date) {

        $lang = session('lang');

        if ($lang == '_vn' || !$lang) {
            if (isset($date) && $date != '') {
                return date('d.m.Y', strtotime($date));
            }
        } else {
            if (isset($date) && $date != '') {
                return date('Y.m.d', strtotime($date));
            }
        }

        return '';
    }

    //-------------------------------------------------------------------------------
    public static function common_date($date) {
        return date('d.m.Y', strtotime($date));
    }

    //-------------------------------------------------------------------------------
    public static function priceFormat($price, $options = []) {
        $options['default'] = isset($options['default']) ? $options['default'] : '0';

        if (isset($price) && $price != '' && $price != 0) {
            $nummber = number_format($price, 2, ',', '.');
            $nummber = str_replace(",00","",$nummber).'đ';
            return $nummber;
        }
        return $options['default'];
    }

    //-------------------------------------------------------------------------------
    public static function changekm($km) {
        return number_format($km/1000, 2, '.', '') + 0;
    }

    public static function change_his($sec)
    {
        $init = $sec;
        $hours = floor($init / 3600);
        if ($hours < 10) {
            $hours = '0'.$hours;
        }
        $minutes = floor(($init / 60) % 60);
        if ($minutes < 10) {
            $minutes = '0'.$minutes;
        }
        $seconds = $init % 60;
        if ($seconds < 10) {
            $seconds = '0'.$seconds;
        }

        $time = "$hours:$minutes:$seconds";
        return $time;
    }

    //-------------------------------------------------------------------------------
    public static function urlExists($url) {

        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    //-------------------------------------------------------------------------------
    public static function bd_nice_number($n) {
        $lang = session('lang');
        // first strip any formatting;
        $n = (0+str_replace(",","",$n));

        // is this a number?
        if(!is_numeric($n)) return false;

        if ($lang == '_vn') {
            // now filter it;
            if($n>=1000000000000) return round(($n/1000000000000),1).' Ngìn tỷ';
            else if($n>=1000000000) return round(($n/1000000000),1).' Tỷ';
            else if($n>=1000000) return round(($n/1000000),1).' Triệu';
        } else {
            if($n>=1000000000000) return round(($n/1000000000000),1).' TB';
            else if($n>=1000000000) return round(($n/1000000000),1).' Billion';
            else if($n>=1000000) return round(($n/1000000),1).' Million';
        }

        return number_format($n);
    }

    //-------------------------------------------------------------------------------
    public static function price_format($price, $options = []) {
        $options['default'] = isset($options['default']) ? $options['default'] : '0';

        if (isset($price) && $price != '' && $price != 0) {
            return number_format($price, 0, ',', '.') . ' đ';
        }
        return $options['default'];
    }

    //-------------------------------------------------------------------------------
    public static function limit_text($text, $limit) {
        $test = str_limit($text, $limit);
        return $test;
    }

    //-------------------------------------------------------------------------------
    public static function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $h = floor($time / 3600);
        $s = ($time % 3600);
        $m = floor($s / 60);
        return sprintf($format, $h, $m);
    }

    //-------------------------------------------------------------------------------
    public static function phongngu($phongngu = "") {
        switch ($phongngu) {
            case 'bed':
                $phongngu = '1 Bed';
                break;
            case '1+den':
                $phongngu = '1 + Den';
                break;
            case '2 beds':
                $phongngu = '2 Beds';
                break;
            case '2+den':
                $phongngu = '2 + Den';
                break;
            case '3 or more':
                $phongngu = '3 Or More';
                break;
            case '':
                $phongngu = 'Others';
                break;
            default:
                # code...
                break;
        }

        return $phongngu;
    }

    //-------------------------------------------------------------------------------
    public static function phongtam($phongtam = "") {
        switch ($phongtam) {
            case 'shared':
                $phongtam = 'Shared';
                break;
            case '3 or more':
                $phongtam = '3 Or More';
                break;
            default:
                # code...
                break;
        }

        return $phongtam;
    }
}