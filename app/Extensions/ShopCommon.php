<?php

namespace App\Extensions;

class ShopCommon
{
    //-------------------------------------------------------------------------------
    public static function get_filename_from_url($sUrl, $delimiter = '/')
    {
        if (empty($sUrl)) {
            return false;
        }

        $arrTemp = explode($delimiter, $sUrl);
        return end($arrTemp);
    }

    //-------------------------------------------------------------------------------
    public static function getExtension($s) {
        $pos = strrpos($s, '.');

        $extension = null;

        if ($pos)
            $extension = substr($s, $pos);

        return $extension;
    }
}