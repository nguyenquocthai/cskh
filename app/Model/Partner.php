<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partners';
    //public $timestamps = false;
    public static function validate() {
        return [
            'pattern' => [
                'name' =>'required',
                'phone' =>'required|numeric',
                'email' =>'required|email',
                'address' =>'required',
                'startdate' =>'required',
                'birthday' =>'required',
                'status' =>'required',
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'numeric'=>':attribute phải là số',
                'email'=>':attribute không đúng định dạng'
            ],

            'customName' => [
                'name'=>'Tên',
                'phone' =>'Số điện thoại',
                'email' =>'Email',
                'address' =>'Địa chỉ',
                'startdate' =>'Ngày bắt đầu',
                'birthday' =>'Ngày sinh',
                'status' =>'Trạng thái',
            ]
        ];
    }

}
