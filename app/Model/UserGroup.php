<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'user_groups';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'slug' => 'required|unique:user_groups,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Đường dẫn',
            ]
        ];
    }

}
