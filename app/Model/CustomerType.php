<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    protected $table = 'customer_types';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'status' =>'required',
                'slug' => 'unique:customer_types,slug,' . $id . ',id,del_flg,0'

            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
                'status'=>'Trạng thái',
            ]
        ];
    }

}
