<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomersLevel extends Model
{
    protected $table = 'customers_levels';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'discount' => 'required|numeric',
                'moneylandmark' => 'required|numeric',
                'numbercontracts' => 'required',
                'status' => 'required|numeric',
                'slug' => 'unique:customers_levels,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tốn tại',
                'numeric'=>':attribute phải là số'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
                'discount'=>'Chiết khấu',
                'moneylandmark'=>'Tổng tiền',
                'numbercontracts'=>'Số lượng hợp đồng',
                'status'=>'Trạng thái',
            ]
        ];
    }

}
