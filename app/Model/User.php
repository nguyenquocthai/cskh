<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'list_rule'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //-------------------------------------------------------------------------------
    public static function validate($id = 0) {
        return [
            'pattern' => [
                'name' =>'required',
                'fullname' =>'required',
                'status' =>'required',
                'phone' =>'required|numeric',
                'email' =>'required|email',
                'address' =>'required',
                'wage' =>'required',
                'slug' => 'unique:users,slug,' . $id . ',id,del_flg,0'
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại',
                'email' => ':attribute không đúng định dạng',
                'numeric'=>':attribute phải là số'
            ],

            'customName' => [
                'name'=>'Username',
                'fullname'=>'Họ tên',
                'email'=>'Email',
                'status'=>'Trạng thái',
                'phone'=>'Số điện thoại',
                'email' =>'Email',
                'address' =>'Địa chỉ',
                'wage' =>'Lương',
                'slug'=>'Username',
            ]
        ];
    }
}
