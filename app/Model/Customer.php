<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'status' =>'required',
                'code' =>'required',
                'companyname' =>'required',
                'phone' =>'required',
                'email' =>'required|email',
                'address' =>'required',
                'website' =>'required',
                'startdate' =>'required',
                'expirationdate' =>'required',
                'enterprise' =>'required',
                'slug' => 'unique:customers,slug,' . $id . ',id,del_flg,0'

            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'email'=>':attribute không đúng định dạng',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
                'status'=>'Trạng thái',
                'code' =>'Mã hợp đồng',
                'companyname' =>'Tên công ty',
                'phone' =>'Số điện thoại',
                'email' =>'Email',
                'address' =>'Địa chỉ',
                'website' =>'Website',
                'startdate' =>'Ngày bắt đầu',
                'expirationdate' =>'Ngày hết hạn',
                'enterprise' =>'Mô hình kinh doanh'
            ]
        ];
    }

}
