<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Đường dẫn',
            ]
        ];
    }

}
