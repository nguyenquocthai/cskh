<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'status' =>'required',
                'price' =>'required',
                'slug' => 'unique:services,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
                'status'=>'Trạng thái',
                'price'=>'Giá',
            ]
        ];
    }

}
