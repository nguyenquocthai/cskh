<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceAttribute extends Model
{
    protected $table = 'service_attributes';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'slug' => 'unique:service_attributes,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
            ]
        ];
    }

}
