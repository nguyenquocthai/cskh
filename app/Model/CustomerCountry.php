<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerCountry extends Model
{
    protected $table = 'customer_countries';
    //public $timestamps = false;
    public static function validate($id=0) {
        return [
            'pattern' => [
                'name' =>'required',
                'slug' => 'unique:customer_countries,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'name'=>'Tên',
                'slug'=>'Tên',
            ]
        ];
    }

}
