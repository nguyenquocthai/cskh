<?php


namespace App\Exceptions;


class OnePayPayment
{

    private $testUrl = 'https://mtf.onepay.vn/onecomm-pay/vpc.op';
    private $realUrl = ' https://onepay.vn/vpcpay/Vpcdps.op';

    private $secureSecret = 'A3EFDFABA8653DF2342E8DAC29B51AF0';
    // Mã merchante site
    private $merchant = 'ONEPAY'; // được cấp bởi OnePAY
    // Mật khẩu bảo mật
    private $accessCode = 'D67342C2'; // được cấp bởi OnePAY

    public function setupMerchant($merchant, $access, $secure) {
        $this->merchant = $merchant;
        $this->accessCode = $access;
        $this->secureSecret = $secure;
    }

    /*
     * process OnePay payment
     * @param array $paymentInfo
     *              + email => email of customer: required
     *              + phone => phone of customer: required
     *              + code  => order code: required
     *              + money    => money payment : required
     *              + return_url  => return url: required
     *
     * */
    public function process(array $paymentInfo = [])
    {
        $vpcURL = $this->testUrl . "?";

        $stringHashData = "";
        $appendAmp = 0;
        $arr = [
            'Title' => 'VPC 3-Party',
            'vpc_AccessCode' => $this->accessCode,
            'vpc_Amount' => $paymentInfo['money'] * 100,
            'vpc_Command' => 'pay',
            'vpc_Currency' => 'VND',
            'vpc_Customer_Email' => $paymentInfo['email'],
            'vpc_Customer_Id' => 'thanhvt',
            'vpc_Customer_Phone' => $paymentInfo['phone'],
            'vpc_Locale' => 'vn',
            'vpc_MerchTxnRef' => date( 'YmdHis' ) . rand(),
            'vpc_Merchant' => $this->merchant,
            'vpc_OrderInfo' => $paymentInfo['code'],
            'vpc_ReturnURL' => $paymentInfo['return_url'],
            'vpc_TicketNo' => '::1',
            'vpc_Version' => '2'
        ];

        foreach($arr as $key => $value) {
            // create the md5 input and URL leaving out any fields that have no value
            // tạo chuỗi đầu dữ liệu những tham số có dữ liệu
            if (strlen($value) > 0) {
                // this ensures the first paramter of the URL is preceded by the '?' char
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                //$stringHashData .= $value; *****************************sử dụng cả tên và giá trị tham số để mã hóa*****************************
                if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $stringHashData .= $key . "=" . $value . "&";
                }
            }
        }

        $stringHashData = rtrim($stringHashData, "&");
        if (strlen($this->secureSecret) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*', $this->secureSecret)));
        }

        header("Location: " . $vpcURL);exit;
    }

    public function resultValidate($arr)
    {
        $res = false;
        if ($arr['vpc_TxnResponseCode'] === '0'
//            && $arr['vpc_SecureHash'] == $this->secureSecret
        ) {
            $res = true;
        }
        return $res;
    }

    private function hash($text)
    {

    }

}
