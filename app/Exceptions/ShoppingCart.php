<?php

namespace App\Exceptions;

use Session;

class ShoppingCart
{
    protected $sessionKey;

    public function __construct()
    {
        $this->sessionKey = config('general.session_shopping_cart');
    }

    public function get()
    {
        $list = Session::get($this->sessionKey);
        return !empty($list)? $list: [];
    }

    public function set($data = [])
    {
        Session::put($this->sessionKey, $data);
    }

    public function destroy()
    {
        Session::forget($this->sessionKey);
    }
}