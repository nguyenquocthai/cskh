<?php


namespace App\Exceptions;


class NganLuongPayment
{

    // real api :  https://www.nganluong.vn/checkout.php
    // test api : https://sandbox.nganluong.vn:8088/nl30/checkout.php
    // site_code real : 52680
    // site_code test : 45877
    // secure_pass real : 1adf7d1146cb3cee68f521478866816b
    // secure_pass test : 506558a307a2e31eb3dca9ec7d64a690
    // email_receiver real : info@tacafoods.com
    // email_receiver test : leecansuu@gmail.com

    // Địa chỉ thanh toán hoá đơn của NgânLượng.vn
    private $nganluong_url = 'https://www.nganluong.vn/checkout.php';
    private $merchant_site_code = '52680'; // Mã website của bạn đăng ký trong chức năng tích hợp thanh toán của NgânLượng.vn.
    private $secure_pass= '1adf7d1146cb3cee68f521478866816b'; // Mật khẩu giao tiếp giữa website của bạn và NgânLượng.vn.
    private $affiliate_code = ''; //Mã đối tác tham gia chương trình liên kết của NgânLượng.vn

    private $email_receiver = 'info@tacafoods.com';

    public function setupMerchant($nganluong_url, $merchant_site_code, $secure_pass, $email_receiver)
    {
        $this->nganluong_url = $nganluong_url;
        $this->merchant_site_code = $merchant_site_code;
        $this->secure_pass = $secure_pass;
        $this->email_receiver = $email_receiver;

    }


    public function buildCheckoutUrlExpand($return_url, $receiver = '', $transaction_info, $order_code, $price, $currency = 'vnd', $quantity = 1, $tax = 0, $discount = 0, $fee_cal = 0, $fee_shipping = 0, $order_description = '', $buyer_info = '', $affiliate_code = '')
    {
        if ($affiliate_code == "") $affiliate_code = $this->affiliate_code;
        $arr_param = array(
            'merchant_site_code'=>  strval($this->merchant_site_code),
            'return_url'        =>  strval(strtolower($return_url)),
            'receiver'          =>  strval($this->email_receiver),
            'transaction_info'  =>  strval($transaction_info),
            'order_code'        =>  strval($order_code),
            'price'             =>  strval($price),
            'currency'          =>  strval($currency),
            'quantity'          =>  strval($quantity),
            'tax'               =>  strval($tax),
            'discount'          =>  strval($discount),
            'fee_cal'           =>  strval($fee_cal),
            'fee_shipping'      =>  strval($fee_shipping),
            'order_description' =>  strval($order_description),
            //"Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng"
            'buyer_info'        =>  strval($buyer_info),
            'affiliate_code'    =>  strval($affiliate_code)
        );

        $secure_code ='';
        $secure_code = implode(' ', $arr_param) . ' ' . $this->secure_pass;
        $arr_param['secure_code'] = md5($secure_code);
        /* */
        $redirect_url = $this->nganluong_url;
        if (strpos($redirect_url, '?') === false) {
            $redirect_url .= '?';
        } else if (substr($redirect_url, strlen($redirect_url)-1, 1) != '?' && strpos($redirect_url, '&') === false) {
            $redirect_url .= '&';
        }
        /* */
        $url = '';
        foreach ($arr_param as $key=>$value) {
            $value = urlencode($value);
            if ($url == '') {
                $url .= $key . '=' . $value;
            } else {
                $url .= '&' . $key . '=' . $value;
            }
        }
        return $redirect_url.$url;
    }

    public function verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code)
    {
        // Tạo mã xác thực từ chủ web
        $str = '';
        $str .= ' ' . strval($transaction_info);
        $str .= ' ' . strval($order_code);
        $str .= ' ' . strval($price);
        $str .= ' ' . strval($payment_id);
        $str .= ' ' . strval($payment_type);
        $str .= ' ' . strval($error_text);
        $str .= ' ' . strval($this->merchant_site_code);
        $str .= ' ' . strval($this->secure_pass);

        // Mã hóa các tham số
        $verify_secure_code = '';
        $verify_secure_code = md5($str);

        // Xác thực mã của chủ web với mã trả về từ nganluong.vn
        if ($verify_secure_code === $secure_code) return true;
        else return false;
    }

}
